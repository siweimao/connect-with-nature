# Connect With Nature - 11 AM Group 9

![alt text](/public/leaf.png/?raw=true)  

The goal of this project is to promote civic appreciation of nature and raise awareness for future conservation efforts by encouraging visitation to the national parks. We hope that this site will help you better appreciate the importance of preservation and consider becoming involved in current conservation efforts!

## Git SHA (Phase II)

c2b9e9f4f1efbadd184ce3b394ebdf18a505aa8c

## Git SHA (Phase III)

75dd29c5da75b7dc450063ae1eba4c6f9b46d9ae

## Git SHA (Phase IV)

21c5cfd82551f6d78800dbdbd151b8665b1e95c2

## Installation & Dependencies

After cloning the repository, run `npm install` to install required project dependencies.<br>
Our project stored private keys in /src/Pages/config.js for use in /src/Pages/StatTable.js.<br>
The config.js file looks as follows:<br>
~~~~
const apiKey = 'GITLAB_API_KEY';
const projID = 'PROJECT_ID';

export { apiKey, projID };
~~~~
To run the project in development mode, run `npm start` and navigate to [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Deployment

On the AWS CLI, first configure personal project credentials including Access Key ID and Secret Access Key.<br>
Within the project directory, run `npm run build` and `npm run deploy` for the site to go live.

## Project Estimations

Estimated Completion Time (Phase I): 20 hrs<br>
Actual Completion Time (Phase I): 30 hrs

Estimated Completion Time (Phase II): 40 hrs<br>
Actual Completion Time (Phase II): 50 hrs

Estimated Completion Time (Phase III): 20 hrs<br>
Actual Completion Time (Phase III): 60 hrs

Estimated Completion Time (Phase IV): 20 hrs<br>
Actual Completion Time (Phase IV): 30 hrs

## Visualizations
Our visualizations are located in the Visualizations tab on the Navigation Bar. Inside this tab, there are three more tabs for each of our visualizations as well as a fourth tab for our developer’s visualizations.


## Links
[Production Website](https://www.connectwithnature.me) <br>
[Postman](https://documenter.getpostman.com/view/6815214/S1ETPvY9)
