import os
from unittest import main, TestCase
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from schema import (
    Base,
    engine,
    ParkInstance,
    StateInstance,
    EventInstance,
    OrgInstance,
    get_env_variable,
)
from main import dict_creator, json_response

Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()


class UnitTests(TestCase):
    def test_source_insert_1(self):
        s = OrgInstance(
            name="Lorem ipsum1",
            tagline="dolor sit amet",
            mission="consectetur adipiscing elit",
            state="Morbi interdum facilisis",
            category="dui posuere placerat",
            rating="Aenean scelerisque libero",
            pics="eget dui ultricies",
            parks="ac tempus sapien mollis",
            events="Nam ornare viverra",
            park_pics="At elementum eu facilisis",
            state_pics="Nisl pretium fusce",
            event_pics="Aliquam vestibulum morbi",
            youtube_id="Eget egestas purus",
        )

        try:
            session.add(s)
            session.commit()
            r = session.query(OrgInstance).filter_by(name="Lorem ipsum1").one()
            self.assertEqual(str(r.tagline), "dolor sit amet")
            session.delete(r)
            session.commit()
        except Exception as e:
            print(e)
            print("error in test_source_insert_1")
            session.rollback()
            assert False

    def test_source_insert_2(self):
        s = ParkInstance(
            name="Lorem ipsum2",
            states="dolor sit amet",
            summary="consectetur adipiscing elit",
            directions="Morbi interdum facilisis",
            weather="dui posuere placerat",
            phone="Aenean scelerisque libero",
            email="eget dui ultricies",
            campgrounds=65,
            fee=34,
            pics="Vivamus pharetra malesuada",
            events="Fusce nec mauris",
            orgs="Quisque sit amet",
            lat=10.0,
            long=10.0,
            state_pics="Consectetur purus ut",
            event_pics="Tortor pretium viverra",
            org_pics="adipiscing elit pellentesque",
        )

        try:
            session.add(s)
            session.commit()
            r = session.query(ParkInstance).filter_by(name="Lorem ipsum2").one()
            self.assertEqual(str(r.email), "eget dui ultricies")
            session.delete(r)
            session.commit()
        except Exception as e:
            print(e)
            print("error in test_source_insert_2")
            session.rollback()
            assert False

    def test_source_insert_3(self):
        s = StateInstance(
            name="Lorem ipsum3",
            abbr="Et",
            nickname="dolor sit amet",
            summary="consectetur adipiscing elit",
            capital="Morbi interdum facilisis",
            areaRank=4,
            densityRank=6,
            popRank=2,
            parks="et ultrices posuere",
            pic="ultricies vehicula felis",
            events="Vivamus pharetra malesuada",
            area="Fusce nec mauris",
            population="Quisque sit amet",
            incomeRank="Mauris sit amet",
            orgs="bibendum consequat",
            park_pics="Placerat orci nulla",
            event_pics="pellentesque dignissim",
            org_pics="Suscipit adipiscing bibendum",
            state_map="duis at consectetur",
        )

        try:
            session.add(s)
            session.commit()
            r = session.query(StateInstance).filter_by(name="Lorem ipsum3").one()
            self.assertEqual(str(r.nickname), "dolor sit amet")
            session.delete(r)
            session.commit()
        except Exception as e:
            print(e)
            print("error in test_source_insert_3")
            session.rollback()
            assert False

    def test_source_insert_4(self):
        s = EventInstance(
            name="Lorem ipsum4",
            date="dolor sit amet",
            location="consectetur adipiscing elit",
            park="Morbi interdum facilisis",
            fee="dui posuere placerat",
            time="Aenean scelerisque libero",
            summary="eget dui ultricies",
            pics="et ultrices posuere",
            states="ultricies vehicula felis",
            orgs="Vivamus pharetra malesuada",
            state_pics="Nullam ac tortor",
            park_pics="vitae purus faucibus",
            org_pics="Tincidunt lobortis feugiat",
            park_lat=0.0,
            park_long=0.0,
            date_single="Volutpat ac tincidunt",
        )

        try:
            session.add(s)
            session.commit()
            r = session.query(EventInstance).filter_by(name="Lorem ipsum4").one()
            self.assertEqual(str(r.states), "ultricies vehicula felis")
            session.delete(r)
            session.commit()
        except Exception as e:
            print(e)
            print("error in test_source_insert_4")
            session.rollback()
            assert False

    def test_source_insert_5(self):
        s = OrgInstance(
            name="Lorem ipsum5",
            tagline="dolor sit amet",
            mission="consectetur adipiscing elit",
            state="Morbi interdum facilisis",
            category="dui posuere placerat",
            rating="Aenean scelerisque libero",
            pics="eget dui ultricies",
            parks="ac tempus sapien mollis",
            events="Nam ornare viverra",
            park_pics="At elementum eu facilisis",
            state_pics="Nisl pretium fusce",
            event_pics="Aliquam vestibulum morbi",
            youtube_id="Eget egestas purus",
        )

        try:
            session.add(s)
            session.commit()
            r = session.query(OrgInstance).filter_by(name="Lorem ipsum5").one()
            self.assertEqual(str(r.category), "dui posuere placerat")
            session.delete(r)
            session.commit()
        except Exception as e:
            print(e)
            print("error in test_source_insert_5")
            session.rollback()
            assert False

    def test_source_insert_6(self):
        s = ParkInstance(
            name="Lorem ipsum6",
            states="dolor sit amet",
            summary="consectetur adipiscing elit",
            directions="Morbi interdum facilisis",
            weather="dui posuere placerat",
            phone="Aenean scelerisque libero",
            email="eget dui ultricies",
            campgrounds=65,
            fee=34,
            pics="Vivamus pharetra malesuada",
            events="Fusce nec mauris",
            orgs="Quisque sit amet",
            lat=10.0,
            long=10.0,
            state_pics="Consectetur purus ut",
            event_pics="Tortor pretium viverra",
            org_pics="adipiscing elit pellentesque",
        )

        try:
            session.add(s)
            session.commit()
            r = session.query(ParkInstance).filter_by(name="Lorem ipsum6").one()
            self.assertEqual(r.campgrounds, 65)
            session.delete(r)
            session.commit()
        except Exception as e:
            print(e)
            print("error in test_source_insert_6")
            session.rollback()
            assert False

    def test_source_insert_7(self):
        s = StateInstance(
            name="Lorem ipsum7",
            nickname="dolor sit amet",
            summary="consectetur adipiscing elit",
            capital="Morbi interdum facilisis",
            areaRank=9,
            densityRank=10,
            popRank=15,
            parks="et ultrices posuere",
            pic="ultricies vehicula felis",
            events="Vivamus pharetra malesuada",
            area="Fusce nec mauris",
            population="Quisque sit amet",
            incomeRank="Mauris sit amet",
            orgs="bibendum consequat",
            park_pics="Placerat orci nulla",
            event_pics="pellentesque dignissim",
            org_pics="Suscipit adipiscing bibendum",
            state_map="duis at consectetur",
        )

        try:
            session.add(s)
            session.commit()
            r = session.query(StateInstance).filter_by(name="Lorem ipsum7").one()
            self.assertEqual(r.incomeRank, "Mauris sit amet")
            session.delete(r)
            session.commit()
        except Exception as e:
            print(e)
            print("error in test_source_insert_7")
            session.rollback()
            assert False

    def test_source_insert_8(self):
        s = EventInstance(
            name="Lorem ipsum8",
            date="dolor sit amet",
            location="consectetur adipiscing elit",
            park="Morbi interdum facilisis",
            fee="dui posuere placerat",
            time="Aenean scelerisque libero",
            summary="eget dui ultricies",
            pics="et ultrices posuere",
            states="ultricies vehicula felis",
            orgs="Vivamus pharetra malesuada",
            state_pics="Nullam ac tortor",
            park_pics="vitae purus faucibus",
            org_pics="Tincidunt lobortis feugiat",
            park_lat=0.0,
            park_long=0.0,
            date_single="Volutpat ac tincidunt",
        )

        try:
            session.add(s)
            session.commit()
            r = session.query(EventInstance).filter_by(name="Lorem ipsum8").one()
            self.assertEqual(str(r.time), "Aenean scelerisque libero")
            session.delete(r)
            session.commit()
        except Exception as e:
            print(e)
            print("error in test_source_insert_8")
            session.rollback()
            assert False

    def test_dict_creator_1(self):
        s1 = StateInstance(
            name="Lorem ipsum9",
            abbr="Et",
            nickname="dolor sit amet",
            summary="consectetur adipiscing elit",
            capital="Morbi interdum facilisis",
            areaRank=4,
            densityRank=6,
            popRank=2,
            parks="et ultrices posuere",
            pic="ultricies vehicula felis",
            events="Vivamus pharetra malesuada",
            area="Fusce nec mauris",
            population="Quisque sit amet",
            incomeRank="Mauris sit amet",
            orgs="bibendum consequat",
            park_pics="Placerat orci nulla",
            event_pics="pellentesque dignissim",
            org_pics="Suscipit adipiscing bibendum",
            state_map="duis at consectetur",
        )
        s2 = StateInstance(
            name="Lorem ipsum10",
            abbr="Et",
            nickname="dolor sit amet",
            summary="consectetur adipiscing elit",
            capital="Morbi interdum facilisis",
            areaRank=4,
            densityRank=6,
            popRank=2,
            parks="et ultrices posuere",
            pic="ultricies vehicula felis",
            events="Vivamus pharetra malesuada",
            area="Fusce nec mauris",
            population="Quisque sit amet",
            incomeRank="Mauris sit amet",
            orgs="bibendum consequat",
            park_pics="Placerat orci nulla",
            event_pics="pellentesque dignissim",
            org_pics="Suscipit adipiscing bibendum",
            state_map="duis at consectetur",
        )
        l = [s1, s2]
        dc = dict_creator(l)
        assert type(dc) is list
        assert type(dc[0]) is dict
        assert dc[0]["name"] == "Lorem ipsum9"

    def test_dict_creator_2(self):
        o1 = OrgInstance(
            name="Lorem ipsum11",
            tagline="dolor sit amet",
            mission="consectetur adipiscing elit",
            state="Morbi interdum facilisis",
            category="dui posuere placerat",
            rating="Aenean scelerisque libero",
            pics="eget dui ultricies",
            parks="ac tempus sapien mollis",
            events="Nam ornare viverra",
            park_pics="At elementum eu facilisis",
            state_pics="Nisl pretium fusce",
            event_pics="Aliquam vestibulum morbi",
            youtube_id="Eget egestas purus",
        )
        o2 = OrgInstance(
            name="Lorem ipsum12",
            tagline="dolor sit amet",
            mission="consectetur adipiscing elit",
            state="Morbi interdum facilisis",
            category="dui posuere placerat",
            rating="Aenean scelerisque libero",
            pics="eget dui ultricies",
            parks="ac tempus sapien mollis",
            events="Nam ornare viverra",
            park_pics="At elementum eu facilisis",
            state_pics="Nisl pretium fusce",
            event_pics="Aliquam vestibulum morbi",
            youtube_id="Eget egestas purus",
        )
        l = [o1, o2]
        dc = dict_creator(l)
        assert type(dc) is list
        assert type(dc[0]) is dict
        assert dc[0]["name"] == "Lorem ipsum11"

    def test_dict_creator_3(self):
        e1 = EventInstance(
            name="Lorem ipsum13",
            date="dolor sit amet",
            location="consectetur adipiscing elit",
            park="Morbi interdum facilisis",
            fee="dui posuere placerat",
            time="Aenean scelerisque libero",
            summary="eget dui ultricies",
            pics="et ultrices posuere",
            states="ultricies vehicula felis",
            orgs="Vivamus pharetra malesuada",
            state_pics="Nullam ac tortor",
            park_pics="vitae purus faucibus",
            org_pics="Tincidunt lobortis feugiat",
            park_lat=0.0,
            park_long=0.0,
            date_single="Volutpat ac tincidunt",
        )
        e2 = EventInstance(
            name="Lorem ipsum14",
            date="dolor sit amet",
            location="consectetur adipiscing elit",
            park="Morbi interdum facilisis",
            fee="dui posuere placerat",
            time="Aenean scelerisque libero",
            summary="eget dui ultricies",
            pics="et ultrices posuere",
            states="ultricies vehicula felis",
            orgs="Vivamus pharetra malesuada",
            state_pics="Nullam ac tortor",
            park_pics="vitae purus faucibus",
            org_pics="Tincidunt lobortis feugiat",
            park_lat=0.0,
            park_long=0.0,
            date_single="Volutpat ac tincidunt",
        )
        l = [e1, e2]
        dc = dict_creator(l)
        assert type(dc) is list
        assert type(dc[0]) is dict
        assert dc[0]["name"] == "Lorem ipsum13"

    def test_dict_creator_4(self):
        p1 = ParkInstance(
            name="Lorem ipsum15",
            states="dolor sit amet",
            summary="consectetur adipiscing elit",
            directions="Morbi interdum facilisis",
            weather="dui posuere placerat",
            phone="Aenean scelerisque libero",
            email="eget dui ultricies",
            campgrounds=65,
            fee=34,
            pics="Vivamus pharetra malesuada",
            events="Fusce nec mauris",
            orgs="Quisque sit amet",
            lat=10.0,
            long=10.0,
            state_pics="Consectetur purus ut",
            event_pics="Tortor pretium viverra",
            org_pics="adipiscing elit pellentesque",
        )
        p2 = ParkInstance(
            name="Lorem ipsum16",
            states="dolor sit amet",
            summary="consectetur adipiscing elit",
            directions="Morbi interdum facilisis",
            weather="dui posuere placerat",
            phone="Aenean scelerisque libero",
            email="eget dui ultricies",
            campgrounds=65,
            fee=34,
            pics="Vivamus pharetra malesuada",
            events="Fusce nec mauris",
            orgs="Quisque sit amet",
            lat=10.0,
            long=10.0,
            state_pics="Consectetur purus ut",
            event_pics="Tortor pretium viverra",
            org_pics="adipiscing elit pellentesque",
        )
        l = [p1, p2]
        dc = dict_creator(l)
        assert type(dc) is list
        assert type(dc[0]) is dict
        assert dc[0]["name"] == "Lorem ipsum15"

    def test_dict_creator_5(self):
        l = []
        dc = dict_creator(l)
        assert type(dc) is list
        assert dc == []

    def test_get_env_variable_1(self):
        try:
            get_env_variable("siweilovesnatures")
        except Exception:
            assert True

    def test_get_env_variable_2(self):
        try:
            get_env_variable("HOME")
            assert True
        except Exception:
            assert False

    def test_json_response_1(self):
        payload = {"a": 1, "b": 2}
        dump, status, dict = json_response(payload)
        assert '"a": 1' in dump
        assert '"b": 2' in dump
        assert status == 200

    def test_json_response_2(self):
        payload = {}
        dump, status, dict = json_response(payload, 404)
        assert status == 404

    def test_json_response_3(self):
        payload = {}
        dump, status, dict = json_response(payload)
        assert dump == "{}"
        assert status == 200
        assert dict == {
            "content-type": "application/json",
            "Access-Control-Allow-Origin": "*",
        }


if __name__ == "__main__":
    main()
