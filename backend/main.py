#!/bin/sh

import logging
import os
import sys
import re

from flask import Flask, request, json

from sqlalchemy import create_engine, or_, func
from sqlalchemy.orm import sessionmaker
from flask_cors import CORS

from schema import engine, Base, ParkInstance, StateInstance, EventInstance, OrgInstance

app = Flask(__name__)

CORS(app)

Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()


@app.route("/park", methods=["GET"])
def index():
    park_name = request.args.get("park_name", None)
    park_state = request.args.get("park_state", None)
    if park_name != None:
        try:
            park_single = session.query(ParkInstance).filter_by(name=park_name)
        except:
            session.rollback()
        park_single_dict = dict_creator(park_single)
        return json_response(park_single_dict)
    else:
        if park_state != None:
            try:
                park_state_list = [
                    x
                    for x in session.query(ParkInstance).all()
                    if park_state in x.states
                ]
            except:
                session.rollback()
            park_state_dict = dict_creator(park_state_list)
            return json_response(park_state_dict)
        else:
            try:
                park_all = session.query(ParkInstance).all()
            except:
                session.rollback()
            park_dict = dict_creator(park_all)
            return json_response(park_dict)


def dict_creator(instanceList):
    ret = []
    for obj in instanceList:
        objDict = {}
        d = obj.__dict__
        for k in d:
            if k == "_sa_instance_state":
                continue
            else:
                objDict[k] = d[k]
        ret.append(objDict)
    return ret


def parkSortSearch(attr, search):
    park_single = {}
    try:
        re = "%" + search.lower() + "%"
        park_single = (
            session.query(ParkInstance)
            .filter(
                or_(
                    func.lower(ParkInstance.name).like(re),
                    func.lower(ParkInstance.states).like(re),
                    func.lower(ParkInstance.summary).like(re),
                    func.lower(ParkInstance.directions).like(re),
                )
            )
            .order_by(attr)
        )
    except:
        session.rollback()
    return park_single


def parkFilterAll(attr, state, minFee, maxFee, minCamp, maxCamp):
    park_single = {}
    try:
        park_single = (
            session.query(ParkInstance)
            .filter_by(states=state)
            .filter(ParkInstance.fee.between(minFee, maxFee))
            .filter(ParkInstance.campgrounds.between(minCamp, maxCamp))
            .order_by(attr)
        )
    except:
        session.rollback()
    return park_single


def parkFilterTwoF(attr, state, minFee, maxFee):
    park_single = {}
    try:
        park_single = (
            session.query(ParkInstance)
            .filter_by(states=state)
            .filter(ParkInstance.fee.between(minFee, maxFee))
            .order_by(attr)
        )
    except:
        session.rollback()
    return park_single


def parkFilterTwoC(attr, state, minCamp, maxCamp):
    park_single = {}
    try:
        park_single = (
            session.query(ParkInstance)
            .filter_by(states=state)
            .filter(ParkInstance.campgrounds.between(minCamp, maxCamp))
            .order_by(attr)
        )
    except:
        session.rollback()
    return park_single


@app.route("/state", methods=["GET"])
def index2():
    state_name = request.args.get("state_name", None)
    if state_name == None:
        try:
            state_all = session.query(StateInstance).all()
        except:
            session.rollback()
        state_dict = dict_creator(state_all)
        return json_response(state_dict)
    else:
        try:
            state_single = session.query(StateInstance).filter_by(name=state_name)
        except:
            session.rollback()
        state_single_dict = dict_creator(state_single)
        return json_response(state_single_dict)


@app.route("/event", methods=["GET"])
def index3():
    event_name = request.args.get("event_name", None)
    if event_name == None:
        try:
            event_all = session.query(EventInstance).all()
        except:
            session.rollback()
        event_dict = dict_creator(event_all)
        return json_response(event_dict)
    else:
        try:
            event_single = session.query(EventInstance).filter_by(name=event_name)
        except:
            session.rollback()
        event_single_dict = dict_creator(event_single)
        return json_response(event_single_dict)


@app.route("/org", methods=["GET"])
def index4():
    org_name = request.args.get("org_name", None)
    org_category = request.args.get("org_category", None)
    if org_name != None:  # if org_name or org_category is filled, then return it
        try:
            org_single = session.query(OrgInstance).filter_by(name=org_name)
        except:
            session.rollback()
        org_single_dict = dict_creator(org_single)
        return json_response(org_single_dict)
    else:
        if org_category != None:
            try:
                org_categories = session.query(OrgInstance).filter_by(
                    category=org_category
                )
            except:
                session.rollback()
            org_category_dict = dict_creator(org_categories)
            return json_response(org_category_dict)
        else:  # else, just return all orgs
            try:
                org_all = session.query(OrgInstance).all()
            except:
                session.rollback()
            org_dict = dict_creator(org_all)
            return json_response(org_dict)


@app.route("/park/filter", methods=["GET"])
def index5():
    # Retrieve args
    state_name = request.args.get("state_name", None)
    fee = request.args.get("fee", None)
    campgrounds = request.args.get("campgrounds", None)
    sort = request.args.get("sort", None)
    search = request.args.get("search", None)
    dictT = {}
    if search != None:
        if sort == "Name (A-Z)":
            dictT = parkSortSearch(ParkInstance.name, search)
        elif sort == "Number of Campsites (Least to Greatest)":
            dictT = parkSortSearch(ParkInstance.campgrounds, search)
        elif sort == "Fee (Low to High)":
            dictT = parkSortSearch(ParkInstance.fee, search)
        else:
            try:
                re = "%" + search.lower() + "%"
                dictT = session.query(ParkInstance).filter(
                    or_(
                        func.lower(ParkInstance.name).like(re),
                        func.lower(ParkInstance.states).like(re),
                        func.lower(ParkInstance.summary).like(re),
                        func.lower(ParkInstance.directions).like(re),
                    )
                )
            except:
                session.rollback()
        park_single_dict = dict_creator(dictT)
        return json_response(park_single_dict)
    elif state_name != None and fee != None and campgrounds != None:
        min = -1
        max = -1
        minCamp = -1
        maxCamp = -1
        if fee == "Low":
            min = 0
            max = 12
        elif fee == "Medium":
            min = 13
            max = 24
        elif fee == "High":
            min = 25
            max = 35

        if campgrounds == "Small":
            minCamp = 0
            maxCamp = 1350
        elif campgrounds == "Moderate":
            minCamp = 1351
            maxCamp = 2700
        elif campgrounds == "Large":
            minCamp = 2701
            maxCamp = 4050

        if sort == "Name (A-Z)":
            dictT = parkFilterAll(
                ParkInstance.name, state_name, min, max, minCamp, maxCamp
            )
        elif sort == "Number of Campsites (Least to Greatest)":
            dictT = parkFilterAll(
                ParkInstance.campgrounds, state_name, min, max, minCamp, maxCamp
            )
        elif sort == "Fee (Low to High)":
            dictT = parkFilterAll(
                ParkInstance.fee, state_name, min, max, minCamp, maxCamp
            )
        else:
            try:
                dictT = (
                    session.query(ParkInstance)
                    .filter_by(states=state_name)
                    .filter(ParkInstance.fee.between(min, max))
                    .filter(ParkInstance.campgrounds.between(minCamp, maxCamp))
                )
            except:
                session.rollback()
        park_single_dict = dict_creator(dictT)
    elif state_name != None and fee != None:
        min = -1
        max = -1

        if fee == "Low":
            min = 0
            max = 12
        elif fee == "Medium":
            min = 13
            max = 24
        elif fee == "High":
            min = 25
            max = 35

        if sort == "Name (A-Z)":
            dictT = parkFilterTwoF(ParkInstance.name, state_name, min, max)
        elif sort == "Number of Campsites (Least to Greatest)":
            dictT = parkFilterTwoF(ParkInstance.campgrounds, state_name, min, max)
        elif sort == "Fee (Low to High)":
            dictT = parkFilterTwoF(ParkInstance.fee, state_name, min, max)
        else:
            try:
                dictT = (
                    session.query(ParkInstance)
                    .filter_by(states=state_name)
                    .filter(ParkInstance.fee.between(min, max))
                )
            except:
                session.rollback()
        park_single_dict = dict_creator(dictT)
    elif state_name != None and campgrounds != None:
        minCamp = -1
        maxCamp = -1
        if campgrounds == "Small":
            minCamp = 0
            maxCamp = 1350
        elif campgrounds == "Moderate":
            minCamp = 1351
            maxCamp = 2700
        elif campgrounds == "Large":
            minCamp = 2701
            maxCamp = 4050

        if sort == "Name (A-Z)":
            dictT = parkFilterTwoC(ParkInstance.name, state_name, minCamp, maxCamp)
        elif sort == "Number of Campsites (Least to Greatest)":
            dictT = parkFilterTwoC(
                ParkInstance.campgrounds, state_name, minCamp, maxCamp
            )
        elif sort == "Fee (Low to High)":
            dictT = parkFilterTwoC(ParkInstance.fee, state_name, minCamp, maxCamp)
        else:
            try:
                dictT = (
                    session.query(ParkInstance)
                    .filter_by(states=state_name)
                    .filter(ParkInstance.campgrounds.between(minCamp, maxCamp))
                )
            except:
                session.rollback()
        park_single_dict = dict_creator(dictT)
    elif fee != None and campgrounds != None:
        minCamp = -1
        maxCamp = -1
        min = -1
        max = -1
        if fee == "Low":
            min = 0
            max = 12
        elif fee == "Medium":
            min = 13
            max = 24
        elif fee == "High":
            min = 25
            max = 35

        if campgrounds == "Small":
            minCamp = 0
            maxCamp = 1350
        elif campgrounds == "Moderate":
            minCamp = 1351
            maxCamp = 2700
        elif campgrounds == "Large":
            minCamp = 2701
            maxCamp = 4050

        if sort == "Name (A-Z)":
            try:
                park_single = (
                    session.query(ParkInstance)
                    .filter(ParkInstance.fee.between(min, max))
                    .filter(ParkInstance.campgrounds.between(minCamp, maxCamp))
                    .order_by(ParkInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Number of Campsites (Least to Greatest)":
            try:
                park_single = (
                    session.query(ParkInstance)
                    .filter(ParkInstance.fee.between(min, max))
                    .filter(ParkInstance.campgrounds.between(minCamp, maxCamp))
                    .order_by(ParkInstance.campgrounds)
                )
            except:
                session.rollback()
        elif sort == "Fee (Low to High)":
            try:
                park_single = (
                    session.query(ParkInstance)
                    .filter(ParkInstance.fee.between(min, max))
                    .filter(ParkInstance.campgrounds.between(minCamp, maxCamp))
                    .order_by(ParkInstance.fee)
                )
            except:
                session.rollback()
        else:
            try:
                park_single = (
                    session.query(ParkInstance)
                    .filter(ParkInstance.fee.between(min, max))
                    .filter(ParkInstance.campgrounds.between(minCamp, maxCamp))
                )
            except:
                session.rollback()
        park_single_dict = dict_creator(park_single)
    elif state_name != None:
        if sort == "Name (A-Z)":
            try:
                park_single = (
                    session.query(ParkInstance)
                    .filter_by(states=state_name)
                    .order_by(ParkInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Number of Campsites (Least to Greatest)":
            try:
                park_single = (
                    session.query(ParkInstance)
                    .filter_by(states=state_name)
                    .order_by(ParkInstance.campgrounds)
                )
            except:
                session.rollback()
        elif sort == "Fee (Low to High)":
            try:
                park_single = (
                    session.query(ParkInstance)
                    .filter_by(states=state_name)
                    .order_by(ParkInstance.fee)
                )
            except:
                session.rollback()
        else:
            try:
                park_single = session.query(ParkInstance).filter_by(states=state_name)
            except:
                session.rollback()
        park_single_dict = dict_creator(park_single)
    elif fee != None:
        min = -1
        max = -1
        if fee == "Low":
            min = 0
            max = 12
        elif fee == "Medium":
            min = 13
            max = 24
        elif fee == "High":
            min = 25
            max = 35
        if sort == "Name (A-Z)":
            try:
                park_single = (
                    session.query(ParkInstance)
                    .filter(ParkInstance.fee.between(min, max))
                    .order_by(ParkInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Number of Campsites (Least to Greatest)":
            try:
                park_single = (
                    session.query(ParkInstance)
                    .filter(ParkInstance.fee.between(min, max))
                    .order_by(ParkInstance.campgrounds)
                )
            except:
                session.rollback()
        elif sort == "Fee (Low to High)":
            try:
                park_single = (
                    session.query(ParkInstance)
                    .filter(ParkInstance.fee.between(min, max))
                    .order_by(ParkInstance.fee)
                )
            except:
                session.rollback()
        else:
            try:
                park_single = session.query(ParkInstance).filter(
                    ParkInstance.fee.between(min, max)
                )
            except:
                session.rollback()
        park_single_dict = dict_creator(park_single)
    elif campgrounds != None:
        minCamp = -1
        maxCamp = -1
        if campgrounds == "Small":
            minCamp = 0
            maxCamp = 1350
        elif campgrounds == "Moderate":
            minCamp = 1351
            maxCamp = 2700
        elif campgrounds == "Large":
            minCamp = 2701
            maxCamp = 4050

        if sort == "Name (A-Z)":
            try:
                park_single = (
                    session.query(ParkInstance)
                    .filter(ParkInstance.campgrounds.between(minCamp, maxCamp))
                    .order_by(ParkInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Number of Campsites (Least to Greatest)":
            try:
                park_single = (
                    session.query(ParkInstance)
                    .filter(ParkInstance.campgrounds.between(minCamp, maxCamp))
                    .order_by(ParkInstance.campgrounds)
                )
            except:
                session.rollback()
        elif sort == "Fee (Low to High)":
            try:
                park_single = (
                    session.query(ParkInstance)
                    .filter(ParkInstance.campgrounds.between(minCamp, maxCamp))
                    .order_by(ParkInstance.fee)
                )
            except:
                session.rollback()
        else:
            try:
                park_single = session.query(ParkInstance).filter(
                    ParkInstance.campgrounds.between(minCamp, maxCamp)
                )
            except:
                session.rollback()
        park_single_dict = dict_creator(park_single)

    elif sort == "Name (A-Z)":
        try:
            park_single = session.query(ParkInstance).order_by(ParkInstance.name)
        except:
            session.rollback()
        park_single_dict = dict_creator(park_single)
    elif sort == "Number of Campsites (Least to Greatest)":
        try:
            park_single = session.query(ParkInstance).order_by(ParkInstance.campgrounds)
        except:
            session.rollback()
        park_single_dict = dict_creator(park_single)
    elif sort == "Fee (Low to High)":
        try:
            park_single = session.query(ParkInstance).order_by(ParkInstance.fee)
        except:
            session.rollback()
        park_single_dict = dict_creator(park_single)
    return json_response(park_single_dict)


@app.route("/state/filter", methods=["GET"])
def index6():
    pop = request.args.get("population", None)
    area = request.args.get("area", None)
    density = request.args.get("density", None)
    sort = request.args.get("sort", None)
    search = request.args.get("search", None)
    if search != None:
        if sort == "Name (A-Z)":
            try:
                re = "%" + search.lower() + "%"
                state_single = (
                    session.query(StateInstance)
                    .filter(
                        or_(
                            func.lower(StateInstance.name).like(re),
                            func.lower(StateInstance.nickname).like(re),
                            func.lower(StateInstance.capital).like(re),
                            func.lower(StateInstance.summary).like(re),
                        )
                    )
                    .order_by(StateInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Population Rank":
            try:
                re = "%" + search.lower() + "%"
                state_single = (
                    session.query(StateInstance)
                    .filter(
                        or_(
                            func.lower(StateInstance.name).like(re),
                            func.lower(StateInstance.nickname).like(re),
                            func.lower(StateInstance.capital).like(re),
                            func.lower(StateInstance.summary).like(re),
                        )
                    )
                    .order_by(StateInstance.popRank)
                )
            except:
                session.rollback()
        elif sort == "Area Rank":
            try:
                re = "%" + search.lower() + "%"
                state_single = (
                    session.query(StateInstance)
                    .filter(
                        or_(
                            func.lower(StateInstance.name).like(re),
                            func.lower(StateInstance.nickname).like(re),
                            func.lower(StateInstance.capital).like(re),
                            func.lower(StateInstance.summary).like(re),
                        )
                    )
                    .order_by(StateInstance.areaRank)
                )
            except:

                session.rollback()
        else:
            try:
                re = "%" + search.lower() + "%"
                state_single = session.query(StateInstance).filter(
                    or_(
                        func.lower(StateInstance.name).like(re),
                        func.lower(StateInstance.nickname).like(re),
                        func.lower(StateInstance.capital).like(re),
                        func.lower(StateInstance.summary).like(re),
                    )
                )
            except:
                session.rollback()
        state_single_dict = dict_creator(state_single)
        return json_response(state_single_dict)

    elif pop != None and area != None and density != None:
        minPop = -1
        maxPop = -1
        if pop == "small":
            minPop = 35
            maxPop = 51
        elif pop == "moderate":
            minPop = 18
            maxPop = 34
        elif pop == "large":
            minPop = 0
            maxPop = 17

        minArea = -1
        maxArea = -1
        if area == "small":
            minArea = 35
            maxArea = 51
        elif area == "moderate":
            minArea = 18
            maxArea = 34
        elif area == "large":
            minArea = 0
            maxArea = 17

        minDen = -1
        maxDen = -1
        if density == "small":
            minDen = 35
            maxDen = 51
        elif density == "moderate":
            minDen = 18
            maxDen = 34
        elif density == "large":
            minDen = 0
            maxDen = 17

        if sort == "Name (A-Z)":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.popRank.between(minPop, maxPop))
                    .filter(StateInstance.areaRank.between(minArea, maxArea))
                    .filter(StateInstance.densityRank.between(minDen, maxDen))
                    .order_by(StateInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Population Rank":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.popRank.between(minPop, maxPop))
                    .filter(StateInstance.areaRank.between(minArea, maxArea))
                    .filter(StateInstance.densityRank.between(minDen, maxDen))
                    .order_by(StateInstance.popRank)
                )
            except:
                session.rollback()
        elif sort == "Area Rank":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.popRank.between(minPop, maxPop))
                    .filter(StateInstance.areaRank.between(minArea, maxArea))
                    .filter(StateInstance.densityRank.between(minDen, maxDen))
                    .order_by(StateInstance.areaRank)
                )
            except:
                session.rollback()
        else:
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.popRank.between(minPop, maxPop))
                    .filter(StateInstance.areaRank.between(minArea, maxArea))
                    .filter(StateInstance.densityRank.between(minDen, maxDen))
                )
            except:
                session.rollback()
        state_single_dict = dict_creator(state_single)
    elif pop != None and area != None:
        minPop = -1
        maxPop = -1
        if pop == "small":
            minPop = 35
            maxPop = 51
        elif pop == "moderate":
            minPop = 18
            maxPop = 34
        elif pop == "large":
            minPop = 0
            maxPop = 17

        minArea = -1
        maxArea = -1
        if area == "small":
            minArea = 35
            maxArea = 51
        elif area == "moderate":
            minArea = 18
            maxArea = 34
        elif area == "large":
            minArea = 0
            maxArea = 17

        if sort == "Name (A-Z)":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.popRank.between(minPop, maxPop))
                    .filter(StateInstance.areaRank.between(minArea, maxArea))
                    .order_by(StateInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Population Rank":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.popRank.between(minPop, maxPop))
                    .filter(StateInstance.areaRank.between(minArea, maxArea))
                    .order_by(StateInstance.popRank)
                )
            except:
                session.rollback()
        elif sort == "Area Rank":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.popRank.between(minPop, maxPop))
                    .filter(StateInstance.areaRank.between(minArea, maxArea))
                    .order_by(StateInstance.areaRank)
                )
            except:
                session.rollback()
        else:
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.popRank.between(minPop, maxPop))
                    .filter(StateInstance.areaRank.between(minArea, maxArea))
                )
            except:
                session.rollback()
        state_single_dict = dict_creator(state_single)
    elif area != None and density != None:
        minArea = -1
        maxArea = -1
        if area == "small":
            minArea = 35
            maxArea = 51
        elif area == "moderate":
            minArea = 18
            maxArea = 34
        elif area == "large":
            minArea = 0
            maxArea = 17

        minDen = -1
        maxDen = -1
        if density == "small":
            minDen = 35
            maxDen = 51
        elif density == "moderate":
            minDen = 18
            maxDen = 34
        elif density == "large":
            minDen = 0
            maxDen = 17

        if sort == "Name (A-Z)":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.areaRank.between(minArea, maxArea))
                    .filter(StateInstance.densityRank.between(minDen, maxDen))
                    .order_by(StateInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Population Rank":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.areaRank.between(minArea, maxArea))
                    .filter(StateInstance.densityRank.between(minDen, maxDen))
                    .order_by(StateInstance.popRank)
                )
            except:
                session.rollback()
        elif sort == "Area Rank":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.areaRank.between(minArea, maxArea))
                    .filter(StateInstance.densityRank.between(minDen, maxDen))
                    .order_by(StateInstance.areaRank)
                )
            except:
                session.rollback()
        else:
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.areaRank.between(minArea, maxArea))
                    .filter(StateInstance.densityRank.between(minDen, maxDen))
                )
            except:
                session.rollback()
        state_single_dict = dict_creator(state_single)
    elif pop != None and density != None:
        minPop = -1
        maxPop = -1
        if pop == "small":
            minPop = 35
            maxPop = 51
        elif pop == "moderate":
            minPop = 18
            maxPop = 34
        elif pop == "large":
            minPop = 0
            maxPop = 17

        minDen = -1
        maxDen = -1
        if density == "small":
            minDen = 35
            maxDen = 51
        elif density == "moderate":
            minDen = 18
            maxDen = 34
        elif density == "large":
            minDen = 0
            maxDen = 17

        if sort == "Name (A-Z)":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.popRank.between(minPop, maxPop))
                    .filter(StateInstance.densityRank.between(minDen, maxDen))
                    .order_by(StateInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Population Rank":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.popRank.between(minPop, maxPop))
                    .filter(StateInstance.densityRank.between(minDen, maxDen))
                    .order_by(StateInstance.popRank)
                )
            except:
                session.rollback()
        elif sort == "Area Rank":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.popRank.between(minPop, maxPop))
                    .filter(StateInstance.densityRank.between(minDen, maxDen))
                    .order_by(StateInstance.areaRank)
                )
            except:
                session.rollback()
        else:
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.popRank.between(minPop, maxPop))
                    .filter(StateInstance.densityRank.between(minDen, maxDen))
                )
            except:
                session.rollback()
        state_single_dict = dict_creator(state_single)
    elif area != None:
        minArea = -1
        maxArea = -1
        if area == "small":
            minArea = 35
            maxArea = 51
        elif area == "moderate":
            minArea = 18
            maxArea = 34
        elif area == "large":
            minArea = 0
            maxArea = 17

        if sort == "Name (A-Z)":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.areaRank.between(minArea, maxArea))
                    .order_by(StateInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Population Rank":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.areaRank.between(minArea, maxArea))
                    .order_by(StateInstance.popRank)
                )
            except:
                session.rollback()
        elif sort == "Area Rank":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.areaRank.between(minArea, maxArea))
                    .order_by(StateInstance.areaRank)
                )
            except:
                session.rollback()
        else:
            try:
                state_single = session.query(StateInstance).filter(
                    StateInstance.areaRank.between(minArea, maxArea)
                )
            except:
                session.rollback()
        state_single_dict = dict_creator(state_single)
    elif pop != None:
        minPop = -1
        maxPop = -1
        if pop == "small":
            minPop = 35
            maxPop = 51
        elif pop == "moderate":
            minPop = 18
            maxPop = 34
        elif pop == "large":
            minPop = 0
            maxPop = 17

        if sort == "Name (A-Z)":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.popRank.between(minPop, maxPop))
                    .order_by(StateInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Population Rank":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.popRank.between(minPop, maxPop))
                    .order_by(StateInstance.popRank)
                )
            except:
                session.rollback()
        elif sort == "Area Rank":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.popRank.between(minPop, maxPop))
                    .order_by(StateInstance.areaRank)
                )
            except:
                session.rollback()
        else:
            try:
                state_single = session.query(StateInstance).filter(
                    StateInstance.popRank.between(minPop, maxPop)
                )
            except:
                session.rollback()
        state_single_dict = dict_creator(state_single)
    elif density != None:

        minDen = -1
        maxDen = -1
        if density == "small":
            minDen = 35
            maxDen = 51
        elif density == "moderate":
            minDen = 18
            maxDen = 34
        elif density == "large":
            minDen = 0
            maxDen = 17

        if sort == "Name (A-Z)":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.densityRank.between(minDen, maxDen))
                    .order_by(StateInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Population Rank":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.densityRank.between(minDen, maxDen))
                    .order_by(StateInstance.popRank)
                )
            except:
                session.rollback()
        elif sort == "Area Rank":
            try:
                state_single = (
                    session.query(StateInstance)
                    .filter(StateInstance.densityRank.between(minDen, maxDen))
                    .order_by(StateInstance.areaRank)
                )
            except:
                session.rollback()
        else:
            try:
                state_single = session.query(StateInstance).filter(
                    StateInstance.densityRank.between(minDen, maxDen)
                )
            except:
                session.rollback()
        state_single_dict = dict_creator(state_single)

    elif sort == "Name (A-Z)":
        try:
            state_single = session.query(StateInstance).order_by(StateInstance.name)
        except:
            session.rollback()
        state_single_dict = dict_creator(state_single)
    elif sort == "Population Rank":
        try:
            state_single = session.query(StateInstance).order_by(StateInstance.popRank)
        except:
            session.rollback()
        state_single_dict = dict_creator(state_single)
    elif sort == "Area Rank":
        try:
            state_single = session.query(StateInstance).order_by(StateInstance.areaRank)
        except:
            session.rollback()
        state_single_dict = dict_creator(state_single)
    return json_response(state_single_dict)


@app.route("/event/filter", methods=["GET"])
def index7():
    state_name = request.args.get("state_name", None)
    park_name = request.args.get("park_name", None)
    date = request.args.get("date", None)
    sort = request.args.get("sort", None)
    search = request.args.get("search", None)
    if search != None:
        if sort == "Name (A-Z)":
            try:
                re = "%" + search.lower() + "%"
                event_single = (
                    session.query(EventInstance)
                    .filter(
                        or_(
                            func.lower(EventInstance.name).like(re),
                            func.lower(EventInstance.states).like(re),
                            func.lower(EventInstance.park).like(re),
                            func.lower(EventInstance.summary).like(re),
                        )
                    )
                    .order_by(EventInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Date (Earliest to Latest)":
            try:
                re = "%" + search.lower() + "%"
                event_single = (
                    session.query(EventInstance)
                    .filter(
                        or_(
                            func.lower(EventInstance.name).like(re),
                            func.lower(EventInstance.states).like(re),
                            func.lower(EventInstance.park).like(re),
                            func.lower(EventInstance.summary).like(re),
                        )
                    )
                    .order_by(EventInstance.date[0])
                )
            except:
                session.rollback()
        else:
            try:
                re = "%" + search.lower() + "%"
                event_single = session.query(EventInstance).filter(
                    or_(
                        func.lower(EventInstance.name).like(re),
                        func.lower(EventInstance.states).like(re),
                        func.lower(EventInstance.park).like(re),
                        func.lower(EventInstance.summary).like(re),
                    )
                )
            except:
                session.rollback()
        event_single_dict = dict_creator(event_single)
        return json_response(event_single_dict)

    elif state_name != None and park_name != None and date != None:
        if sort == "Name (A-Z)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(states=state_name)
                    .filter_by(park=park_name)
                    .filter_by(date_single=date)
                    .order_by(EventInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Date (Earliest to Latest)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(states=state_name)
                    .filter_by(park=park_name)
                    .filter_by(date_single=date)
                    .order_by(EventInstance.date[0])
                )
            except:
                session.rollback()
        else:
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(states=state_name)
                    .filter_by(park=park_name)
                    .filter_by(date_single=date)
                )
            except:
                session.rollback()
        event_single_dict = dict_creator(event_single)

    elif state_name != None and park_name != None:
        if sort == "Name (A-Z)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(states=state_name)
                    .filter_by(park=park_name)
                    .order_by(EventInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Date (Earliest to Latest)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(states=state_name)
                    .filter_by(park=park_name)
                    .order_by(EventInstance.date[0])
                )
            except:
                session.rollback()
        else:
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(states=state_name)
                    .filter_by(park=park_name)
                )
            except:
                session.rollback()
        event_single_dict = dict_creator(event_single)
    elif state_name != None and park_name != None:
        if sort == "Name (A-Z)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(states=state_name)
                    .filter_by(park=park_name)
                    .order_by(EventInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Date (Earliest to Latest)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(states=state_name)
                    .filter_by(park=park_name)
                    .order_by(EventInstance.date[0])
                )
            except:
                session.rollback()
        else:
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(states=state_name)
                    .filter_by(park=park_name)
                )
            except:
                session.rollback()
        event_single_dict = dict_creator(event_single)
    elif state_name != None and date != None:
        if sort == "Name (A-Z)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(states=state_name)
                    .order_by(EventInstance.name)
                    .filter_by(date_single=date)
                )
            except:
                session.rollback()
        elif sort == "Date (Earliest to Latest)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(states=state_name)
                    .order_by(EventInstance.date[0])
                    .filter_by(date_single=date)
                )
            except:
                session.rollback()
        else:
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(states=state_name)
                    .filter_by(date_single=date)
                )
            except:
                session.rollback()
        event_single_dict = dict_creator(event_single)
    elif park_name != None and date != None:
        if sort == "Name (A-Z)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(park=park_name)
                    .order_by(EventInstance.name)
                    .filter_by(date_single=date)
                )
            except:
                session.rollback()
        elif sort == "Date (Earliest to Latest)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(park=park_name)
                    .order_by(EventInstance.date[0])
                    .filter_by(date_single=date)
                )
            except:
                session.rollback()
        else:
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(park=park_name)
                    .filter_by(date_single=date)
                )
            except:
                session.rollback()
        event_single_dict = dict_creator(event_single)

    elif state_name != None:
        if sort == "Name (A-Z)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(states=state_name)
                    .order_by(EventInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Date (Earliest to Latest)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(states=state_name)
                    .order_by(EventInstance.date[0])
                )
            except:
                session.rollback()
        else:
            try:
                event_single = session.query(EventInstance).filter_by(states=state_name)
            except:
                session.rollback()
        event_single_dict = dict_creator(event_single)
    elif park_name != None and date != None:
        if sort == "Name (A-Z)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(park=park_name)
                    .order_by(EventInstance.name)
                    .filter_by(date_single=date)
                )
            except:
                session.rollback()
        elif sort == "Date (Earliest to Latest)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(park=park_name)
                    .order_by(EventInstance.date[0])
                    .filter_by(date_single=date)
                )
            except:
                session.rollback()
        else:
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(park=park_name)
                    .filter_by(date_single=date)
                )
            except:
                session.rollback()
        event_single_dict = dict_creator(event_single)
    elif park_name != None:
        if sort == "Name (A-Z)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(park=park_name)
                    .order_by(EventInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Date (Earliest to Latest)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(park=park_name)
                    .order_by(EventInstance.date[0])
                )
            except:
                session.rollback()
        else:
            try:
                event_single = session.query(EventInstance).filter_by(park=park_name)
            except:
                session.rollback()
        event_single_dict = dict_creator(event_single)
    elif state_name != None:
        if sort == "Name (A-Z)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(states=state_name)
                    .order_by(EventInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Date (Earliest to Latest)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .filter_by(states=state_name)
                    .order_by(EventInstance.date[0])
                )
            except:
                session.rollback()
        else:
            try:
                event_single = session.query(EventInstance).filter_by(states=state_name)
            except:
                session.rollback()
        event_single_dict = dict_creator(event_single)
    elif sort == "Name (A-Z)":
        try:
            event_single = session.query(EventInstance).order_by(EventInstance.name)
        except:
            session.rollback()
        event_single_dict = dict_creator(event_single)
    elif date != None:
        if sort == "Name (A-Z)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .order_by(EventInstance.name)
                    .filter(EventInstance.date[0].like(date))
                )
            except:
                session.rollback()
        elif sort == "Date (Earliest to Latest)":
            try:
                event_single = (
                    session.query(EventInstance)
                    .order_by(EventInstance.date[0])
                    .filter_by(date_single=date)
                )
            except:
                session.rollback()
        else:
            try:
                event_single = session.query(EventInstance).filter_by(date_single=date)
            except:
                session.rollback()
        event_single_dict = dict_creator(event_single)
    elif sort == "Date (Earliest to Latest)":
        try:
            event_single = session.query(EventInstance).order_by(EventInstance.date)
        except:
            session.rollback()
        event_single_dict = dict_creator(event_single)

    return json_response(event_single_dict)


@app.route("/org/filter", methods=["GET"])
def index8():
    state_name = request.args.get("state_name", None)
    category = request.args.get("category", None)
    rating = request.args.get("rating", None)
    sort = request.args.get("sort", None)
    search = request.args.get("search", None)
    if search != None:
        if sort == "Name (A-Z)":
            try:
                re = "%" + search.lower() + "%"
                org_single = (
                    session.query(OrgInstance)
                    .filter(
                        or_(
                            func.lower(OrgInstance.name).like(re),
                            func.lower(OrgInstance.tagline).like(re),
                            func.lower(OrgInstance.mission).like(re),
                            func.lower(OrgInstance.state).like(re),
                            func.lower(OrgInstance.category).like(re),
                        )
                    )
                    .order_by(OrgInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Rating (Least to Greatest)":
            try:
                re = "%" + search.lower() + "%"
                org_single = (
                    session.query(OrgInstance)
                    .filter(
                        or_(
                            func.lower(OrgInstance.name).like(re),
                            func.lower(OrgInstance.tagline).like(re),
                            func.lower(OrgInstance.mission).like(re),
                            func.lower(OrgInstance.state).like(re),
                            func.lower(OrgInstance.category).like(re),
                        )
                    )
                    .order_by(OrgInstance.rating)
                )
            except:
                session.rollback()
        else:
            try:
                re = "%" + search.lower() + "%"
                org_single = session.query(OrgInstance).filter(
                    or_(
                        func.lower(OrgInstance.name).like(re),
                        func.lower(OrgInstance.tagline).like(re),
                        func.lower(OrgInstance.mission).like(re),
                        func.lower(OrgInstance.state).like(re),
                        func.lower(OrgInstance.category).like(re),
                    )
                )
            except:
                session.rollback()
        org_single_dict = dict_creator(org_single)
        return json_response(org_single_dict)

    elif state_name != None and category != None and rating != None:
        if sort == "Name (A-Z)":
            try:
                org_single = (
                    session.query(OrgInstance)
                    .filter_by(state=state_name)
                    .filter_by(category=category)
                    .filter_by(rating=rating)
                    .order_by(OrgInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Rating (Least to Greatest)":
            try:
                org_single = (
                    session.query(OrgInstance)
                    .filter_by(state=state_name)
                    .filter_by(category=category)
                    .filter_by(rating=rating)
                    .order_by(OrgInstance.rating)
                )
            except:
                session.rollback()
        else:
            try:
                org_single = (
                    session.query(OrgInstance)
                    .filter_by(state=state_name)
                    .filter_by(rating=rating)
                    .filter_by(category=category)
                )
            except:
                session.rollback()
        org_single_dict = dict_creator(org_single)
    elif state_name != None and rating != None:
        if sort == "Name (A-Z)":
            try:
                org_single = (
                    session.query(OrgInstance)
                    .filter_by(state=state_name)
                    .filter_by(rating=rating)
                    .order_by(OrgInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Rating (Least to Greatest)":
            try:
                org_single = (
                    session.query(OrgInstance)
                    .filter_by(state=state_name)
                    .filter_by(rating=rating)
                    .order_by(OrgInstance.rating)
                )
            except:
                session.rollback()
        else:
            try:
                org_single = (
                    session.query(OrgInstance)
                    .filter_by(state=state_name)
                    .filter_by(rating=rating)
                )
            except:
                session.rollback()
        org_single_dict = dict_creator(org_single)
    elif category != None and rating != None:
        if sort == "Name (A-Z)":
            try:
                org_single = (
                    session.query(OrgInstance)
                    .filter_by(category=category)
                    .filter_by(rating=rating)
                    .order_by(OrgInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Rating (Least to Greatest)":
            try:
                org_single = (
                    session.query(OrgInstance)
                    .filter_by(category=category)
                    .filter_by(rating=rating)
                    .order_by(OrgInstance.rating)
                )
            except:
                session.rollback()
        else:
            try:
                org_single = (
                    session.query(OrgInstance)
                    .filter_by(category=category)
                    .filter_by(rating=rating)
                )
            except:
                session.rollback()
        org_single_dict = dict_creator(org_single)
    elif state_name != None and category != None:
        if sort == "Name (A-Z)":
            try:
                org_single = (
                    session.query(OrgInstance)
                    .filter_by(state=state_name)
                    .filter_by(category=category)
                    .order_by(OrgInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Rating (Least to Greatest)":
            try:
                org_single = (
                    session.query(OrgInstance)
                    .filter_by(state=state_name)
                    .filter_by(category=category)
                    .order_by(OrgInstance.rating)
                )
            except:
                session.rollback()
        else:
            try:
                org_single = (
                    session.query(OrgInstance)
                    .filter_by(state=state_name)
                    .filter_by(category=category)
                )
            except:
                session.rollback()
        org_single_dict = dict_creator(org_single)
    elif state_name != None:
        if sort == "Name (A-Z)":
            try:
                org_single = (
                    session.query(OrgInstance)
                    .filter_by(state=state_name)
                    .order_by(OrgInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Rating (Least to Greatest)":
            try:
                org_single = (
                    session.query(OrgInstance)
                    .filter_by(state=state_name)
                    .order_by(OrgInstance.rating)
                )
            except:
                session.rollback()
        else:
            try:
                org_single = session.query(OrgInstance).filter_by(state=state_name)
            except:
                session.rollback()
        org_single_dict = dict_creator(org_single)
    elif category != None:
        if sort == "Name (A-Z)":
            try:
                org_single = (
                    session.query(OrgInstance)
                    .filter_by(category=category)
                    .order_by(OrgInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Rating (Least to Greatest)":
            try:
                org_single = (
                    session.query(OrgInstance)
                    .filter_by(category=category)
                    .order_by(OrgInstance.rating)
                )
            except:
                session.rollback()
        else:
            try:
                org_single = session.query(OrgInstance).filter_by(category=category)
            except:
                session.rollback()
        org_single_dict = dict_creator(org_single)
    elif rating != None:
        if sort == "Name (A-Z)":
            try:
                org_single = (
                    session.query(OrgInstance)
                    .filter_by(rating=rating)
                    .order_by(OrgInstance.name)
                )
            except:
                session.rollback()
        elif sort == "Rating (Least to Greatest)":
            try:
                org_single = (
                    session.query(OrgInstance)
                    .filter_by(rating=rating)
                    .order_by(OrgInstance.rating)
                )
            except:
                session.rollback()
        else:
            try:
                org_single = session.query(OrgInstance).filter_by(rating=rating)
            except:
                session.rollback()
        org_single_dict = dict_creator(org_single)
    elif sort == "Name (A-Z)":
        try:
            org_single = session.query(OrgInstance).order_by(OrgInstance.name)
        except:
            session.rollback()
        org_single_dict = dict_creator(org_single)
    elif sort == "Rating (Least to Greatest)":
        try:
            org_single = session.query(OrgInstance).order_by(OrgInstance.rating)
        except:
            session.rollback()
        org_single_dict = dict_creator(org_single)

    return json_response(org_single_dict)


def json_response(payload, status=200):
    return (
        json.dumps(payload),
        status,
        {"content-type": "application/json", "Access-Control-Allow-Origin": "*"},
    )


@app.errorhandler(500)
def server_error(e):
    logging.exception("An error occurred during a request.")
    return (
        """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(
            e
        ),
        500,
    )


if __name__ == "__main__":
    print("Starting")
    app.run("0.0.0.0", port=8080)
