import React from "react";
import Navbar from "./Components/NavBar.js";
import Footer from "./Components/Footer.js";
import Home from "./Pages/Home.js";
import ParksInstance from "./Pages/ParkInstance.js";
import StatesInstance from "./Pages/StateInstance.js";
import OrgsInstance from "./Pages/OrgInstance.js";
import EventsInstance from "./Pages/EventInstance.js";
import ParkList from "./Pages/ParkList.js";
import StateList from "./Pages/StateList.js";
import OrgList from "./Pages/OrgList.js";
import EventList from "./Pages/EventList.js";
import PlaceHolder from "./Pages/PlaceHolder.js";
import About from "./Pages/About.js";
import Search from "./Pages/Search.js";
import Visualization from "./Pages/Visuals.js";
import { Route, Switch, Redirect } from "react-router-dom";

export const Routes = () => {
  return (
    <div>
      <Navbar />
      <Switch>
        <Route exact path="/Home" component={Home} />
        <Route exact path="/">
          <Redirect to="/Home" />
        </Route>

        <Route exact path="/Parks" component={ParkList} />
        <Route path="/Parks/:name" component={ParksInstance} />

        <Route exact path="/States" component={StateList} />
        <Route path="/States/:name" component={StatesInstance} />

        <Route exact path="/Orgs" component={OrgList} />
        <Route path="/Orgs/:name" component={OrgsInstance} />

        <Route exact path="/Events" component={EventList} />
        <Route path="/Events/:name" component={EventsInstance} />

        <Route exact path="/About" component={About} />

        <Route exact path="/Search" component={Search} />
        <Route path="/Search/:name" component={Search} />

        <Route exact path="/Visualizations" component={Visualization} />

        <Route component={PlaceHolder} />
      </Switch>
      <Footer />
    </div>
  );
};
