import React, { Component } from "react";
import { TwitterTimelineEmbed } from 'react-twitter-embed';

class StateTwitterBox extends Component {
  render() {
    const { name } = this.props;
    return (
      <TwitterTimelineEmbed
      sourceType="profile"
      screenName={name}
      options={{height: 600, width: 800}}
    />
    );
  }
}
    
export default StateTwitterBox;