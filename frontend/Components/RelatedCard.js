import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";
import CardActionArea from "@material-ui/core/CardActionArea";

const contentContainer = {
  width: "400px",
};

const text = {
  textAlign: "center",
  fontSize: "1.25em",
  fontFamily: "raleway",
  color: "black",
};

const styles = {
  card: {
    padding: "0",
    maxWidth: "345",
    flexWrap: "wrap",
    marginLeft: "1vw",
    marginRight: "1vw",
  },
  title: {
    textAlign: "center",
    fontSize: "2em",
    fontFamily: "questrial",
    color: "black",
    paddingTop: "1vh",
  },
  media: {
    objectFit: "cover",
    marginTop: "2vh",
  },
};

function RelatedCard(props) {
  const { classes } = props;

  return (
    <Link to={props.link}>
      <Card className={classes.card}>
        <CardActionArea>
          <CardContent style={contentContainer}>
            <div className={classes.title}>{props.title}</div>
            <Typography noWrap={true} style={text}>{props.text}</Typography>
            <CardMedia
              component="img"
              className={classes.media}
              height="300"
              image={props.pic}
            />
          </CardContent>
        </CardActionArea>
      </Card>
    </Link>
  );
}

RelatedCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(RelatedCard);
