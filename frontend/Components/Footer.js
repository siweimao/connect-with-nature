import React from "react";
import { withStyles } from "@material-ui/core/styles";

const styles = {
    feet: {
        display: "flex",
        height: "25px",
        fontFamily: "questrial",
        alignItems: "center",
        justifyContent: "center",
        background: "#84b1aa",
        color: "rgba(255, 255, 255, .8)",
        width: '100%',
        fontSize: ".85em",
    }
};

function Footer(props) {
    const { classes } = props;
    return (
        <div className={classes.feet}>Connecting with Nature since 2019.</div>
    );
}

export default withStyles(styles)(Footer);