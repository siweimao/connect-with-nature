import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

const styles = {
  card: {
    marginRight: ".5vw",
    marginLeft: ".5vw",
  },
  header: {
    textAlign: "center",
    fontSize: "2em",
    fontFamily: "questrial",
  },
  hr: {
    height: "1px",
    width: "25%",
    borderTop: "1.75px solid black",
    marginTop: "0",
    marginBottom: ".75em",
  },
  content: {
    fontSize: "1.25em",
    fontFamily: "raleway",
    marginLeft: "1vw",
    marginRight: "1vw",
  },
};

function StateInfoCard(props) {
  const { classes } = props;

  return (
    <Card className={classes.card}>
      <CardContent>
        <div>
          <div className={classes.header}>Summary</div>
          <hr className={classes.hr}/>
          <div className={classes.content}>{props.summary}</div>
        </div>
      </CardContent>
    </Card>
  );
}

StateInfoCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(StateInfoCard);
