import React from "react";
import { Link } from 'react-router-dom';

function HomeSection(props) {
  const { img, header, text, link } = props;
  return (
    <section>
      <div className="container">
        <div className="row align-items-center">
          <div className="col-lg-6 order-lg-1">
            <div className="p-5">
              <Link to={link} style={{ color: '#000' }}>
              <h2 className="display-4 font-weight-normal">{header}</h2>
              </Link>
              <p className="lead">{text}</p>
            </div>
          </div>

          <div className="col-lg-6 order-lg-2">
            <div className="p-5">
              <img className="img-fluid rounded-circle" src={img} alt="" />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default HomeSection;
