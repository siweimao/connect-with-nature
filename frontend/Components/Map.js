import MapGL, { Marker } from "react-map-gl";
import React, { Component } from "react";
import CityPin from "./city-pin";

const TOKEN =
  "pk.eyJ1IjoidGludGluYXRvciIsImEiOiJjanUwNHRvaTkxbG9rNGRwZjBtNzY3emtuIn0.ojoNvsdfEtKbSeTzdNLb3w";

class Map extends Component {
  constructor(props) {
    super(props);

    this.state = {
      viewport: {
        latitude: 0,
        longitude: 0,
        zoom: 7,
        bearing: 0,
        pitch: 0
      }
    };
  }

  static getDerivedStateFromProps(props, state) {
    const { latitude, longitude } = props;

    if (
      state.viewport.latitude === 0 ||
      state.viewport.latitude === undefined
    ) {
      const newvp = { ...state };
      newvp.viewport.latitude = latitude;
      newvp.viewport.longitude = longitude;
      return newvp;
    } else return state;
  }

  _updateViewport = viewport => {
    this.setState({ viewport });
  };

  _renderCityMarker = (lat, long) => {
    return (
      <Marker key={`marker-1`} longitude={long} latitude={lat}>
        <CityPin size={20} onClick={() => console.log("asdf")} />
      </Marker>
    );
  };

  render() {
    const { longitude, latitude } = this.props;
    const { viewport } = this.state;
    return (
      <MapGL
        {...viewport}
        width={800}
        height={400}
        mapStyle={"mapbox://styles/mapbox/streets-v11"}
        onViewportChange={this._updateViewport}
        mapboxApiAccessToken={TOKEN}
      >
        {this._renderCityMarker(latitude, longitude)}
      </MapGL>
    );
  }
}

export default Map;
