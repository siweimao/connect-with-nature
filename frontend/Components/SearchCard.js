import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Highlighter from "react-highlight-words";
import { Link } from "react-router-dom";

const styles = {
  card: {
    height: "auto",
    width: "70vw",
    margin: "2vh",
    backgroundColor: "white",
    fontFamily: "Questrial"
  },
  font: {
    fontFamily: "Questrial"
  }
};

class SearchCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: this.props.item,
      query: this.props.query,
      searchText: this.props.searchText,
      link: this.props.link
    };
  }

  getNeighborText(str, queryStr) {
    var arr = str.split(" ");
    var matchArr = queryStr.split(" ");
    var begin = -1;
    var end = 0;
    var gap = 5;
    var subStrArr = [];
    var lastIndexBegin = -1;
    for (var i = 0; i < arr.length; i++) {
      for (var s in matchArr) {
        if (arr[i].toLowerCase().includes(matchArr[s].toLowerCase())) {
          if (begin === -1) {
            begin = i;
            end = i + gap;
          } else if (end + gap > i) {
            end = i + gap;
          } else {
            if (begin - gap >= 0) {
              subStrArr.push(arr.slice(begin - gap, end));
              lastIndexBegin = begin;
            } else {
              subStrArr.push(arr.slice(begin, end));
              lastIndexBegin = begin;
            }
            begin = i;
            end = i + gap;
          }
        }
      }
    }
    if (lastIndexBegin !== begin) {
      if (begin - gap >= 0) {
        subStrArr.push(arr.slice(begin - gap, end));
      } else {
        subStrArr.push(arr.slice(begin, end));
      }
    }
  }

  render() {
    const { classes, item, query, searchText, link } = this.props;
    if (searchText !== undefined && query !== undefined) {
      this.getNeighborText(searchText, query);
    }
    return (
      <Card className={classes.card}>
        <Link to={`${link}` + item.name}>
          <CardContent>
            <Typography className={classes.font} variant="body1">
              <b>{item.name}</b>
            </Typography>
            <Typography className={classes.font} variant="body1">
              {searchText !== undefined && (
                <Highlighter
                  searchWords={[query]}
                  textToHighlight={searchText}
                />
              )}
            </Typography>
          </CardContent>
        </Link>
      </Card>
    );
  }
}

SearchCard.propTypes = {
  data: PropTypes.object
};

export default withStyles(styles)(SearchCard);
