import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const styles = {
  card: {
    marginRight: ".5vw",
    marginLeft: ".5vw",
  },
  pos: {
    marginBottom: 12
  },
  header: {
    textAlign: "center",
    fontSize: "2em",
    fontFamily: "questrial",
  },
  centerContent: {
    textAlign: "center",
    fontSize: "1.25em",
    fontFamily: "raleway",
    marginBottom: "1.25em",
  },
  content: {
    fontSize: "1.25em",
    fontFamily: "raleway",
    marginBottom: "1.25em",
    marginLeft: "1vw",
    marginRight: "1vw",
  },
  hr: {
    height: "1px",
    width: "25%",
    borderTop: "1.75px solid black",
    marginTop: "0",
    marginBottom: ".75em",
  }
};

function OrgInfoCard(props) {
  const { classes } = props;

  return (
    <Card className={classes.card}>
      <CardContent>
        <div>
          <div className={classes.header}>State</div>
          <hr className={classes.hr}/>
          <div className={classes.centerContent}>{props.state}</div>
        </div>
        <div>
          <div className={classes.header}>Tagline</div>
          <hr className={classes.hr}/>
          <div className={classes.content}>{props.tagline}</div>
        </div>
        <div>
          <div className={classes.header}>Mission</div>
          <hr className={classes.hr}/>
          <div className={classes.content}>{props.mission}</div>
        </div>
      </CardContent>
    </Card>
  );
}

OrgInfoCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(OrgInfoCard);
