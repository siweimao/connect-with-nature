import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";

const styles = {
  card: {
    maxWidth: 300,
    margin: "1.5vw"
  },
  mediaContainer: {
    display: "flex",
    flexDirection: "column"
  },
  media: {
    objectFit: "contain",
    width: "auto",
    height: "20vh",
    margin: "3vw",
    padding: "1vh"
  },
  text: {
    display: "flex",
    justifyContent: "center",
    fontSize: "3vh"
  }
};

function FrontCard(props) {
  const { classes, title, img } = props;
  return (
    <Card className={classes.card}>
      <CardActionArea className={classes.mediaContainer}>
        <CardMedia
          component="img"
          alt="Contemplative Reptile"
          className={classes.media}
          image={img}
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography
            gutterBottom
            variant="h5"
            component="h2"
            className={classes.text}
          >
            {title}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

FrontCard.propTypes = {
  classes: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  img: PropTypes.string.isRequired
};

export default withStyles(styles)(FrontCard);
