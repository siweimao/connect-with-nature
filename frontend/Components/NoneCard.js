import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

const CardStyle = {
    fontFamily: "questrial",
    paddingBottom: "16px",
}

function NoneCard() {
    return (
        <Card>
            <CardContent style={CardStyle}>
                <div>No related models found.</div>
            </CardContent>
        </Card>
    );
  }

export default NoneCard;