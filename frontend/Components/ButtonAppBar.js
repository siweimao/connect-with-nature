import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

const styles = {
  root: {
    flexGrow: 1,
    flexShrink: 1
  },
  grow: {
    flexGrow: 1
  }
};
const bar = {
  backgroundColor: "#014421",
  colorPrimary: "red"
};

function ButtonAppBar(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <AppBar style={bar} position="static">
        <Toolbar>
          <Button
            style={bar}
            component={Link}
            to="/Home"
            color="inherit"
            className={classes.grow}
          >
            <Typography variant="h6" color="inherit">
              Connect With Nature~
            </Typography>
          </Button>
          <Button
            component={Link}
            to="/Parks"
            color="inherit"
            className={classes.grow}
          >
            <Typography variant="h6" color="inherit">
              Parks
            </Typography>
          </Button>
          <Button
            component={Link}
            to="/Events"
            color="inherit"
            className={classes.grow}
          >
            <Typography variant="h6" color="inherit">
              Events
            </Typography>
          </Button>
          <Button
            component={Link}
            to="/States"
            color="inherit"
            className={classes.grow}
          >
            <Typography variant="h6" color="inherit">
              States
            </Typography>
          </Button>
          <Button
            component={Link}
            to="/Orgs"
            color="inherit"
            className={classes.grow}
          >
            <Typography variant="h6" color="inherit">
              Organizations
            </Typography>
          </Button>
          <Button
            component={Link}
            to="/About"
            color="inherit"
            className={classes.grow}
          >
            <Typography variant="h6" color="inherit">
              About
            </Typography>
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}

ButtonAppBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ButtonAppBar);
