import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { Navbar, Nav, FormControl, Button, Form } from "react-bootstrap";
import { withRouter } from "react-router-dom";

const styles = {
  root: {
    backgroundColor: "#014421"
  },
  button: {
    fontFamily: "Questrial"
  },
  focus: {
    color: "white !important",
    fontFamily: "Questrial"
  }
};

class NavBar extends Component {
  constuctor() {
    this.routeChange = this.routeChange.bind(this);
  }

  state = {
    query: ""
  };

  handleSubmit = event => {
    event.preventDefault();
    this.setState({ query: event.target[0].value }, function() {
      this.routeChange(this.state.query);
    });
  };

  routeChange(query) {
    let path = "/Search/" + query;
    this.props.history.push(path);
    this.props.history.location.state = true;
  }

  render() {
    const { classes } = this.props;
    const highlight = window.location.href.split("/")[3];
    return (
      <Navbar className={classes.root} expand="lg" variant="dark">
        <Navbar.Brand className={classes.button} href="/Home">
          Connect With Nature
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="navbar-nav ml-auto">
            <Nav.Link
              className={highlight === "Parks" ? classes.focus : classes.button}
              href="/Parks"
            >
              Parks
            </Nav.Link>
            <Nav.Link
              className={
                highlight === "States" ? classes.focus : classes.button
              }
              href="/States"
            >
              States
            </Nav.Link>
            <Nav.Link
              className={
                highlight === "Events" ? classes.focus : classes.button
              }
              href="/Events"
            >
              Events
            </Nav.Link>
            <Nav.Link
              className={highlight === "Orgs" ? classes.focus : classes.button}
              href="/Orgs"
            >
              Organizations
            </Nav.Link>
            <Nav.Link
              className={
                highlight === "Visualizations" ? classes.focus : classes.button
              }
              href="/Visualizations"
            >
              Visualizations
            </Nav.Link>
            <Nav.Link
              className={highlight === "About" ? classes.focus : classes.button}
              href="/About"
            >
              About
            </Nav.Link>
          </Nav>
          <Form inline className={classes.button} onSubmit={this.handleSubmit}>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <Button
              className={classes.button}
              variant="outline-success"
              type="submit"
            >
              Go!
            </Button>
          </Form>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default withRouter(withStyles(styles)(NavBar));
