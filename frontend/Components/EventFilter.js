import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import {
  Dropdown,
  ButtonToolbar,
  Button,
  InputGroup,
  FormControl
} from "react-bootstrap";
import "../styles/Filter.css"

const styles = {
  parentContainer: {
    display: "flex",
    width: "65vw",
    height: "auto",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "center",
  },

  buttonContainer: {
    marginLeft: "1vw",
    display: "flex",
    // flexBasis: "100%",
    alignItems: "center",
    justifyContent: "center",
    // marginTop: "1.5vh",
  },

  searchField: {
    width: "10vw",
    minWidth: "200px",
    fontFamily: "Questrial",
  },

  questrial: {
    fontFamily: "Questrial",
  },

  goButton: {
    fontFamily: "Questrial",
    marginLeft: "0.25vw",
  },

  dropdownMenu: {
    height: "20vh",
    overflowY: "scroll"
  },
};

const states = {
  AL: "Alabama",
  AK: "Alaska",
  AZ: "Arizona",
  AR: "Arkansas",
  CA: "California",
  CO: "Colorado",
  CT: "Connecticut",
  DE: "Delaware",
  FL: "Florida",
  GA: "Georgia",
  HI: "Hawaii",
  ID: "Idaho",
  IL: "Illinois",
  IN: "Indiana",
  IA: "Iowa",
  KS: "Kansas",
  KY: "Kentucky",
  LA: "Louisiana",
  ME: "Maine",
  MD: "Maryland",
  MA: "Massachusetts",
  MI: "Michigan",
  MN: "Minnesota",
  MS: "Mississippi",
  MO: "Missouri",
  MT: "Montana",
  NE: "Nebraska",
  NV: "Nevada",
  NH: "New Hampshire",
  NJ: "New Jersey",
  NM: "New Mexico",
  NY: "New York",
  NC: "North Carolina",
  ND: "North Dakota",
  OH: "Ohio",
  OK: "Oklahoma",
  OR: "Oregon",
  PA: "Pennsylvania",
  RI: "Rhode Island",
  SC: "South Carolina",
  SD: "South Dakota",
  TN: "Tennessee",
  TX: "Texas",
  UT: "Utah",
  VT: "Vermont",
  VA: "Virginia",
  WA: "Washington",
  WV: "West Virginia",
  WI: "Wisconsin",
  WY: "Wyoming"
};

const parks = {
  1: "Yosemite National Park",
  2: "Saguaro National Park",
  3: "Joshua Tree National Park",
  4: "Hawaii Volcanoes National Park",
  5: "Hot Springs National Park",
  6: "Virgin Islands National Park",
  7: "Petrified Forest National Park",
  8: "Bryce Canyon National Park",
  9: "Channel Islands National Park",
  10: "Lassen Volcanic National Park",
  11: "Cuyahoga Valley National Park",
  12: "Rocky Mountain National Park",
  13: "Biscayne National Park",
  14: "Guadalupe Mountains National Park",
  15: "Canyonlands National Park",
  16: "Pinnacles National Park",
  17: "Indiana Dunes National Park",
  18: "Haleakala National Park",
  19: "Everglades National Park",
  20: "Gateway Arch National Park",
  21: "Death Valley National Park"
};
class EventFilter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      func: props.func,
      resetFunc: props.resetFunc,
      stateText: "Filter by State: ",
      parkText: "Filter by Park: ",
      eventText: "Filter by Date: ",
      sortText: "Sort By: ",
      searchText: "",
      searched: props.searched
    };
  }

  render() {
    const { classes, searched } = this.props;
    return (
      <div className={classes.parentContainer}>
        <InputGroup className={classes.searchField}>
          <FormControl
            placeholder="Search..."
            aria-label="Search..."
            aria-describedby="basic-addon2"
            onChange={item => {
              this.setState({ searchText: item.target.value });
            }}
            value={this.state.searchText}
          />
        </InputGroup>

        {!searched && (
        <ButtonToolbar className={classes.questrial}>
          <Dropdown>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              {this.state.stateText}
            </Dropdown.Toggle>
            <Dropdown.Menu className={classes.dropdownMenu}>
              {Object.values(states).map(value => {
                return (
                  <Dropdown.Item
                    key={value}
                    onClick={() => {
                      this.setState({ stateText: value });
                    }}
                  >
                    {value}
                  </Dropdown.Item>
                );
              })}
            </Dropdown.Menu>
          </Dropdown>
        </ButtonToolbar>)}

        {!searched && (
        <ButtonToolbar className={classes.questrial}>
          <Dropdown>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              {this.state.parkText}
            </Dropdown.Toggle>
            <Dropdown.Menu className={classes.dropdownMenu}>
              {Object.values(parks).map(value => {
                return (
                  <Dropdown.Item
                    key={value}
                    onClick={() => {
                      this.setState({ parkText: value });
                    }}
                  >
                    {value}
                  </Dropdown.Item>
                );
              })}
            </Dropdown.Menu>
          </Dropdown>
        </ButtonToolbar> )}

        {!searched && (
        <ButtonToolbar className={classes.questrial}>
          <Dropdown>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              {this.state.eventText}
            </Dropdown.Toggle>
            <Dropdown.Menu className={classes.questrial}>
              {["2019-03-26", "2019-03-27", "2019-03-28", "2019-03-29", "2019-03-30", "2019-03-31"].map(value => {
                return (
                  <Dropdown.Item
                    key={value}
                    onClick={() => {
                      this.setState({ eventText: value });
                    }}
                  >
                    {value}
                  </Dropdown.Item>
                );
              })}
            </Dropdown.Menu>
          </Dropdown>
        </ButtonToolbar> )}


        <ButtonToolbar className={classes.questrial}>
          <Dropdown>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              {this.state.sortText}
            </Dropdown.Toggle>
            <Dropdown.Menu className={classes.questrial}>
              {["Name (A-Z)", "Date (Earliest to Latest)"].map(value => {
                return (
                  <Dropdown.Item
                    key={value}
                    onClick={() => {
                      this.setState({ sortText: value });
                    }}
                  >
                    {value}
                  </Dropdown.Item>
                );
              })}
            </Dropdown.Menu>
          </Dropdown>
        </ButtonToolbar>
        <div className={classes.buttonContainer}>
          <Button
            className={classes.questrial}
            variant="info"
            onClick={() => {
              this.state.resetFunc();
              this.setState({
                stateText: "Filter by State: ",
                parkText: "Filter by Park: ",
                eventText: "Filter by Date: ",
                sortText: "Sort By: ",
                searchText: ""
              });
            }}
          >
            Reset
          </Button>

          <Button
            className={classes.goButton}
            variant="secondary"
            onClick={() => {
              this.state.func(
                this.state.stateText,
                this.state.parkText,
                this.state.eventText,
                this.state.sortText,
                this.state.searchText
              );
            }}
          >
            Go
          </Button>
        </div>
      </div>
    );
  }
}

EventFilter.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(EventFilter);
