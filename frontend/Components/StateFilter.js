import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import {
  Dropdown,
  ButtonToolbar,
  Button,
  InputGroup,
  FormControl
} from "react-bootstrap";
import "../styles/Filter.css"

const styles = {
  parentContainer: {
    display: "flex",
    width: "65vw",
    height: "auto",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "center",
  },

  buttonContainer: {
    marginLeft: "1vw",
    display: "flex",
    // flexBasis: "100%",
    alignItems: "center",
    justifyContent: "center",
    // marginTop: "1.5vh",
  },

  goButton: {
    fontFamily: "Questrial",
    marginLeft: "0.25vw",
  },

  searchField: {
    width: "10vw",
    minWidth: "200px",
    fontFamily: "Questrial",
  },

  questrial: {
    fontFamily: "Questrial",
  },
};

class StateFilter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      func: props.func,
      resetFunc: props.resetFunc,
      popText: "Filter by Population: ",
      areaText: "Filter by Area: ",
      densityText: "Filter by Density: ",
      sortText: "Sort By: ",
      searchText: "",
      searched : props.searched
    };
  }

  render() {
    const { classes, searched } = this.props;
    return (
      <div className={classes.parentContainer}>
        <InputGroup className={classes.searchField}>
          <FormControl
            placeholder="Search..."
            aria-label="Search..."
            aria-describedby="basic-addon2"
            onChange={item => {
              this.setState({ searchText: item.target.value });
            }}
            value={this.state.searchText}
          />
        </InputGroup>

        {!searched && (
        <ButtonToolbar className={classes.questrial}>
          <Dropdown>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              {this.state.popText}
            </Dropdown.Toggle>
            <Dropdown.Menu className={classes.questrial}>
              {["small", "moderate", "large"].map(value => {
                return (
                  <Dropdown.Item
                    key={value}
                    onClick={() => {
                      this.setState({ popText: value });
                    }}
                  >
                    {value}
                  </Dropdown.Item>
                );
              })}
            </Dropdown.Menu>
          </Dropdown>
        </ButtonToolbar> )}

        {!searched && (
        <ButtonToolbar className={classes.questrial}>
          <Dropdown>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              {this.state.areaText}
            </Dropdown.Toggle>
            <Dropdown.Menu className={classes.questrial}>
              {["small", "moderate", "large"].map(value => {
                return (
                  <Dropdown.Item
                    key={value}
                    onClick={() => {
                      this.setState({ areaText: value });
                    }}
                  >
                    {value}
                  </Dropdown.Item>
                );
              })}
            </Dropdown.Menu>
          </Dropdown>
        </ButtonToolbar>)}

        {!searched && (
        <ButtonToolbar className={classes.questrial}>
          <Dropdown>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              {this.state.densityText}
            </Dropdown.Toggle>
            <Dropdown.Menu className={classes.questrial}>
              {["small", "moderate", "large"].map(value => {
                return (
                  <Dropdown.Item
                    key={value}
                    onClick={() => {
                      this.setState({ densityText: value });
                    }}
                  >
                    {value}
                  </Dropdown.Item>
                );
              })}
            </Dropdown.Menu>
          </Dropdown>
        </ButtonToolbar>)}


        <ButtonToolbar className={classes.questrial}>
          <Dropdown>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              {this.state.sortText}
            </Dropdown.Toggle>
            <Dropdown.Menu className={classes.questrial}>
              {["Name (A-Z)", "Population Rank", "Area Rank"].map(value => {
                return (
                  <Dropdown.Item
                    key={value}
                    onClick={() => {
                      this.setState({ sortText: value });
                    }}
                  >
                    {value}
                  </Dropdown.Item>
                );
              })}
            </Dropdown.Menu>
          </Dropdown>
        </ButtonToolbar>
        <div className={classes.buttonContainer}>
          <Button
            className={classes.questrial}
            variant="info"
            onClick={() => {
              this.state.resetFunc();
              this.setState({
                popText: "Filter by Population: ",
                areaText: "Filter by Area: ",
                densityText: "Filter by Density: ",
                sortText: "Sort By: ",
                searchText: ""
              });
            }}
          >
            Reset
          </Button>

          <Button
            className={classes.goButton}
            variant="secondary"
            onClick={() => {
              this.state.func(
                this.state.popText,
                this.state.areaText,
                this.state.densityText,
                this.state.sortText,
                this.state.searchText
              );
            }}
          >
            Go
          </Button>
        </div>
      </div>
    );
  }
}

StateFilter.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(StateFilter);
