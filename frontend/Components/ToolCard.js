import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Tooltip from '@material-ui/core/Tooltip';

const styles = {
  cardStyle: {
    minWidth: 200,
    minHeight: 200,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "20vh",
    margin: "30px",
    backgroundColor: 'white',
    boxShadow: "none"
  },
  textStyle: {
    "&:hover": {
    color: 'white',
    fontFamily: "Questrial",
    }
  },

  title: {
    textAlign: "center",
    padding: "12px",
    fontFamily: "Questrial",
  },
  body: {
    textAlign: "center",
    fontSize: "16px",
    whiteSpace: 'pre-line',
    fontFamily: "Questrial",
  },
  image: {
    maxWidth: "100%",
    maxHeight: "100%",
    height: "auto",
    "&:hover": {
      backgroundColor: 'white'
    }
  },
  lightTooltip: {
    backgroundColor: "white",
    color: 'rgba(0, 0, 0, 0.87)',
    fontSize: 20,
    fontFamily: "Questrial",
  }
};

function DeveloperCard(props) {
  const { classes, name, text, image } = props;

  return (
    <Card className={classes.cardStyle}>
      <CardContent>
        <Tooltip title={text} placement="bottom" classes={{ tooltip: classes.lightTooltip }}>
          <img alt="" className={classes.image} src={image} />
        </Tooltip>
      </CardContent>
    </Card>
  );
}


DeveloperCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(DeveloperCard);
