import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const styles = {
  cardStyle: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "70vw",
    margin: "1vh",
    whiteSpace: "pre-line",
    fontFamily: "Questrial"
  },
  title: {
    textAlign: "center",
    padding: "12px",
    fontFamily: "Questrial",
    fontSize: "30px"
  },
  body: {
    textAlign: "center",
    fontSize: "16px",
    whiteSpace: "pre-line",
    fontFamily: "Questrial"
  },
  bodyDescription: {
    textAlign: "center",
    fontSize: "16px",
    whiteSpace: "pre-line",
    fontFamily: "Questrial"
  },
  content: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    whiteSpace: "pre-line",
    fontFamily: "Questrial"
  }
};

var id = 0;

function AboutCard(props) {
  const { classes, title, text, links } = props;

  return (
    <Card className={classes.cardStyle}>
      <CardContent className={classes.content}>
        {title !== undefined && (
          <Typography className={classes.title} variant="title">
            {title}
          </Typography>
        )}
        {text !== undefined && (
          <Typography className={classes.body} variant="body2">
            {text}
          </Typography>
        )}
        {links !== undefined &&
          links.map(cur => {
            const link = cur.split(": ");
            return (
              <a
                key={id++}
                href={link[1]}
                className={classes.bodyDescription}
                variant="body2"
              >
                {link[0]}
              </a>
            );
          })}
      </CardContent>
    </Card>
  );
}

AboutCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AboutCard);
