import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import {
  Dropdown,
  ButtonToolbar,
  Button,
  InputGroup,
  FormControl,
} from "react-bootstrap";
import "../styles/Filter.css"

const styles = {
  parentContainer: {
    display: "flex",
    width: "65vw",
    height: "auto",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "center",
  },

  buttonContainer: {
    marginLeft: "1vw",
    display: "flex",
    // flexBasis: "100%",
    alignItems: "center",
    justifyContent: "center",
    // marginTop: "1.5vh",
  },

  searchField: {
    width: "10vw",
    minWidth: "200px",
    fontFamily: "Questrial",
  },

  questrial: {
    fontFamily: "Questrial",
  },

  goButton: {
    fontFamily: "Questrial",
    marginLeft: "0.25vw",
  },

  dropdownMenu: {
    height: "20vh",
    overflowY: "scroll"
  },
};

const states = {
  AL: "Alabama",
  AK: "Alaska",
  AZ: "Arizona",
  AR: "Arkansas",
  CA: "California",
  CO: "Colorado",
  CT: "Connecticut",
  DE: "Delaware",
  FL: "Florida",
  GA: "Georgia",
  HI: "Hawaii",
  ID: "Idaho",
  IL: "Illinois",
  IN: "Indiana",
  IA: "Iowa",
  KS: "Kansas",
  KY: "Kentucky",
  LA: "Louisiana",
  ME: "Maine",
  MD: "Maryland",
  MA: "Massachusetts",
  MI: "Michigan",
  MN: "Minnesota",
  MS: "Mississippi",
  MO: "Missouri",
  MT: "Montana",
  NE: "Nebraska",
  NV: "Nevada",
  NH: "New Hampshire",
  NJ: "New Jersey",
  NM: "New Mexico",
  NY: "New York",
  NC: "North Carolina",
  ND: "North Dakota",
  OH: "Ohio",
  OK: "Oklahoma",
  OR: "Oregon",
  PA: "Pennsylvania",
  RI: "Rhode Island",
  SC: "South Carolina",
  SD: "South Dakota",
  TN: "Tennessee",
  TX: "Texas",
  UT: "Utah",
  VT: "Vermont",
  VA: "Virginia",
  WA: "Washington",
  WV: "West Virginia",
  WI: "Wisconsin",
  WY: "Wyoming"
};

class ParkFilter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      func: props.func,
      resetFunc: props.resetFunc,
      searched : props.searched,
      stateText: "Filter by State: ",
      feeText: "Filter by Fee: ",
      campText: "Filter by Campgrounds Size: ",
      sortText: "Sort By: ",
      searchText: ""
    };
  }

  render() {
    const { classes, searched } = this.props;
    return (
      <div className={classes.parentContainer}>
        <InputGroup className={classes.searchField}>
          <FormControl
            placeholder="Search..."
            aria-label="Search..."
            aria-describedby="basic-addon2"
            onChange={item => {
              this.setState({ searchText: item.target.value });
            }}
            value={this.state.searchText}
          />
        </InputGroup>

        {!searched && (
        <ButtonToolbar className={classes.questrial}>
          <Dropdown>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              {this.state.stateText}
            </Dropdown.Toggle>
            <Dropdown.Menu className={classes.dropdownMenu}>
              {Object.values(states).map(value => {
                return (
                  <Dropdown.Item
                    className={classes.questrial}
                    key={value}
                    onClick={() => {
                      this.setState({ stateText: value });
                    }}
                  >
                    {value}
                  </Dropdown.Item>
                );
              })}
            </Dropdown.Menu>
          </Dropdown>
        </ButtonToolbar> )}

        {!searched && (
        <ButtonToolbar className={classes.questrial}>
          <Dropdown>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              {this.state.feeText}
            </Dropdown.Toggle>
            <Dropdown.Menu>
              {["Low", "Medium", "High"].map(value => {
                return (
                  <Dropdown.Item
                    className={classes.questrial}
                    key={value}
                    onClick={() => {
                      this.setState({ feeText: value });
                    }}
                  >
                    {value}
                  </Dropdown.Item>
                );
              })}
            </Dropdown.Menu>
          </Dropdown>
        </ButtonToolbar>)}

        {!searched && (
        <ButtonToolbar className={classes.questrial}>
          <Dropdown>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              {this.state.campText}
            </Dropdown.Toggle>
            <Dropdown.Menu>
              {["Small", "Moderate", "Large"].map(value => {
                return (
                  <Dropdown.Item
                    className={classes.questrial}
                    key={value}
                    onClick={() => {
                      this.setState({ campText: value });
                    }}
                  >
                    {value}
                  </Dropdown.Item>
                );
              })}
            </Dropdown.Menu>
          </Dropdown>
        </ButtonToolbar>)}

        <ButtonToolbar className={classes.questrial}>
          <Dropdown>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              {this.state.sortText}
            </Dropdown.Toggle>
            <Dropdown.Menu>
              {[
                "Name (A-Z)",
                "Number of Campsites (Least to Greatest)",
                "Fee (Low to High)"
              ].map(value => {
                return (
                  <Dropdown.Item
                    className={classes.questrial}
                    key={value}
                    onClick={() => {
                      this.setState({ sortText: value });
                    }}
                  >
                    {value}
                  </Dropdown.Item>
                );
              })}
            </Dropdown.Menu>
          </Dropdown>
        </ButtonToolbar>
          
        <div className={classes.buttonContainer}>
          <Button
            className={classes.questrial}
            variant="info"
            onClick={() => {
              this.state.resetFunc();
              this.setState({
                stateText: "Filter by State: ",
                feeText: "Filter by Fee: ",
                sortText: "Sort By: ",
                campText: "Filter by Campgrounds Size: ",
                searchText: ""
              });
            }}
          >
            Reset
          </Button>

          <Button
            className={classes.goButton}
            variant="secondary"
            onClick={() => {
              this.state.func(
                this.state.stateText,
                this.state.feeText,
                this.state.campText,
                this.state.sortText,
                this.state.searchText
              );
            }}
          >
            Go
          </Button>
        </div>

        
      </div>

    );
  }
}

ParkFilter.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ParkFilter);
