import React, { Component } from "react";
import { TwitterTimelineEmbed } from 'react-twitter-embed';

class TwitterBox extends Component {
  render() {
    var park = require('./nationalParkTwitter.json');
    const { name } = this.props;
    var parkHandle = park[name];
    return (
      <TwitterTimelineEmbed
      sourceType="profile"
      screenName={parkHandle}
      options={{height: 600, width: 800}}
    />
    );
  }
}
    
export default TwitterBox;