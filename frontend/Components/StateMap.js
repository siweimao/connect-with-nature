import React, {Component} from 'react';
import * as d3 from "d3";
import uStates from './uStates';
import './StatesMap.css';
class StateMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: null,
      isLoaded: false,
      parkStateDict: props.parkData,
      orgStateDict: props.orgData,
      eventStateDict: props.eventData,
      first: props.first,
      second: props.second,
      third: props.third
    };
    this.drawChart = this.drawChart.bind(this);
  }

  componentDidMount() {
    this.setState({
        isLoaded: true,
      });
  }

  drawChart() {
    const { parkStateDict, orgStateDict, eventStateDict, first, second, third } = this.state;
    function tooltipHtml(n, d){ /* function to create html content string in tooltip div. */
      return "<h4>"+n+"</h4><table>"+
        "<tr><td>" + (first) + "</td><td>"+(d.parks)+"</td></tr>"+
        "<tr><td>" + (second) + "</td><td>"+(d.orgs)+"</td></tr>"+
        "<tr><td>" + (third) + "</td><td>"+(d.events)+"</td></tr>"+
        "</table>";
    }
    var sampleData ={};
    ["HI", "AK", "FL", "SC", "GA", "AL", "NC", "TN", "RI", "CT", "MA",
    "ME", "NH", "VT", "NY", "NJ", "PA", "DE", "MD", "WV", "KY", "OH",
    "MI", "WY", "MT", "ID", "WA", "DC", "TX", "CA", "AZ", "NV", "UT",
    "CO", "NM", "OR", "ND", "SD", "NE", "IA", "MS", "IN", "IL", "MN",
    "WI", "MO", "AR", "OK", "KS", "LA", "VA"]
      .forEach(function(d){
        var p = parkStateDict[d];
        var o = orgStateDict[d];
        var e = eventStateDict[d];

        sampleData[d]={parks: p, orgs: o, events: e,
                       color:d3.interpolate("#c7eaab", "#164406")((p+o+(e / 15))/5)};
      });

    /* draw states on id #statesvg */
    uStates.draw("#statesvg", sampleData, tooltipHtml);

    d3.select(window.frameElement).style("height", "600px");
  }
  render() {
    if(this.state.isLoaded){
      this.drawChart();
    }
    return (
      <div className="container">
        <div id="tooltip"></div>
        <svg width="960" height="600" id="statesvg"></svg>
      </div>
    );

  }
}
export default StateMap;
