import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import ListInfoCard from "../Components/ListInfoCard.js";
import Pagination from "../Components/Pagination.js";
import OrgFilter from "../Components/OrgFilter.js";
import OrgSearchList from "./OrgSearchList";
import axios from "axios";

const statesToAbbrev = {
  Alabama: "AL",
  Alaska: "AK",
  Arizona: "AZ",
  Arkansas: "AR",
  California: "CA",
  Colorado: "CO",
  Connecticut: "CT",
  Delaware: "DE",
  Florida: "FL",
  Georgia: "GA",
  Hawaii: "HI",
  Idaho: "ID",
  Illinois: "IL",
  Indiana: "IN",
  Iowa: "IA",
  Kansas: "KS",
  Kentucky: "KY",
  Louisiana: "LA",
  Maine: "ME",
  Maryland: "MD",
  Massachusetts: "MA",
  Michigan: "MI",
  Minnesota: "MN",
  Mississippi: "MS",
  Missouri: "MO",
  Montana: "MT",
  Nebraska: "NE",
  Nevada: "NV",
  "New Hampshire": "NH",
  "New Jersey": "NJ",
  "New Mexico": "NW",
  "New York": "NY",
  "North Carolina": "NC",
  "North Dakota": "ND",
  Ohio: "OH",
  Oklahoma: "OK",
  Oregon: "OR",
  Pennsylvania: "PA",
  "Rhode Island": "RI",
  "South Carolina": "SC",
  "South Dakota": "SD",
  Tennessee: "TN",
  Texas: "TX",
  Utah: "UT",
  Vermont: "VT",
  Virginia: "VA",
  Washington: "WA",
  "West Virginia": "WV",
  Wisconsin: "WI",
  Wyoming: "WY"
};

function FormRow(props) {
  const data = props;
  if (data === undefined || data.data.length === 0) {
    return <div />;
  } else {
    const length = data.data.length;

    return (
      <React.Fragment>
        <Grid container spacing={40}>
          {length > 0 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Orgs/" + data.data[0].name}
                coverName={data.data[0].name}
                coverDescript="Dedicated to research, education and conservation, and display of California native plants"
                coverImage={data.data[0].pics}
                attribute1="Name"
                attribute1val={data.data[0].name}
                attribute2="Tagline"
                attribute2val={data.data[0].tagline}
                attribute3="Category"
                attribute3val={data.data[0].category}
                attribute4="State"
                attribute4val={data.data[0].state}
                attribute5="Rating"
                attribute5val={data.data[0].rating}
              />
            </Grid>
          )}
          {length > 1 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Orgs/" + data.data[1].name}
                coverName={data.data[1].name}
                coverDescript="Dedicated to research, education and conservation, and display of California native plants"
                coverImage={data.data[1].pics}
                attribute1="Name"
                attribute1val={data.data[1].name}
                attribute2="Tagline"
                attribute2val={data.data[1].tagline}
                attribute3="Category"
                attribute3val={data.data[1].category}
                attribute4="State"
                attribute4val={data.data[1].state}
                attribute5="Rating"
                attribute5val={data.data[1].rating}
              />
            </Grid>
          )}
          {length > 2 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Orgs/" + data.data[2].name}
                coverName={data.data[2].name}
                coverDescript="Dedicated to research, education and conservation, and display of California native plants"
                coverImage={data.data[2].pics}
                attribute1="Name"
                attribute1val={data.data[2].name}
                attribute2="Tagline"
                attribute2val={data.data[2].tagline}
                attribute3="Category"
                attribute3val={data.data[2].category}
                attribute4="State"
                attribute4val={data.data[2].state}
                attribute5="Rating"
                attribute5val={data.data[2].rating}
              />
            </Grid>
          )}
          {length > 3 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Orgs/" + data.data[3].name}
                coverName={data.data[3].name}
                coverDescript="Dedicated to research, education and conservation, and display of California native plants"
                coverImage={data.data[3].pics}
                attribute1="Name"
                attribute1val={data.data[3].name}
                attribute2="Tagline"
                attribute2val={data.data[3].tagline}
                attribute3="Category"
                attribute3val={data.data[3].category}
                attribute4="State"
                attribute4val={data.data[3].state}
                attribute5="Rating"
                attribute5val={data.data[3].rating}
              />
            </Grid>
          )}
          {length > 4 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Orgs/" + data.data[4].name}
                coverName={data.data[4].name}
                coverDescript="Dedicated to research, education and conservation, and display of California native plants"
                coverImage={data.data[4].pics}
                attribute1="Name"
                attribute1val={data.data[4].name}
                attribute2="Tagline"
                attribute2val={data.data[4].tagline}
                attribute3="Category"
                attribute3val={data.data[4].category}
                attribute4="State"
                attribute4val={data.data[4].state}
                attribute5="Rating"
                attribute5val={data.data[4].rating}
              />
            </Grid>
          )}
          {length > 5 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Orgs/" + data.data[5].name}
                coverName={data.data[5].name}
                coverDescript="Dedicated to research, education and conservation, and display of California native plants"
                coverImage={data.data[5].pics}
                attribute1="Name"
                attribute1val={data.data[5].name}
                attribute2="Tagline"
                attribute2val={data.data[5].tagline}
                attribute3="Category"
                attribute3val={data.data[5].category}
                attribute4="State"
                attribute4val={data.data[5].state}
                attribute5="Rating"
                attribute5val={data.data[5].rating}
              />
            </Grid>
          )}
          {length > 6 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Orgs/" + data.data[6].name}
                coverName={data.data[6].name}
                coverDescript="Dedicated to research, education and conservation, and display of California native plants"
                coverImage={data.data[6].pics}
                attribute1="Name"
                attribute1val={data.data[6].name}
                attribute2="Tagline"
                attribute2val={data.data[6].tagline}
                attribute3="Category"
                attribute3val={data.data[6].category}
                attribute4="State"
                attribute4val={data.data[6].state}
                attribute5="Rating"
                attribute5val={data.data[6].rating}
              />
            </Grid>
          )}
          {length > 7 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Orgs/" + data.data[7].name}
                coverName={data.data[7].name}
                coverDescript="Dedicated to research, education and conservation, and display of California native plants"
                coverImage={data.data[7].pics}
                attribute1="Name"
                attribute1val={data.data[7].name}
                attribute2="Tagline"
                attribute2val={data.data[7].tagline}
                attribute3="Category"
                attribute3val={data.data[7].category}
                attribute4="State"
                attribute4val={data.data[7].state}
                attribute5="Rating"
                attribute5val={data.data[7].rating}
              />
            </Grid>
          )}
          {length > 8 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Orgs/" + data.data[8].name}
                coverName={data.data[8].name}
                coverDescript="Dedicated to research, education and conservation, and display of California native plants"
                coverImage={data.data[8].pics}
                attribute1="Name"
                attribute1val={data.data[8].name}
                attribute2="Tagline"
                attribute2val={data.data[8].tagline}
                attribute3="Category"
                attribute3val={data.data[8].category}
                attribute4="State"
                attribute4val={data.data[8].state}
                attribute5="Rating"
                attribute5val={data.data[8].rating}
              />
            </Grid>
          )}
        </Grid>
      </React.Fragment>
    );
  }
}

const styles = {
  root: {
    paddingTop: "4vh",
    paddingBottom: "5vh",
    flexGrow: 1,
    height: "inherit",
    width: "90vw"
  },
  container: {
    backgroundColor: "#ebfaeb",
    paddingTop: "3.25vh",
    minHeight: "100vh",
    height: "auto",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  pageNumber: {
    width: "100%",
    textAlign: "center",
    fontFamily: "questrial",
    fontSize: "1.25em",
    marginBottom: "2vh"
  },
  banner: {
    display: "flex",
    height: "14vh",
    fontFamily: "questrial",
    alignItems: "center",
    justifyContent: "center",
    color: "black",
    width: "100%",
    fontSize: "3.5em"
  },
  bannerSearch: {
    display: "flex",
    height: "6vh",
    fontFamily: "questrial",
    alignItems: "center",
    justifyContent: "center",
    color: "black",
    width: "100%",
    fontSize: "1.5em"
  }
};

class OrgList extends Component {
  state = {
    allOrgs: [],
    currentOrgs: [],
    currentPage: null,
    totalPages: null,
    filtered: false,
    sorted: false,
    searched: false
  };

  filterBy = (stateName, category, org, sort, search) => {
    this.setState({ searched: false });
    this.setState({ searchText: search });
    var query = "https://api.connectwithnature.me/org";
    var list = [];
    var fil = false;
    var srt = false;
    var srch = false;

    if (
      stateName !== "Filter by State: " ||
      category !== "Filter by Category: " ||
      sort !== "Sort By: " ||
      org !== "Filter by Rating: " ||
      search !== ""
    ) {
      query += "/filter";
      if (stateName !== "Filter by State: ") {
        fil = true;
        if (list.length === 0)
          list.push(`?state_name=${statesToAbbrev[stateName]}`);
        else list.push(`&state_name=${statesToAbbrev[stateName]}`);
      }
      if (category !== "Filter by Category: ") {
        fil = true;
        if (list.length === 0) list.push(`?category=${category}`);
        else list.push(`&category=${category}`);
      }
      if (org !== "Filter by Rating: ") {
        fil = true;
        if (list.length === 0) list.push(`?rating=${org}`);
        else list.push(`&rating=${org}`);
      }
      if (sort !== "Sort By: ") {
        srt = true;
        if (list.length === 0) list.push(`?sort=${sort}`);
        else list.push(`&sort=${sort}`);
      }
      if (search !== "") {
        srch = true;
        if (list.length === 0) list.push(`?search=${search}`);
        else list.push(`&search=${search}`);
      }
    }

    list.forEach(x => {
      query += x;
    });

    axios
      .all([axios.get(query)])
      .then(
        axios.spread(orgFilterResponse => {
          this.setState({
            currentOrgs: orgFilterResponse.data,
            allOrgs: orgFilterResponse.data
          });
        })
      )
      .then(filter =>
        filter || fil
          ? this.setState({ filtered: true })
          : console.log("filter not set")
      )
      .then(sort =>
        sort || srt
          ? this.setState({ sorted: true })
          : console.log("sort not set")
      )
      .then(search =>
        search || srch
          ? this.setState({ searched: true })
          : console.log("search not set")
      )
      .then(this.onPageChangedFilter());
  };

  scrapeProjectInfo = () => {
    axios.all([axios.get("https://api.connectwithnature.me/org")]).then(
      axios.spread(orgResponse => {
        this.setState({
          allOrgs: orgResponse.data,
          currentParks: orgResponse.data,
          filtered: false,
          sorted: false,
          searched: false
        });
      })
    );
  };

  componentDidMount() {
    this.scrapeProjectInfo();
  }

  onPageChangedFilter() {
    const { allOrgs } = this.state;
    const pageLimit = this.pageLimit;
    const currentPage = 1;
    const totalPages = Math.ceil(this.totalRecords / this.pageLimit);

    const offset = (currentPage - 1) * pageLimit;
    const currentOrgs = allOrgs.slice(offset, offset + pageLimit);

    this.setState({ currentPage, currentOrgs, totalPages });
  }

  onPageChanged = data => {
    const { allOrgs } = this.state;
    const { currentPage, totalPages, pageLimit } = data;

    const offset = (currentPage - 1) * pageLimit;
    const currentOrgs = allOrgs.slice(offset, offset + pageLimit);

    this.setState({ currentPage, currentOrgs, totalPages });
  };

  render() {
    window.scrollTo(0, 0);
    const { classes } = this.props;
    const { allOrgs, currentOrgs, filtered, searched, searchText } = this.state;
    return (
      <div className={classes.container}>
        <div className={classes.banner}>Organizations</div>
        <hr />
        <OrgFilter
          className={classes.dropdown}
          func={this.filterBy}
          resetFunc={this.scrapeProjectInfo}
          searched={searched}
        />

        {searched && <OrgSearchList allOrgs={currentOrgs} query={searchText} />}

        {!searched && (
          <Grid container className={classes.root}>
            <Grid container item xs={12}>
              <div className={classes.pageNumber}>
                Page {this.state.currentPage} / {Math.ceil(allOrgs.length/9) == 0 ? 1 : Math.ceil(allOrgs.length/9)}
              </div>
              <FormRow className={classes.items} data={currentOrgs} />
            </Grid>
          </Grid>
        )}

        {allOrgs.length !== 0 && !filtered && !searched && (
          <Pagination
            className={classes.pagination}
            totalRecords={allOrgs.length}
            pageLimit={9}
            onPageChanged={this.onPageChanged}
          />
        )}

        {allOrgs.length !== 0 && filtered && !searched && (
          <Pagination
            className={classes.pagination}
            totalRecords={allOrgs.length}
            pageLimit={9}
            onPageChanged={this.onPageChanged}
          />
        )}
      </div>
    );
  }
}

OrgList.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(OrgList);
