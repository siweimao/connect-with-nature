import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import ListInfoCard from "../Components/ListInfoCard.js";
import Pagination from "../Components/Pagination.js";
import EventFilter from "../Components/EventFilter.js";
import EventSearchList from "./EventSearchList";
import axios from "axios";

const styles = {
  root: {
    paddingTop: "4vh",
    paddingBottom: "5vh",
    flexGrow: 1,
    height: "inherit",
    width: "90vw"
  },
  container: {
    backgroundColor: "#ebfaeb",
    paddingTop: "3.25vh",
    minHeight: "100vh",
    height: "auto",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  banner: {
    display: "flex",
    height: "14vh",
    fontFamily: "questrial",
    alignItems: "center",
    justifyContent: "center",
    color: "black",
    width: "100%",
    fontSize: "3.5em"
  },
  pageNumber: {
    width: "100%",
    textAlign: "center",
    fontFamily: "questrial",
    fontSize: "1.25em",
    marginBottom: "2vh"
  },
  bannerSearch: {
    display: "flex",
    height: "6vh",
    fontFamily: "questrial",
    alignItems: "center",
    justifyContent: "center",
    color: "black",
    width: "100%",
    fontSize: "1.5em"
  }
};

const statesToAbbrev = {
  Alabama: "AL",
  Alaska: "AK",
  Arizona: "AZ",
  Arkansas: "AR",
  California: "CA",
  Colorado: "CO",
  Connecticut: "CT",
  Delaware: "DE",
  Florida: "FL",
  Georgia: "GA",
  Hawaii: "HI",
  Idaho: "ID",
  Illinois: "IL",
  Indiana: "IN",
  Iowa: "IA",
  Kansas: "KS",
  Kentucky: "KY",
  Louisiana: "LA",
  Maine: "ME",
  Maryland: "MD",
  Massachusetts: "MA",
  Michigan: "MI",
  Minnesota: "MN",
  Mississippi: "MS",
  Missouri: "MO",
  Montana: "MT",
  Nebraska: "NE",
  Nevada: "NV",
  "New Hampshire": "NH",
  "New Jersey": "NJ",
  "New Mexico": "NW",
  "New York": "NY",
  "North Carolina": "NC",
  "North Dakota": "ND",
  Ohio: "OH",
  Oklahoma: "OK",
  Oregon: "OR",
  Pennsylvania: "PA",
  "Rhode Island": "RI",
  "South Carolina": "SC",
  "South Dakota": "SD",
  Tennessee: "TN",
  Texas: "TX",
  Utah: "UT",
  Vermont: "VT",
  Virginia: "VA",
  Washington: "WA",
  "West Virginia": "WV",
  Wisconsin: "WI",
  Wyoming: "WY"
};

function FormRow(props) {
  const data = props;
  if (data === undefined || data.data.length === 0) {
    return <div />;
  } else {
    const length = data.data.length;
    return (
      <React.Fragment>
        <Grid container spacing={40}>
          {length > 0 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"/Events/" + data.data[0].name}
                coverName={data.data[0].name}
                coverDescript="potate"
                coverImage={data.data[0].pics[0]}
                attribute1="Dates"
                attribute1val={data.data[0].date[0]}
                attribute2="Time"
                attribute2val={data.data[0].time}
                attribute3="Location"
                attribute3val={data.data[0].location}
                attribute4="National Park"
                attribute4val={data.data[0].park}
                attribute5="Fee Information"
                attribute5val={data.data[0].fee}
              />
            </Grid>
          )}
          {length > 1 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"/Events/" + data.data[1].name}
                coverName={data.data[1].name}
                coverDescript="potate"
                coverImage={data.data[1].pics[0]}
                attribute1="Dates"
                attribute1val={data.data[1].date[0]}
                attribute2="Time"
                attribute2val={data.data[1].time}
                attribute3="Location"
                attribute3val={data.data[1].location}
                attribute4="National Park"
                attribute4val={data.data[1].park}
                attribute5="Fee Information"
                attribute5val={data.data[1].fee}
              />
            </Grid>
          )}
          {length > 2 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"/Events/" + data.data[2].name}
                coverName={data.data[2].name}
                coverDescript="potate"
                coverImage={data.data[2].pics[0]}
                attribute1="Dates"
                attribute1val={data.data[2].date[0]}
                attribute2="Time"
                attribute2val={data.data[2].time}
                attribute3="Location"
                attribute3val={data.data[2].location}
                attribute4="National Park"
                attribute4val={data.data[2].park}
                attribute5="Fee Information"
                attribute5val={data.data[2].fee}
              />
            </Grid>
          )}
          {length > 3 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"/Events/" + data.data[3].name}
                coverName={data.data[3].name}
                coverDescript="potate"
                coverImage={data.data[3].pics[0]}
                attribute1="Dates"
                attribute1val={data.data[3].date[0]}
                attribute2="Time"
                attribute2val={data.data[3].time}
                attribute3="Location"
                attribute3val={data.data[3].location}
                attribute4="National Park"
                attribute4val={data.data[3].park}
                attribute5="Fee Information"
                attribute5val={data.data[3].fee}
              />
            </Grid>
          )}
          {length > 4 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"/Events/" + data.data[4].name}
                coverName={data.data[4].name}
                coverDescript="potate"
                coverImage={data.data[4].pics[0]}
                attribute1="Dates"
                attribute1val={data.data[4].date[0]}
                attribute2="Time"
                attribute2val={data.data[4].time}
                attribute3="Location"
                attribute3val={data.data[4].location}
                attribute4="National Park"
                attribute4val={data.data[4].park}
                attribute5="Fee Information"
                attribute5val={data.data[4].fee}
              />
            </Grid>
          )}
          {length > 5 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"/Events/" + data.data[5].name}
                coverName={data.data[5].name}
                coverDescript="potate"
                coverImage={data.data[5].pics[0]}
                attribute1="Dates"
                attribute1val={data.data[5].date[0]}
                attribute2="Time"
                attribute2val={data.data[5].time}
                attribute3="Location"
                attribute3val={data.data[5].location}
                attribute4="National Park"
                attribute4val={data.data[5].park}
                attribute5="Fee Information"
                attribute5val={data.data[5].fee}
              />
            </Grid>
          )}
          {length > 6 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"/Events/" + data.data[6].name}
                coverName={data.data[6].name}
                coverDescript="potate"
                coverImage={data.data[6].pics[0]}
                attribute1="Dates"
                attribute1val={data.data[6].date[0]}
                attribute2="Time"
                attribute2val={data.data[6].time}
                attribute3="Location"
                attribute3val={data.data[6].location}
                attribute4="National Park"
                attribute4val={data.data[6].park}
                attribute5="Fee Information"
                attribute5val={data.data[6].fee}
              />
            </Grid>
          )}
          {length > 7 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"/Events/" + data.data[7].name}
                coverName={data.data[7].name}
                coverDescript="potate"
                coverImage={data.data[7].pics[0]}
                attribute1="Dates"
                attribute1val={data.data[7].date[0]}
                attribute2="Time"
                attribute2val={data.data[7].time}
                attribute3="Location"
                attribute3val={data.data[7].location}
                attribute4="National Park"
                attribute4val={data.data[7].park}
                attribute5="Fee Information"
                attribute5val={data.data[7].fee}
              />
            </Grid>
          )}
          {length > 8 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"/Events/" + data.data[8].name}
                coverName={data.data[8].name}
                coverDescript="potate"
                coverImage={data.data[8].pics[0]}
                attribute1="Dates"
                attribute1val={data.data[8].date[0]}
                attribute2="Time"
                attribute2val={data.data[8].time}
                attribute3="Location"
                attribute3val={data.data[8].location}
                attribute4="National Park"
                attribute4val={data.data[8].park}
                attribute5="Fee Information"
                attribute5val={data.data[8].fee}
              />
            </Grid>
          )}
        </Grid>
      </React.Fragment>
    );
  }
}

class EventList extends Component {
  state = {
    allEvents: [],
    currentEvents: [],
    currentPage: null,
    totalPages: null,
    filtered: false,
    sorted: false,
    searched: false
  };

  filterBy = (stateName, parkName, event, sort, search) => {
    this.setState({ searched: false });
    this.setState({ searchText: search });
    var query = "https://api.connectwithnature.me/event";
    var list = [];
    var fil = false;
    var srt = false;
    var srch = false;
    if (
      stateName !== "Filter by State: " ||
      parkName !== "Filter by Park: " ||
      sort !== "Sort By: " ||
      event !== "Filter by Date: " ||
      search !== ""
    ) {
      query += "/filter";
      if (stateName !== "Filter by State: ") {
        fil = true;
        if (list.length === 0)
          list.push(`?state_name=${statesToAbbrev[stateName]}`);
        else list.push(`&state_name=${statesToAbbrev[stateName]}`);
      }
      if (parkName !== "Filter by Park: ") {
        fil = true;
        if (list.length === 0) list.push(`?park_name=${parkName}`);
        else list.push(`&park_name=${parkName}`);
      }
      if (event !== "Filter by Date: ") {
        fil = true;
        if (list.length === 0) list.push(`?date=${event}`);
        else list.push(`&date=${event}`);
      }
      if (sort !== "Sort By: ") {
        srt = true;
        if (list.length === 0) list.push(`?sort=${sort}`);
        else list.push(`&sort=${sort}`);
      }
      if (search !== "") {
        srch = true;
        if (list.length === 0) list.push(`?search=${search}`);
        else list.push(`&search=${search}`);
      }
    }

    list.forEach(x => {
      query += x;
    });

    axios
      .all([axios.get(query)])
      .then(
        axios.spread(eventFilterResponse => {
          this.setState({
            currentEvents: eventFilterResponse.data,
            allEvents: eventFilterResponse.data
          });
        })
      )
      .then(filter =>
        filter || fil
          ? this.setState({ filtered: true })
          : console.log("filter not set")
      )
      .then(sort =>
        sort || srt
          ? this.setState({ sorted: true })
          : console.log("sort not set")
      )
      .then(search =>
        search || srch
          ? this.setState({ searched: true })
          : console.log("search not set")
      )
      .then(this.onPageChangedFilter());
  };

  scrapeProjectInfo = () => {
    axios.all([axios.get("https://api.connectwithnature.me/event")]).then(
      axios.spread(eventResponse => {
        this.setState({
          allEvents: eventResponse.data,
          currentEvents: eventResponse.data,
          filtered: false,
          sorted: false,
          searched: false
        });
      })
    );
  };

  componentDidMount() {
    this.scrapeProjectInfo();
  }

  onPageChangedFilter = () => {
    const { allEvents } = this.state;
    const pageLimit = this.pageLimit;
    const currentPage = 1;
    const totalPages = Math.ceil(this.totalRecords / this.pageLimit);

    const offset = (currentPage - 1) * pageLimit;
    const currentEvents = allEvents.slice(offset, offset + pageLimit);

    this.setState({ currentPage, currentEvents, totalPages });
  };

  onPageChanged = data => {
    const { allEvents } = this.state;
    const { currentPage, totalPages, pageLimit } = data;

    const offset = (currentPage - 1) * pageLimit;
    const currentEvents = allEvents.slice(offset, offset + pageLimit);

    this.setState({ currentPage, currentEvents, totalPages });
  };

  render() {
    window.scrollTo(0, 0);
    const { classes } = this.props;
    const {
      allEvents,
      currentEvents,
      filtered,
      searchText,
      searched
    } = this.state;
    return (
      <div className={classes.container}>
        <div className={classes.banner}>Events</div>
        <hr />
        <EventFilter
          className={classes.dropdown}
          func={this.filterBy}
          resetFunc={this.scrapeProjectInfo}
          searched={searched}
        />

        {searched && (
          <EventSearchList allEvents={currentEvents} query={searchText} />
        )}

        {!searched && (
          <Grid container className={classes.root}>
            <Grid container item xs={12}>
              <div className={classes.pageNumber}>
                Page {this.state.currentPage} / {Math.ceil(allEvents.length/9) == 0 ? 1 : Math.ceil(allEvents.length/9)}
              </div>
              <FormRow className={classes.items} data={currentEvents} />
            </Grid>
          </Grid>
        )}

        {allEvents.length !== 0 && !filtered && !searched && (
          <Pagination
            totalRecords={allEvents.length}
            pageLimit={9}
            onPageChanged={this.onPageChanged}
          />
        )}

        {allEvents.length !== 0 && filtered && !searched && (
          <Pagination
            totalRecords={allEvents.length}
            pageLimit={9}
            onPageChanged={this.onPageChanged}
          />
        )}
      </div>
    );
  }
}

EventList.propTypes = {
  data: PropTypes.object
};

export default withStyles(styles)(EventList);
