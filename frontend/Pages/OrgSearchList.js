import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import SearchCard from "../Components/SearchCard.js";

const styles = {
  root: {
    padding: "3vh",
    flexGrow: 1,
    height: "inherit"
  },
  font: {
    fontFamily: "Questrial"
  },
  container: {
    padding: "3vh",
    height: "inherit",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  bannerSearch: {
    display: "flex",
    height: "6vh",
    fontFamily: "questrial",
    alignItems: "center",
    justifyContent: "center",
    color: "black",
    width: "100%",
    fontSize: "1.5em"
  }
};

class OrgSearchList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allOrgs: props.allOrgs,
      textToSearch: [],
      query: props.query
    };
  }

  componentDidMount() {
    const { allOrgs } = this.state;
    allOrgs.forEach(org => {
      this.loadHighlightQuery(org);
    });
  }

  loadHighlightQuery(org) {
    var arr = [];
    var searchQueries = [
      "category",
      "mission",
      "name",
      "rating",
      "state",
      "tagline"
    ];
    for (var s in searchQueries) {
      if (org[searchQueries[s]]) arr.push(org[searchQueries[s]]);
    }
    var arrString = [arr.toString()];
    this.setState(org => {
      const textToSearch = org.textToSearch.concat(arrString);

      return {
        textToSearch
      };
    });
  }

  render() {
    const { classes } = this.props;
    const { allOrgs, textToSearch, query } = this.state;
    var keygen = -1;
    return (
      <div>
        {allOrgs.length !== 0 && (
          <div className={classes.bannerSearch}>
            Search results for: {query}
          </div>
        )}
        {allOrgs.length === 0 && (
          <div className={classes.bannerSearch}>
            No results found for: {query}
          </div>
        )}
      <div className={classes.container}>
        {allOrgs.map(item => {
          keygen += 1;
          return (
            <SearchCard
              item={item}
              key={keygen}
              query={query}
              searchText={textToSearch[keygen]}
              link={"/Orgs/"}
            />
          );
        })}
      </div>
      </div>
    );
  }
}

OrgSearchList.propTypes = {
  data: PropTypes.object
};

export default withStyles(styles)(OrgSearchList);
