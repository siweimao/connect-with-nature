import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import SearchCard from "../Components/SearchCard.js";

const styles = {
  root: {
    padding: "3vh",
    flexGrow: 1,
    height: "inherit"
  },
  font: {
    fontFamily: "Questrial"
  },
  container: {
    padding: "3vh",
    height: "inherit",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  bannerSearch: {
    display: "flex",
    height: "6vh",
    fontFamily: "questrial",
    alignItems: "center",
    justifyContent: "center",
    color: "black",
    width: "100%",
    fontSize: "1.5em"
  }
};

class EventSearchList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allEvents: props.allEvents,
      textToSearch: [],
      query: props.query
    };
  }

  componentDidMount() {
    const { allEvents } = this.state;
    allEvents.forEach(event => {
      this.loadHighlightQuery(event);
    });
  }

  loadHighlightQuery(event) {
    var arr = [];
    var searchQueries = [
      "fee",
      "location",
      "name",
      "park",
      "states",
      "summary",
      "time"
    ];
    for (var s in searchQueries) {
      if (event[searchQueries[s]]) arr.push(event[searchQueries[s]]);
    }
    var arrString = [arr.toString()];
    this.setState(event => {
      const textToSearch = event.textToSearch.concat(arrString);

      return {
        textToSearch
      };
    });
  }

  render() {
    const { classes } = this.props;
    const { allEvents, textToSearch, query } = this.state;
    var keygen = -1;
    return (
      <div>
        {allEvents.length !== 0 && (
          <div className={classes.bannerSearch}>
            Search results for: {query}
          </div>
        )}
        {allEvents.length === 0 && (
          <div className={classes.bannerSearch}>
            No results found for: {query}
          </div>
        )}
      <div className={classes.container}>
        {allEvents.map(item => {
          keygen += 1;
          return (
            <SearchCard
              item={item}
              key={keygen}
              query={query}
              searchText={textToSearch[keygen]}
              link={"/Events/"}
            />
          );
        })}
      </div>
      </div>
    );
  }
}

EventSearchList.propTypes = {
  data: PropTypes.object
};

export default withStyles(styles)(EventSearchList);
