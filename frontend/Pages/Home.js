import React from "react";
import Splash from "../Components/Splash";
import HomeSection from "../Components/HomeSection";
import HomeSectionAlternate from "../Components/HomeSectionAlternate";
import yellowstone from "../Components/media/yellowstone.jpg";
import conservation from "../Components/media/conservation.jpg";
import adventure from "../Components/media/adventure.jpg";
import map from "../Components/media/map.jpg";
import scrollToComponent from "react-scroll-to-component";

const firstHeader = "Discover US National Parks";
const firstText =
  "The United States has over 60 unique and fascinating National Parks.  Take a stroll through nature and explore the beauty of the great outdoors!";
const secondHeader = "Join Conservation Efforts Today";
const secondText =
  "Learn about numerous nature conservation organizations located throughout the US and support a worthwhile cause to preserve our Earth!";
const thirdHeader = "Find Events & Activities";
const thirdText =
  "Engage with like-minded individuals and participate in the numerous events happening in and around the national parks!";
const fourthHeader = "Explore the States";
const fourthText =
  "Learn about national parks and current news in each of the 50 states. Find the nearest national park to you and start planning your next adventure!";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.scrollTo = React.createRef();
  }

  scrollToRef = () => {
    scrollToComponent(this.refs.scrollTo, {
      align: "top",
      duration: 1000
    });
  };

  render() {
    return (
      <React.Fragment>
        <Splash handlerFunction={this.scrollToRef} />
        <div ref="scrollTo">
          <HomeSection
            img={yellowstone}
            header={firstHeader}
            text={firstText}
            link={"/Parks"}
          />
          <HomeSectionAlternate
            img={conservation}
            header={secondHeader}
            text={secondText}
            link={"/Orgs"}
          />
          <HomeSection
            img={adventure}
            header={thirdHeader}
            text={thirdText}
            link={"/Events"}
          />
          <HomeSectionAlternate
            img={map}
            header={fourthHeader}
            text={fourthText}
            link={"/States"}
          />
        </div>
      </React.Fragment>
    );
  }
}

export default Home;
