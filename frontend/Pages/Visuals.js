import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import axios from "axios";
import BarChart from "../Components/BarChart";
import StateMap from "../Components/StateMap";
import BubbleChart from "@weknow/react-bubble-chart-d3";

const styles = {
  root: {
    flexGrow: 1,
    backgroundColor: "#388e3c"
  },
  container: {
    minHeight: "91vh",
    height: "auto",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  loaderContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    minHeight: "100vh",
    backgroundColor: "#ebfaeb"
  },
  banner: {
    display: "flex",
    height: "10vh",
    fontFamily: "questrial",
    alignItems: "center",
    justifyContent: "center",
    color: "black",
    width: "100%",
    fontSize: "2.5em"
  }
};

const theme = createMuiTheme({
  palette: {
    primary: { main: "#fafafa" },
    secondary: { main: "#11cb5f" }
  },
  typography: { useNextVariants: true }
});

class Visuals extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      parkStateDict: {
        AL: 0,
        AK: 0,
        AZ: 0,
        AR: 0,
        CA: 0,
        CO: 0,
        CT: 0,
        DE: 0,
        FL: 0,
        GA: 0,
        HI: 0,
        ID: 0,
        IL: 0,
        IN: 0,
        IA: 0,
        KS: 0,
        KY: 0,
        LA: 0,
        ME: 0,
        MD: 0,
        MA: 0,
        MI: 0,
        MN: 0,
        MS: 0,
        MO: 0,
        MT: 0,
        NE: 0,
        NV: 0,
        NH: 0,
        NJ: 0,
        NM: 0,
        NY: 0,
        NC: 0,
        ND: 0,
        OH: 0,
        OK: 0,
        OR: 0,
        PA: 0,
        RI: 0,
        SC: 0,
        SD: 0,
        TN: 0,
        TX: 0,
        UT: 0,
        VT: 0,
        VA: 0,
        VI: 0,
        WA: 0,
        WV: 0,
        WI: 0,
        WY: 0
      },
      orgStateDict: {
        AL: 0,
        AK: 0,
        AZ: 0,
        AR: 0,
        CA: 0,
        CO: 0,
        CT: 0,
        DE: 0,
        FL: 0,
        GA: 0,
        HI: 0,
        ID: 0,
        IL: 0,
        IN: 0,
        IA: 0,
        KS: 0,
        KY: 0,
        LA: 0,
        ME: 0,
        MD: 0,
        MA: 0,
        MI: 0,
        MN: 0,
        MS: 0,
        MO: 0,
        MT: 0,
        NE: 0,
        NV: 0,
        NH: 0,
        NJ: 0,
        NM: 0,
        NY: 0,
        NC: 0,
        ND: 0,
        OH: 0,
        OK: 0,
        OR: 0,
        PA: 0,
        RI: 0,
        SC: 0,
        SD: 0,
        TN: 0,
        TX: 0,
        UT: 0,
        VT: 0,
        VA: 0,
        VI: 0,
        WA: 0,
        WV: 0,
        WI: 0,
        WY: 0
      },
      eventStateDict: {
        AL: 0,
        AK: 0,
        AZ: 0,
        AR: 0,
        CA: 0,
        CO: 0,
        CT: 0,
        DE: 0,
        FL: 0,
        GA: 0,
        HI: 0,
        ID: 0,
        IL: 0,
        IN: 0,
        IA: 0,
        KS: 0,
        KY: 0,
        LA: 0,
        ME: 0,
        MD: 0,
        MA: 0,
        MI: 0,
        MN: 0,
        MS: 0,
        MO: 0,
        MT: 0,
        NE: 0,
        NV: 0,
        NH: 0,
        NJ: 0,
        NM: 0,
        NY: 0,
        NC: 0,
        ND: 0,
        OH: 0,
        OK: 0,
        OR: 0,
        PA: 0,
        RI: 0,
        SC: 0,
        SD: 0,
        TN: 0,
        TX: 0,
        UT: 0,
        VT: 0,
        VA: 0,
        VI: 0,
        WA: 0,
        WV: 0,
        WI: 0,
        WY: 0
      },
      billStateDict: {
        AL: 0,
        AK: 0,
        AZ: 0,
        AR: 0,
        CA: 0,
        CO: 0,
        CT: 0,
        DE: 0,
        FL: 0,
        GA: 0,
        HI: 0,
        ID: 0,
        IL: 0,
        IN: 0,
        IA: 0,
        KS: 0,
        KY: 0,
        LA: 0,
        ME: 0,
        MD: 0,
        MA: 0,
        MI: 0,
        MN: 0,
        MS: 0,
        MO: 0,
        MT: 0,
        NE: 0,
        NV: 0,
        NH: 0,
        NJ: 0,
        NM: 0,
        NY: 0,
        NC: 0,
        ND: 0,
        OH: 0,
        OK: 0,
        OR: 0,
        PA: 0,
        RI: 0,
        SC: 0,
        SD: 0,
        TN: 0,
        TX: 0,
        UT: 0,
        VT: 0,
        VA: 0,
        VI: 0,
        WA: 0,
        WV: 0,
        WI: 0,
        WY: 0
      },
      efOrgStateDict: {
        AL: 0,
        AK: 0,
        AZ: 0,
        AR: 0,
        CA: 0,
        CO: 0,
        CT: 0,
        DE: 0,
        FL: 0,
        GA: 0,
        HI: 0,
        ID: 0,
        IL: 0,
        IN: 0,
        IA: 0,
        KS: 0,
        KY: 0,
        LA: 0,
        ME: 0,
        MD: 0,
        MA: 0,
        MI: 0,
        MN: 0,
        MS: 0,
        MO: 0,
        MT: 0,
        NE: 0,
        NV: 0,
        NH: 0,
        NJ: 0,
        NM: 0,
        NY: 0,
        NC: 0,
        ND: 0,
        OH: 0,
        OK: 0,
        OR: 0,
        PA: 0,
        RI: 0,
        SC: 0,
        SD: 0,
        TN: 0,
        TX: 0,
        UT: 0,
        VT: 0,
        VA: 0,
        VI: 0,
        WA: 0,
        WV: 0,
        WI: 0,
        WY: 0
      },
      progStateDict: {
        AL: 0,
        AK: 0,
        AZ: 0,
        AR: 0,
        CA: 0,
        CO: 0,
        CT: 0,
        DE: 0,
        FL: 0,
        GA: 0,
        HI: 0,
        ID: 0,
        IL: 0,
        IN: 0,
        IA: 0,
        KS: 0,
        KY: 0,
        LA: 0,
        ME: 0,
        MD: 0,
        MA: 0,
        MI: 0,
        MN: 0,
        MS: 0,
        MO: 0,
        MT: 0,
        NE: 0,
        NV: 0,
        NH: 0,
        NJ: 0,
        NM: 0,
        NY: 0,
        NC: 0,
        ND: 0,
        OH: 0,
        OK: 0,
        OR: 0,
        PA: 0,
        RI: 0,
        SC: 0,
        SD: 0,
        TN: 0,
        TX: 0,
        UT: 0,
        VT: 0,
        VA: 0,
        VI: 0,
        WA: 0,
        WV: 0,
        WI: 0,
        WY: 0
      },
      loaded: false
    };
  }

  updateBarData(dataPark, dataOrg, dataEvent) {
    dataPark.data.forEach(park => {
      var updatedDict = { ...this.state.parkStateDict };
      updatedDict[park.states] = updatedDict[park.states] + 1;
      this.setState(prevState => ({
        parkStateDict: updatedDict
      }));
    });
    dataOrg.data.forEach(org => {
      var updatedDict = { ...this.state.orgStateDict };
      updatedDict[org.state] = updatedDict[org.state] + 1;
      this.setState(prevState => ({
        orgStateDict: updatedDict
      }));
    });
    dataEvent.data.forEach(event => {
      var updatedDict = { ...this.state.eventStateDict };
      updatedDict[event.states] = updatedDict[event.states] + 1;
      this.setState(prevState => ({
        eventStateDict: updatedDict
      }));
    });
  }

  updateCMapData(dataAll) {
    dataAll.data.forEach(element => {
      var updatedDict1 = { ...this.state.billStateDict };
      var updatedDict2 = { ...this.state.efOrgStateDict };
      var updatedDict3 = { ...this.state.progStateDict };

      updatedDict1[element.abbr] = element.num_bills;
      updatedDict2[element.abbr] = element.num_orgs;
      updatedDict3[element.abbr] = element.num_progs;

      this.setState(prevState => ({
        billStateDict: updatedDict1,
        efOrgStateDict: updatedDict2,
        progStateDict: updatedDict3
      }));
    });
  }

  scrapeProjectInfo() {
    axios
      .all([
        axios.get(`https://api.connectwithnature.me/park`),
        axios.get("https://api.connectwithnature.me/org"),
        axios.get("https://api.connectwithnature.me/event")
      ])
      .then(
        axios.spread((parkResponse, orgResponse, eventResponse) => {
          this.updateBarData(parkResponse, orgResponse, eventResponse);
          this.setState({
            loaded: true
          });
        })
      );
  }

  scrapeCustomerInfo() {
    axios.all([axios.get(`https://earth-first.live/api/states`)]).then(
      axios.spread(allResponse => {
        this.updateCMapData(allResponse);
      })
    );
  }

  componentDidMount() {
    this.scrapeProjectInfo();
    this.scrapeCustomerInfo();
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;
    const { parkStateDict, orgStateDict, eventStateDict, loaded } = this.state;

    const { billStateDict, efOrgStateDict, progStateDict } = this.state;
    var data = [];
    if (parkStateDict != null) {
      let temp = this.state.parkStateDict;
      for (const [key, value] of Object.entries(temp)) {
        var tempDict = {};
        tempDict["label"] = key;
        tempDict["value"] = value;
        data.push(tempDict);
      }
    }
    var dataEF = [];
    if (efOrgStateDict != null) {
      let temp = efOrgStateDict;
      for (const [key, value] of Object.entries(temp)) {
        let tempDict = {};
        tempDict["label"] = key;
        tempDict["value"] = value;
        dataEF.push(tempDict);
      }
    }
    var dataOrg = [];
    if (orgStateDict != null) {
      let temp = this.state.orgStateDict;
      for (const [key, value] of Object.entries(temp)) {
        let tempDict = {};
        tempDict["label"] = key;
        tempDict["value"] = value;
        dataOrg.push(tempDict);
      }
    }
    if (loaded) {
      return (
        <div>
          <MuiThemeProvider theme={theme}>
            <Paper className={classes.root} square={true}>
              <Tabs
                value={this.state.value}
                onChange={this.handleChange}
                indicatorColor="primary"
                textColor="primary"
                centered
              >
                <Tab label="Bar Chart" textColor="primary" />
                <Tab label="Bubble Chart" />
                <Tab label="Map Chart" />
                <Tab label="Customer Visuals" />
              </Tabs>
            </Paper>
          </MuiThemeProvider>

          <div className={classes.container}>
            {this.state.value === 0 && (
              <div>
                <div className={classes.banner}>
                  Number of Nature Conservation Organizations in Each State
                </div>
                <BarChart
                  data={this.state.orgStateDict}
                  x="State (State Abbreviation)"
                  y="Organizations (Number of Organizations)"
                />
              </div>
            )}
            {this.state.value === 1 && (
              <div>
                <div className={classes.banner}>
                  Number of National Parks in Each State
                </div>
                <BubbleChart
                  graph={{
                    zoom: 1.0,
                    offsetX: 0.0,
                    offsetY: 0.0
                  }}
                  width={1000}
                  height={1000}
                  showLegend={false} // optional value, pass false to disable the legend.
                  legendPercentage={20} // number that represent the % of with that legend going to use.
                  valueFont={{
                    family: "Arial",
                    size: 12,
                    color: "#fff",
                    weight: "bold"
                  }}
                  labelFont={{
                    family: "Arial",
                    size: 16,
                    color: "#fff",
                    weight: "bold"
                  }}
                  //Custom bubble/legend click functions such as searching using the label, redirecting to other page
                  bubbleClickFunc={this.bubbleClick}
                  legendClickFun={this.legendClick}
                  data={data}
                />
              </div>
            )}
            {this.state.value === 2 && (
              <div>
                <div className={classes.banner}>
                  Number of Parks, Events, and Orgs in Each State
                </div>

                <StateMap
                  parkData={parkStateDict}
                  orgData={orgStateDict}
                  eventData={eventStateDict}
                  first="Parks"
                  second="Orgs:"
                  third="Events:"
                />
              </div>
            )}
            {this.state.value === 3 && (
              <div>
                <div className={classes.banner}>
                  Number of Bills in Each State
                </div>
                <BarChart
                  data={billStateDict}
                  x="State (State Abbreviation)"
                  y="Bills (Number of Bills)"
                />
                <div className={classes.banner}>
                  Number of Organizations in Each State
                </div>
                <BubbleChart
                  graph={{
                    zoom: 1.0,
                    offsetX: 0.0,
                    offsetY: 0.0
                  }}
                  width={1000}
                  height={1000}
                  showLegend={false} // optional value, pass false to disable the legend.
                  legendPercentage={20} // number that represent the % of with that legend going to use.
                  valueFont={{
                    family: "Arial",
                    size: 12,
                    color: "#fff",
                    weight: "bold"
                  }}
                  labelFont={{
                    family: "Arial",
                    size: 16,
                    color: "#fff",
                    weight: "bold"
                  }}
                  //Custom bubble/legend click functions such as searching using the label, redirecting to other page
                  bubbleClickFunc={this.bubbleClick}
                  legendClickFun={this.legendClick}
                  data={dataEF}
                />
                <div className={classes.banner}>
                  Number of Bills, Orgs, and Programs in Each State
                </div>
                <StateMap
                  parkData={billStateDict}
                  orgData={efOrgStateDict}
                  eventData={progStateDict}
                  first="Bills:"
                  second="Orgs:"
                  third="Programs:"
                />
              </div>
            )}
          </div>
        </div>
      );
    }
    if (!loaded) {
      return (
        <div className={classes.loaderContainer}>
          <div className="spinner-border text-success" />
        </div>
      );
    }
  }
}

export default withStyles(styles)(Visuals);
