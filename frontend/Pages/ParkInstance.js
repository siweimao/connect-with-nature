import React, { Component } from "react";
import ParkInfoCard from "../Components/ParkInfoCard.js";
import RelatedCard from "../Components/RelatedCard.js";
import Map from "../Components/Map.js";
import TwitterBox from "../Components/TwitterBox.js";
import axios from "axios";
import { withStyles } from "@material-ui/core/styles";

const Header = {
  color: "white",
  fontSize: "4em",
  textAlign: "center",
  fontFamily: "Questrial"
};

const Background = {
  backgroundColor: "#ebfaeb",
  minHeight: "100vh",
  height: "auto"
};

const ImageContainer = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

const RelatedContainer = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  flexWrap: "wrap",
  paddingBottom: "4vh",
};

const ParkInfoCardStyle = {
  paddingTop: "3vh",
}

const MapWindow = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-evenly",
  paddingTop: "3vh",
  width: "100%",
};

const styles = {
  loaderContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    minHeight: "100vh",
    backgroundColor: "#ebfaeb"
  },
  image: {
    display: "block",
    width: "100%",
    maxHeight: "300px",
    objectFit: "cover",
    filter: "blur(3px) brightness(40%)",
  },
  displayImage: {
    display: "block",
    width: "100%",
    maxHeight: "300px",
    objectFit: "cover",
  },
  content: {
    position: "absolute",
    display: "flex",
    flexDirection: "column"
  },
  contentContainer: {
    margin: "0 8vw 0 8vw",
  },
  related: {
    color: "black",
    fontSize: "4em",
    textAlign: "center",
    fontFamily: "Questrial",
    marginTop: "7vh",
  },
  hr: {
    height: "1px",
    width: "20vw",
    borderTop: "1.75px solid black",
    marginTop: "0",
    marginBottom: "3vh",
  }
}

class ParksInstance extends Component {
  state = {
    data: {
      pics: [
        "https://downloadpng.com/wp-content/uploads/thenext-thumb-cache//cartoon-tree-png-bfb6027a01338183db2bdfa145ba5932-900x0.png"
      ]
    }
  };

  scrapeProjectInfo() {
    axios
      .all([
        axios.get(
          `https://api.connectwithnature.me/park?park_name=${
            this.props.match.params.name
          }`
        )
      ])
      .then(
        axios.spread(parksResponse => {
          this.setState({
            data: parksResponse.data[0]
          });
        })
      );
  }

  componentDidMount() {
    this.scrapeProjectInfo();
  }

  render() {
    window.scrollTo(0, 0);
    const {classes} = this.props;
    var jsonStates = require("../data/states.json");
    const { data } = this.state;
    const {
      name,
      pics,
      states,
      summary,
      directions,
      weather,
      events,
      orgs,
      state_pics,
      org_pics,
      event_pics
    } = data !== undefined ? data : "";
    var orgsArr = orgs;
    var orgName;
    if (orgsArr) {
      orgName = orgsArr[0];
    }

    var eventsArr = events;
    var eventsName;
    if (eventsArr) {
      eventsName = eventsArr[0];
    }
    var str = states;
    var res;
    if (states) {
      res = str.split(",");
      str = res[0];
    }

    var orgPics = org_pics;

    var eventsPics = event_pics;
    var eventsPicUsed;
    if (eventsPics) {
      eventsPicUsed = eventsPics[0];
    }

    var statesPics = state_pics;
    var statesPicUsed;
    if (statesPics) {
      statesPicUsed = statesPics[0];
    }

    if (name === undefined) {
      return (
        <div className={classes.loaderContainer}>
          <div className="spinner-border text-success" />
        </div>
      );
    } else {
      return (
        <React.Fragment>
          <div style={Background}>
            <div style={ImageContainer}>
              <img alt="" className={classes.image} src={pics[0]} />
              <div className={classes.content}>
                <h1 style={Header}>
                  {name}
                </h1>
              </div>
            </div>
            <div className={classes.contentContainer}>
              <div className="row">
                <div className="col-sm-6" style={ParkInfoCardStyle}>
                  <div style={MapWindow}>
                    <img alt="" className={classes.displayImage} src={pics[0]} />
                  </div>
                  <div style={MapWindow}>
                    <Map
                      longitude={this.state.data.long}
                      latitude={this.state.data.lat}
                    />
                  </div>
                  <div style={MapWindow}>
                    <TwitterBox name={name} />
                  </div>
                </div>
                <div className="col-sm-6" style={ParkInfoCardStyle}>
                  <div>
                    <br />
                    <ParkInfoCard
                      states={states}
                      summary={summary}
                      directions={directions}
                      weather={weather}
                    />
                    <br />
                  </div>
                </div>
              </div>
            </div>
            
            <h1 className={classes.related}>
              Related
            </h1>
            <hr className={classes.hr} />
            <div style={RelatedContainer}>
              {states !== undefined && states.length > 0 && (
                <RelatedCard
                  title="States"
                  link={"/States/" + jsonStates[str]}
                  text={jsonStates[str]}
                  pic={statesPicUsed}
                />
              )}
              {events && (
                <RelatedCard
                  title="Events"
                  link={"/Events/" + eventsName}
                  text={eventsName}
                  pic={eventsPicUsed}
                />
              )}
              {orgs && (
                <RelatedCard
                  title="Organization"
                  link={"/Orgs/" + orgName}
                  text={orgName}
                  pic={orgPics}
                />
              )}
            </div>
            <br />
          </div>
        </React.Fragment>
      );
    }
  }
}

export default withStyles(styles)(ParksInstance);
