import React, { Component } from "react";
import OrgInfoCard from "../Components/OrgInfoCard.js";
import RelatedCard from "../Components/RelatedCard.js";
import axios from "axios";
import YouTube from "react-youtube";
import { FacebookProvider, Page } from 'react-facebook';
import splash from "../Components/media/org_splash.jpg";

const opts = {
  height: '600',
  width: '1080',
  playerVars: {}
};

const loaderContainer = {
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  minHeight: "100vh",
  backgroundColor: "#ebfaeb"
};

const Background = {
  backgroundColor: "#ebfaeb",
  minHeight: "100vh",
  height: "auto"
};

const ImageContainer = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

const Image = {
  display: "block",
  width: "100%",
  maxHeight: "300px",
  objectFit: "cover",
  filter: "blur(3px) brightness(40%)",
};

const Content = {
  position: "absolute",
  display: "flex",
  flexDirection: "column"
};

const Header = {
  color: "white",
  fontSize: "3em",
  textAlign: "center",
  fontFamily: "Questrial"
};

const contentContainer = {
  margin: "0 8vw 0 8vw",
};

const paddingTopStyle = {
  paddingTop: "3vh",
};

const MapWindow = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-evenly",
  paddingTop: "3vh",
  width: "100%",
};

const DisplayImage = {
  display: "block",
  maxHeight: "400px",
  objectFit: "contain",
};

const Related = {
  color: "black",
  fontSize: "4em",
  textAlign: "center",
  fontFamily: "Questrial",
  marginTop: "7vh",
};

const hr = {
  height: "1px",
  width: "20vw",
  borderTop: "1.75px solid black",
  marginTop: "0",
  marginBottom: "3vh",
};

const RelatedContainer = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  flexWrap: "wrap",
  paddingBottom: "4vh",
};

class OrgsInstance extends Component {
  state = {
    data: {}
  };

  scrapeProjectInfo() {
    axios
      .all([
        axios.get(
          `https://api.connectwithnature.me/org?org_name=${
            this.props.match.params.name
          }`
        )
      ])
      .then(
        axios.spread(orgResponse => {
          this.setState({
            data: orgResponse.data[0]
          });
        })
      );
  }

  componentDidMount() {
    this.scrapeProjectInfo();
  }

  render() {
    window.scrollTo(0, 0);
    var jsonStates = require("../data/states.json");
    const { data } = this.state;
    const {
      name,
      pics,
      mission,
      state,
      tagline,
      events,
      parks,
      park_pics,
      event_pics,
      state_pics,
      youtube_id,
      fb
    } = data !== undefined ? data : "";
    var parkStr = parks;
    var parkName;
    if (parkStr) {
      parkName = parkStr[0];
    }
    var eventsArr = events;
    var eventsName;
    if (eventsArr) {
      eventsName = eventsArr[0];
    }

    var parksPics = park_pics;
    var parksPicUsed;
    if (parksPics) {
      parksPicUsed = parksPics[0];
    }

    var eventsPics = event_pics;
    var eventsPicUsed;
    if (eventsPics) {
      eventsPicUsed = eventsPics[0];
    }

    var statesPics = state_pics;
    var statesPicUsed;
    if (statesPics) {
      statesPicUsed = statesPics[0];
    }

    var youtube_id1 = youtube_id;

    if (fb === undefined) {
      return (
        <div style={loaderContainer}>
          <div className="spinner-border text-success" />
        </div>
      );
    } else {
    return (
      <React.Fragment>
        <div style={Background}>
          <div style={ImageContainer}>
            <img  style={Image} src={splash} />
            <div style={Content}>
              <h1 style={Header}>
                {name}
              </h1>
            </div>
          </div>
          <div style={contentContainer}>
            <div className="row">
              <div className="col-sm-12" style={paddingTopStyle}>
                <div style={MapWindow}>
                  <YouTube
                    videoId={youtube_id1}
                    opts={opts}
                    onReady={this._onReady}
                  />
                </div>
              </div>
              <div className="col-sm-6" style={paddingTopStyle}>
                <div style={ImageContainer}>
                  <img alt="" className="img-fluid" style={DisplayImage} src={pics} />
                </div>
                <div style={MapWindow}>
                  <FacebookProvider appId="355536678429531">
                      <Page href={"https://www.facebook.com/" + fb}
                        width="1000"
                        tabs="timeline" />
                  </FacebookProvider>
                </div>
              </div>
              <div className="col-sm-6" style={paddingTopStyle}>
                <div>
                  <OrgInfoCard
                    tagline={tagline}
                    mission={mission}
                    state={state}
                  />
                </div>
              </div>  
            </div>
          </div>
          
          <h1 style={Related}>
            Related
          </h1>
          <hr style={hr} />
          <div style={RelatedContainer}>
            {parkName !== undefined && parkName.length > 0 && (
              <RelatedCard
                title="Park"
                link={"/Parks/" + parkName}
                text={parkName}
                pic={parksPicUsed}
              />
            )}
            {state !== undefined && state.length > 0 && (
              <RelatedCard
                title="States"
                link={"/States/" + jsonStates[state]}
                text={jsonStates[state]}
                pic={statesPicUsed}
              />
            )}
            {events && (
              <RelatedCard
                title="Events"
                link={"/Events/" + eventsName}
                text={eventsName}
                pic={eventsPicUsed}
              />
            )}
          </div>
          <br />
        </div>
      </React.Fragment>
    );
  }
}
  _onReady(event) {
    event.target.pauseVideo();
  }
}

export default OrgsInstance;
