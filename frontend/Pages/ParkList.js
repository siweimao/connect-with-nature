import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import ListInfoCard from "../Components/ListInfoCard.js";
import Pagination from "../Components/Pagination.js";
import ParkFilter from "../Components/ParkFilter.js";
import ParkSearchList from "./ParkSearchList";
import axios from "axios";
import "../styles/Filter.css";

const styles = {
  root: {
    paddingTop: "4vh",
    paddingBottom: "5vh",
    flexGrow: 1,
    height: "inherit",
    width: "90vw"
  },
  container: {
    backgroundColor: "#ebfaeb",
    paddingTop: "3.25vh",
    minHeight: "100vh",
    height: "auto",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  banner: {
    display: "flex",
    height: "14vh",
    fontFamily: "questrial",
    alignItems: "center",
    justifyContent: "center",
    color: "black",
    width: "100%",
    fontSize: "3.5em"
  },
  pageNumber: {
    width: "100%",
    textAlign: "center",
    fontFamily: "questrial",
    fontSize: "1.25em",
    marginBottom: "2vh"
  },
  bannerSearch: {
    display: "flex",
    height: "6vh",
    fontFamily: "questrial",
    alignItems: "center",
    justifyContent: "center",
    color: "black",
    width: "100%",
    fontSize: "1.5em"
  },
  pagination: {}
};

const statesToAbbrev = {
  Alabama: "AL",
  Alaska: "AK",
  Arizona: "AZ",
  Arkansas: "AR",
  California: "CA",
  Colorado: "CO",
  Connecticut: "CT",
  Delaware: "DE",
  Florida: "FL",
  Georgia: "GA",
  Hawaii: "HI",
  Idaho: "ID",
  Illinois: "IL",
  Indiana: "IN",
  Iowa: "IA",
  Kansas: "KS",
  Kentucky: "KY",
  Louisiana: "LA",
  Maine: "ME",
  Maryland: "MD",
  Massachusetts: "MA",
  Michigan: "MI",
  Minnesota: "MN",
  Mississippi: "MS",
  Missouri: "MO",
  Montana: "MT",
  Nebraska: "NE",
  Nevada: "NV",
  "New Hampshire": "NH",
  "New Jersey": "NJ",
  "New Mexico": "NW",
  "New York": "NY",
  "North Carolina": "NC",
  "North Dakota": "ND",
  Ohio: "OH",
  Oklahoma: "OK",
  Oregon: "OR",
  Pennsylvania: "PA",
  "Rhode Island": "RI",
  "South Carolina": "SC",
  "South Dakota": "SD",
  Tennessee: "TN",
  Texas: "TX",
  Utah: "UT",
  Vermont: "VT",
  Virginia: "VA",
  Washington: "WA",
  "West Virginia": "WV",
  Wisconsin: "WI",
  Wyoming: "WY"
};

function FormRow(props) {
  const data = props;
  if (data === undefined || data.data.length === 0) {
    return <div />;
  } else {
    const length = data.data.length;
    return (
      <React.Fragment>
        <Grid container spacing={40}>
          {length > 0 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Parks/" + data.data[0].name}
                coverName={data.data[0].name}
                coverDescript="potate"
                coverImage={data.data[0].pics[0]}
                attribute1="States"
                attribute1val={data.data[0].states}
                attribute2="Phone Number"
                attribute2val={data.data[0].phone}
                attribute3="Email Address"
                attribute3val={data.data[0].email}
                attribute4="Number of Campsites"
                attribute4val={data.data[0].campgrounds}
                attribute5="Entrance Fees"
                attribute5val={data.data[0].fee}
              />
            </Grid>
          )}
          {length > 1 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Parks/" + data.data[1].name}
                coverName={data.data[1].name}
                coverDescript="potate"
                coverImage={data.data[1].pics[0]}
                attribute1="States"
                attribute1val={data.data[1].states}
                attribute2="Phone Number"
                attribute2val={data.data[1].phone}
                attribute3="Email Address"
                attribute3val={data.data[1].email}
                attribute4="Number of Campsites"
                attribute4val={data.data[1].campgrounds}
                attribute5="Entrance Fees"
                attribute5val={data.data[1].fee}
              />
            </Grid>
          )}
          {length > 2 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Parks/" + data.data[2].name}
                coverName={data.data[2].name}
                coverDescript="potate"
                coverImage={data.data[2].pics[0]}
                attribute1="States"
                attribute1val={data.data[2].states}
                attribute2="Phone Number"
                attribute2val={data.data[2].phone}
                attribute3="Email Address"
                attribute3val={data.data[2].email}
                attribute4="Number of Campsites"
                attribute4val={data.data[2].campgrounds}
                attribute5="Entrance Fees"
                attribute5val={data.data[2].fee}
              />
            </Grid>
          )}
          {length > 3 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Parks/" + data.data[3].name}
                coverName={data.data[3].name}
                coverDescript="potate"
                coverImage={data.data[3].pics[0]}
                attribute1="States"
                attribute1val={data.data[3].states}
                attribute2="Phone Number"
                attribute2val={data.data[3].phone}
                attribute3="Email Address"
                attribute3val={data.data[3].email}
                attribute4="Number of Campsites"
                attribute4val={data.data[3].campgrounds}
                attribute5="Entrance Fees"
                attribute5val={data.data[3].fee}
              />
            </Grid>
          )}
          {length > 4 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Parks/" + data.data[4].name}
                coverName={data.data[4].name}
                coverDescript="potate"
                coverImage={data.data[4].pics[0]}
                attribute1="States"
                attribute1val={data.data[4].states}
                attribute2="Phone Number"
                attribute2val={data.data[4].phone}
                attribute3="Email Address"
                attribute3val={data.data[4].email}
                attribute4="Number of Campsites"
                attribute4val={data.data[4].campgrounds}
                attribute5="Entrance Fees"
                attribute5val={data.data[4].fee}
              />
            </Grid>
          )}
          {length > 5 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Parks/" + data.data[5].name}
                coverName={data.data[5].name}
                coverDescript="potate"
                coverImage={data.data[5].pics[0]}
                attribute1="States"
                attribute1val={data.data[5].states}
                attribute2="Phone Number"
                attribute2val={data.data[5].phone}
                attribute3="Email Address"
                attribute3val={data.data[5].email}
                attribute4="Number of Campsites"
                attribute4val={data.data[5].campgrounds}
                attribute5="Entrance Fees"
                attribute5val={data.data[5].fee}
              />
            </Grid>
          )}
          {length > 6 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Parks/" + data.data[6].name}
                coverName={data.data[6].name}
                coverDescript="potate"
                coverImage={data.data[6].pics[0]}
                attribute1="States"
                attribute1val={data.data[6].states}
                attribute2="Phone Number"
                attribute2val={data.data[6].phone}
                attribute3="Email Address"
                attribute3val={data.data[6].email}
                attribute4="Number of Campsites"
                attribute4val={data.data[6].campgrounds}
                attribute5="Entrance Fees"
                attribute5val={data.data[6].fee}
              />
            </Grid>
          )}
          {length > 7 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Parks/" + data.data[7].name}
                coverName={data.data[7].name}
                coverDescript="potate"
                coverImage={data.data[7].pics[0]}
                attribute1="States"
                attribute1val={data.data[7].states}
                attribute2="Phone Number"
                attribute2val={data.data[7].phone}
                attribute3="Email Address"
                attribute3val={data.data[7].email}
                attribute4="Number of Campsites"
                attribute4val={data.data[7].campgrounds}
                attribute5="Entrance Fees"
                attribute5val={data.data[7].fee}
              />
            </Grid>
          )}
          {length > 8 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Parks/" + data.data[8].name}
                coverName={data.data[8].name}
                coverDescript="potate"
                coverImage={data.data[8].pics[0]}
                attribute1="States"
                attribute1val={data.data[8].states}
                attribute2="Phone Number"
                attribute2val={data.data[8].phone}
                attribute3="Email Address"
                attribute3val={data.data[8].email}
                attribute4="Number of Campsites"
                attribute4val={data.data[8].campgrounds}
                attribute5="Entrance Fees"
                attribute5val={data.data[8].fee}
              />
            </Grid>
          )}
        </Grid>
      </React.Fragment>
    );
  }
}

class ParkList extends Component {
  state = {
    allParks: [],
    currentParks: [],
    currentPage: null,
    totalPages: null,
    filtered: false,
    sorted: false,
    searched: false,
    searchText: ""
  };

  filterBy = (stateName, fee, camp, sort, search) => {
    this.setState({ searched: false });
    this.setState({ searchText: search });
    var query = "https://api.connectwithnature.me/park";
    var list = [];
    var fil = false;
    var srt = false;
    var srch = false;
    if (
      stateName !== "Filter by State: " ||
      fee !== "Filter by Fee: " ||
      sort !== "Sort By: " ||
      camp !== "Filter by Campgrounds Size: " ||
      search !== ""
    ) {
      query += "/filter";
      if (stateName !== "Filter by State: ") {
        fil = true;
        if (list.length === 0)
          list.push(`?state_name=${statesToAbbrev[stateName]}`);
        else list.push(`&state_name=${statesToAbbrev[stateName]}`);
      }
      if (sort !== "Sort By: ") {
        srt = true;
        if (list.length === 0) list.push(`?sort=${sort}`);
        else list.push(`&sort=${sort}`);
      }
      if (fee !== "Filter by Fee: ") {
        fil = true;
        if (list.length === 0) list.push(`?fee=${fee}`);
        else list.push(`&fee=${fee}`);
      }
      if (camp !== "Filter by Campgrounds Size: ") {
        fil = true;
        if (list.length === 0) list.push(`?campgrounds=${camp}`);
        else list.push(`&campgrounds=${camp}`);
      }
      if (search !== "") {
        srch = true;
        if (list.length === 0) list.push(`?search=${search}`);
        else list.push(`&search=${search}`);
      }
    }

    list.forEach(x => {
      query += x;
    });

    axios
      .all([axios.get(query)])
      .then(
        axios.spread(parkFilterResponse => {
          this.setState({
            currentParks: parkFilterResponse.data,
            allParks: parkFilterResponse.data
          });
        })
      )
      .then(filter =>
        filter || fil
          ? this.setState({ filtered: true })
          : console.log("filter not set")
      )
      .then(sort =>
        sort || srt
          ? this.setState({ sorted: true })
          : console.log("sort not set")
      )
      .then(search =>
        search || srch
          ? this.setState({ searched: true })
          : console.log("search not set")
      )
      .then(this.onPageChangedFilter());
  };

  scrapeProjectInfo = () => {
    axios.all([axios.get("https://api.connectwithnature.me/park")]).then(
      axios.spread(parkResponse => {
        this.setState({
          allParks: parkResponse.data,
          currentParks: parkResponse.data,
          filtered: false,
          sorted: false,
          searched: false
        });
      })
    );
  };

  componentDidMount() {
    this.scrapeProjectInfo();
  }

  onPageChangedFilter() {
    const { allParks } = this.state;
    const pageLimit = this.pageLimit;
    const currentPage = 1;
    const totalPages = Math.ceil(this.totalRecords / this.pageLimit);

    const offset = (currentPage - 1) * pageLimit;
    const currentParks = allParks.slice(offset, offset + pageLimit);

    this.setState({ currentPage, currentParks, totalPages });
  }

  onPageChanged = data => {
    const { allParks } = this.state;
    const { currentPage, totalPages, pageLimit } = data;

    const offset = (currentPage - 1) * pageLimit;
    const currentParks = allParks.slice(offset, offset + pageLimit);
    this.setState({ currentPage, currentParks, totalPages });
  };

  render() {
    window.scrollTo(0, 0);
    const { classes } = this.props;
    const {
      allParks,
      currentParks,
      filtered,
      searched,
      searchText
    } = this.state;
    return (
      <div className={classes.container} ref="scrollTo">
        <div className={classes.banner}>Parks</div>
        <hr />

        <ParkFilter
          className={classes.dropdown}
          func={this.filterBy}
          resetFunc={this.scrapeProjectInfo}
          searched={searched}
        />

        {searched && (
          <ParkSearchList allParks={currentParks} query={searchText} />
        )}

        {!searched && (
          <Grid container className={classes.root}>
            <Grid container item xs={12}>
              <div className={classes.pageNumber}>
                Page {this.state.currentPage} / {Math.ceil(allParks.length/9) == 0 ? 1 : Math.ceil(allParks.length/9)}
              </div>
              <FormRow className={classes.items} data={currentParks} />
            </Grid>
          </Grid>
        )}

        {allParks.length > 9 && !filtered && !searched && (
          <Pagination
            className={classes.pagination}
            totalRecords={allParks.length}
            pageLimit={9}
            onPageChanged={this.onPageChanged}
          />
        )}

        {allParks.length > 9 && filtered && !searched && (
          <Pagination
            className={classes.pagination}
            totalRecords={allParks.length}
            pageLimit={9}
            onPageChanged={this.onPageChanged}
          />
        )}
      </div>
    );
  }
}

ParkList.propTypes = {
  data: PropTypes.object
};

export default withStyles(styles)(ParkList);
