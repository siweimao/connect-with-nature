import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import SearchCard from "../Components/SearchCard.js";

const styles = {
  root: {
    padding: "3vh",
    flexGrow: 1,
    height: "inherit"
  },
  font: {
    fontFamily: "Questrial"
  },
  container: {
    padding: "3vh",
    height: "inherit",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  bannerSearch: {
    display: "flex",
    height: "6vh",
    fontFamily: "questrial",
    alignItems: "center",
    justifyContent: "center",
    color: "black",
    width: "100%",
    fontSize: "1.5em"
  }
};

class ParkSearchList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allParks: props.allParks,
      textToSearch: [],
      query: props.query
    };
  }

  componentDidMount() {
    const { allParks } = this.state;
    allParks.forEach(park => {
      this.loadHighlightQuery(park);
    });
  }

  loadHighlightQuery(park) {
    var arr = [];
    var searchQueries = ["name", "states", "directions", "summary"];
    for (var s in searchQueries) {
      if (park[searchQueries[s]]) arr.push(park[searchQueries[s]]);
    }
    var arrString = [arr.toString()];
    this.setState(state => {
      const textToSearch = state.textToSearch.concat(arrString);

      return {
        textToSearch
      };
    });
  }

  render() {
    const { classes } = this.props;
    const { allParks, textToSearch, query } = this.state;
    var keygen = -1;
    return (
      <div>
        {allParks.length !== 0 && (
          <div className={classes.bannerSearch}>
            Search results for: {query}
          </div>
        )}
        {allParks.length === 0 && (
          <div className={classes.bannerSearch}>
            No results found for: {query}
          </div>
        )}
        <div className={classes.container}>
          {allParks.map(item => {
            keygen += 1;
            return (
              <SearchCard
                item={item}
                key={keygen}
                query={query}
                searchText={textToSearch[keygen]}
                link={"/Parks/"}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

ParkSearchList.propTypes = {
  data: PropTypes.object
};

export default withStyles(styles)(ParkSearchList);
