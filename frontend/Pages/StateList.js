import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import ListInfoCard from "../Components/ListInfoCard.js";
import Pagination from "../Components/Pagination.js";
import StateFilter from "../Components/StateFilter.js";
import StateSearchList from "./StateSearchList";
import axios from "axios";

//pass the paginated data into form row which will display the three cards at a time

function FormRow(props) {
  const data = props;
  if (data === undefined || data.data.length === 0) {
    return <div />;
  } else {
    const length = data.data.length;
    return (
      <React.Fragment>
        <Grid container spacing={40}>
          {length > 0 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"States/" + data.data[0].name}
                coverName={data.data[0].name}
                coverDescript="potate"
                coverImage={data.data[0].pic[0]}
                attribute1="Capital"
                attribute1val={data.data[0].capital}
                attribute2="Nickname"
                attribute2val={data.data[0].nickname}
                attribute3="Area Rank"
                attribute3val={data.data[0].areaRank}
                attribute4="Density Rank"
                attribute4val={data.data[0].densityRank}
                attribute5="Population Rank"
                attribute5val={data.data[0].popRank}
              />
            </Grid>
          )}
          {length > 1 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"States/" + data.data[1].name}
                coverName={data.data[1].name}
                coverDescript="potate"
                coverImage={data.data[1].pic[0]}
                attribute1="Capital"
                attribute1val={data.data[1].capital}
                attribute2="Nickname"
                attribute2val={data.data[1].nickname}
                attribute3="Area Rank"
                attribute3val={data.data[1].areaRank}
                attribute4="Density Rank"
                attribute4val={data.data[1].densityRank}
                attribute5="Population Rank"
                attribute5val={data.data[1].popRank}
              />
            </Grid>
          )}
          {length > 2 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"States/" + data.data[2].name}
                coverName={data.data[2].name}
                coverDescript="potate"
                coverImage={data.data[2].pic[0]}
                attribute1="Capital"
                attribute1val={data.data[2].capital}
                attribute2="Nickname"
                attribute2val={data.data[2].nickname}
                attribute3="Area Rank"
                attribute3val={data.data[2].areaRank}
                attribute4="Density Rank"
                attribute4val={data.data[2].densityRank}
                attribute5="Population Rank"
                attribute5val={data.data[2].popRank}
              />
            </Grid>
          )}
          {length > 3 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"States/" + data.data[3].name}
                coverName={data.data[3].name}
                coverDescript="potate"
                coverImage={data.data[3].pic[0]}
                attribute1="Capital"
                attribute1val={data.data[3].capital}
                attribute2="Nickname"
                attribute2val={data.data[3].nickname}
                attribute3="Area Rank"
                attribute3val={data.data[3].areaRank}
                attribute4="Density Rank"
                attribute4val={data.data[3].densityRank}
                attribute5="Population Rank"
                attribute5val={data.data[3].popRank}
              />
            </Grid>
          )}
          {length > 4 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"States/" + data.data[4].name}
                coverName={data.data[4].name}
                coverDescript="potate"
                coverImage={data.data[4].pic[0]}
                attribute1="Capital"
                attribute1val={data.data[4].capital}
                attribute2="Nickname"
                attribute2val={data.data[4].nickname}
                attribute3="Area Rank"
                attribute3val={data.data[4].areaRank}
                attribute4="Density Rank"
                attribute4val={data.data[4].densityRank}
                attribute5="Population Rank"
                attribute5val={data.data[4].popRank}
              />
            </Grid>
          )}
          {length > 5 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"States/" + data.data[5].name}
                coverName={data.data[5].name}
                coverDescript="potate"
                coverImage={data.data[5].pic[0]}
                attribute1="Capital"
                attribute1val={data.data[5].capital}
                attribute2="Nickname"
                attribute2val={data.data[5].nickname}
                attribute3="Area Rank"
                attribute3val={data.data[5].areaRank}
                attribute4="Density Rank"
                attribute4val={data.data[5].densityRank}
                attribute5="Population Rank"
                attribute5val={data.data[5].popRank}
              />
            </Grid>
          )}
          {length > 6 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"States/" + data.data[6].name}
                coverName={data.data[6].name}
                coverDescript="potate"
                coverImage={data.data[6].pic[0]}
                attribute1="Capital"
                attribute1val={data.data[6].capital}
                attribute2="Nickname"
                attribute2val={data.data[6].nickname}
                attribute3="Area Rank"
                attribute3val={data.data[6].areaRank}
                attribute4="Density Rank"
                attribute4val={data.data[6].densityRank}
                attribute5="Population Rank"
                attribute5val={data.data[4].popRank}
              />
            </Grid>
          )}
          {length > 7 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"States/" + data.data[7].name}
                coverName={data.data[7].name}
                coverDescript="potate"
                coverImage={data.data[7].pic[0]}
                attribute1="Capital"
                attribute1val={data.data[7].capital}
                attribute2="Nickname"
                attribute2val={data.data[7].nickname}
                attribute3="Area Rank"
                attribute3val={data.data[7].areaRank}
                attribute4="Density Rank"
                attribute4val={data.data[7].densityRank}
                attribute5="Population Rank"
                attribute5val={data.data[7].popRank}
              />
            </Grid>
          )}
          {length > 8 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"States/" + data.data[8].name}
                coverName={data.data[8].name}
                coverDescript="potate"
                coverImage={data.data[8].pic[0]}
                attribute1="Capital"
                attribute1val={data.data[8].capital}
                attribute2="Nickname"
                attribute2val={data.data[8].nickname}
                attribute3="Area Rank"
                attribute3val={data.data[8].areaRank}
                attribute4="Density Rank"
                attribute4val={data.data[8].densityRank}
                attribute5="Population Rank"
                attribute5val={data.data[8].popRank}
              />
            </Grid>
          )}
        </Grid>
      </React.Fragment>
    );
  }
}

const styles = theme => ({
  root: {
    paddingTop: "4vh",
    paddingBottom: "5vh",
    flexGrow: 1,
    height: "inherit",
    width: "90vw"
  },
  container: {
    backgroundColor: "#ebfaeb",
    paddingTop: "3.25vh",
    minHeight: "100vh",
    height: "auto",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  banner: {
    display: "flex",
    height: "14vh",
    fontFamily: "questrial",
    alignItems: "center",
    justifyContent: "center",
    color: "black",
    width: "100%",
    fontSize: "3.5em"
  },
  pageNumber: {
    width: "100%",
    textAlign: "center",
    fontFamily: "questrial",
    fontSize: "1.25em",
    marginBottom: "2vh"
  },
  bannerSearch: {
    display: "flex",
    height: "6vh",
    fontFamily: "questrial",
    alignItems: "center",
    justifyContent: "center",
    color: "black",
    width: "100%",
    fontSize: "1.5em"
  },
  dropdown: {}
});

class StateList extends Component {
  state = {
    allStates: [],
    currentStates: [],
    currentPage: null,
    totalPages: null,
    filtered: false,
    sorted: false,
    searched: false
  };

  filterBy = (population, area, density, sort, search) => {
    this.setState({ searched: false });
    this.setState({ searchText: search });
    var query = "https://api.connectwithnature.me/state";
    var list = [];
    var fil = false;
    var srt = false;
    var srch = false;

    if (
      population !== "Filter by Population: " ||
      area !== "Filter by Area: " ||
      sort !== "Sort By: " ||
      density !== "Filter by Density: " ||
      search !== ""
    ) {
      query += "/filter";
      if (population !== "Filter by Population: ") {
        fil = true;
        if (list.length === 0) list.push(`?population=${population}`);
        else list.push(`&population=${population}`);
      }
      if (area !== "Filter by Area: ") {
        fil = true;
        if (list.length === 0) list.push(`?area=${area}`);
        else list.push(`&area=${area}`);
      }
      if (density !== "Filter by Density: ") {
        fil = true;
        if (list.length === 0) list.push(`?density=${density}`);
        else list.push(`&density=${density}`);
      }
      if (sort !== "Sort By: ") {
        srt = true;
        if (list.length === 0) list.push(`?sort=${sort}`);
        else list.push(`&sort=${sort}`);
      }
      if (search !== "") {
        srch = true;
        if (list.length === 0) list.push(`?search=${search}`);
        else list.push(`&search=${search}`);
      }
    }

    list.forEach(x => {
      query += x;
    });

    axios
      .all([axios.get(query)])
      .then(
        axios.spread(stateFilterResponse => {
          this.setState({
            currentStates: stateFilterResponse.data,
            allStates: stateFilterResponse.data
          });
        })
      )
      .then(filter =>
        filter || fil
          ? this.setState({ filtered: true })
          : console.log("filter not set")
      )
      .then(sort =>
        sort || srt
          ? this.setState({ sorted: true })
          : console.log("sort not set")
      )
      .then(search =>
        search || srch
          ? this.setState({ searched: true })
          : console.log("search not set")
      )
      .then(this.onPageChangedFilter());
  };

  scrapeProjectInfo = () => {
    axios.all([axios.get("https://api.connectwithnature.me/state")]).then(
      axios.spread(stateResponse => {
        this.setState({
          allStates: stateResponse.data,
          currentStates: stateResponse.data,
          filtered: false,
          sorted: false,
          searched: false
        });
      })
    );
  };

  componentDidMount() {
    this.scrapeProjectInfo();
  }

  onPageChangedFilter = () => {
    const { allStates } = this.state;
    const pageLimit = this.pageLimit;
    const currentPage = 1;
    const totalPages = Math.ceil(this.totalRecords / this.pageLimit);

    const offset = (currentPage - 1) * pageLimit;
    const currentStates = allStates.slice(offset, offset + pageLimit);

    this.setState({ currentPage, currentStates, totalPages });
  };

  onPageChanged = data => {
    const { allStates } = this.state;
    const { currentPage, totalPages, pageLimit } = data;

    const offset = (currentPage - 1) * pageLimit;
    const currentStates = allStates.slice(offset, offset + pageLimit);

    this.setState({ currentPage, currentStates, totalPages });
  };

  render() {
    window.scrollTo(0, 0);
    const { classes } = this.props;
    const {
      allStates,
      currentStates,
      filtered,
      searched,
      searchText
    } = this.state;
    return (
      <div className={classes.container}>
        <div className={classes.banner}>States</div>
        <hr />

        <StateFilter
          className={classes.dropdown}
          func={this.filterBy}
          resetFunc={this.scrapeProjectInfo}
          searched={searched}
        />

        {searched && (
          <StateSearchList allStates={currentStates} query={searchText} />
        )}

        {!searched && (
          <Grid container className={classes.root}>
            <Grid container item xs={12}>
              <div className={classes.pageNumber}>
                Page {this.state.currentPage} / {Math.ceil(allStates.length/9) == 0 ? 1 : Math.ceil(allStates.length/9)}
              </div>
              <FormRow className={classes.items} data={currentStates} />
            </Grid>
          </Grid>
        )}

        {allStates.length !== 0 && !filtered && !searched && (
          <Pagination
            className={classes.pagination}
            totalRecords={allStates.length}
            pageLimit={9}
            onPageChanged={this.onPageChanged}
          />
        )}

        {allStates.length !== 0 && filtered && !searched && (
          <Pagination
            className={classes.pagination}
            totalRecords={allStates.length}
            pageLimit={9}
            onPageChanged={this.onPageChanged}
          />
        )}
      </div>
    );
  }
}

StateList.propTypes = {
  data: PropTypes.object
};

export default withStyles(styles)(StateList);
