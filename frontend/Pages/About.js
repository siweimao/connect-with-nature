import React from "react";
import StatTable from "../Components/StatTable.js";
import AboutCard from "../Components/AboutCard.js";
import DeveloperCard from "../Components/DeveloperCard.js";
import ToolCard from "../Components/ToolCard.js";
import allen from "../Components/media/allen.png";
import deyuan from "../Components/media/deyuan.jpeg";
import kimberly from "../Components/media/kimberly.png";
import siwei from "../Components/media/siwei.png";
import tin from "../Components/media/tin.jpeg";
import { withStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

const styles = {
  banner: {
    display: "flex",
    height: "14vh",
    fontFamily: "questrial",
    alignItems: "center",
    justifyContent: "center",
    color: "black",
    width: "100%",
    fontSize: "3.5em"
  },
  container: {
    display: "flex",
    paddingTop: "3.25vh",
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "#ebfaeb",
    height: "auto"
  },
  containerDevs: {
    display: "flex",
    flexDirection: "rows",
    justifyContent: "space-evenly",
    flexWrap: "wrap",
    fontFamily: "Questrial"
  },
  header: {
    paddingTop: "24px",
    paddingBottom: "8px",
    color: "black",
    fontSize: "50px",
    textAlign: "center",
    fontFamily: "Questrial"
  },
  table: {
    margin: "1vh",
    fontFamily: "Questrial"
  },
  hr: {
    height: "1px",
    width: "20vw",
    borderTop: "1.75px solid black",
    marginTop: "0",
    marginBottom: "3vh"
  },
  title: {
    textAlign: "center",
    padding: "12px",
    fontFamily: "Questrial",
    fontSize: "30px"
  },
  containerTools: {
    display: "flex",
    flexDirection: "rows",
    justifyContent: "space-evenly",
    flexWrap: "wrap",
    fontFamily: "Questrial",
    width: "70vw",
    margin: "1vh",
    backgroundColor: "white"
  },
  containerToolsUsed: {
    display: "flex",
    flexDirection: "rows",
    justifyContent: "space-evenly",
    flexWrap: "wrap",
    fontFamily: "Questrial",
    width: "70vw",
    padding: "20px",
    backgroundColor: "white",
    boxShadow: "0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)",
    marginBottom: "7vh",
    marginTop: "1vh"
  }
};

function About(props) {
  const { classes } = props;
  const insight =
    "When integrating all of the disparate data about national parks, the parks' events, nature conservation organizations, and the fifty states, interesting results and patterns began to emerge. There was a notable discrepancy between the number of national parks, park events, and nature conservation organizations in the states with higher elevation compared to states with lower elevation. For example, the states that hold the Rocky Mountains and Appalachian Mountains also held a greater number of national parks, park events, and nature conservation organizations compared to states with lower elevation. Additionally, another patttern that emerged is that the states that hold a greater number of national parks also are home to a greater number of nature conservation organizations. ";

  return (
    <div className={classes.container}>
      <Typography className={classes.banner}>About Us</Typography>
      <hr className={classes.hr} />
      <AboutCard
        title={"Project Goal"}
        text={
          "The goal of this project is to promote civic appreciation of nature and raise awareness for future conservation efforts by encouraging visitation to the national parks. We hope that this site will you help you better appreciate the importance of preservation and consider becoming involved in current conservation efforts!\n\nNational Parks are enjoyed by many everywhere in the United States but are maintained only by a few. This website is a cumulated effort in helping others understand the value of these national parks and the importance in preserving them. To this end, the website provides information of the convservation groups contributing towards national park preservation for the public to see and the many events these organizations and national parks have for the public to participate in."
        }
      />
      <AboutCard title={"Insight"} text={insight} />
      <div className={classes.containerDevs}>
        <DeveloperCard
          name="Siwei Mao"
          image={siwei}
          text={
            "Junior Computer Science major who likes eating\n \nResponsibilities: Back-end, Hosting"
          }
        />
        <DeveloperCard
          name="Deyuan Zhang"
          image={deyuan}
          text={
            "Junior Computer Science major who likes trading\n \nResponsibilities: Back-end, API Scraping"
          }
        />
        <DeveloperCard
          name="Tin La"
          image={tin}
          text={
            "Senior Computer Science major who loves sleeping \n \n Responsibilities: Front-end, React"
          }
        />
        <DeveloperCard
          name="Kimberly Hwang"
          image={kimberly}
          text={
            "Junior Computer Science major who likes corgis \n \n Responsibilities: Back-end, API Scraping"
          }
        />
        <DeveloperCard
          name="Allen Chen"
          image={allen}
          text={
            "Senior Computer Science major who likes running \n \n Responsibilities: Front-end, About Page, Testing "
          }
        />
      </div>
      <StatTable className={classes.table} />
      <AboutCard
        title="Data Sources"
        links={[
          "NPS API: https://www.nps.gov/subjects/developer/index.htm",
          "We used the NPS API to extract a JSON for information about different national parks. \n",
          "Wikipedia API: https://en.wikipedia.org/w/api.php",
          "We used the Wikipedia API to extract a JSON for information about the different states. \n",
          "YouTube API: https://developers.google.com/youtube/v3/",
          "We used the YouTube API to implement embedded videos in our instance pages.",
          "CharityNavigator API: http://api.charitynavigator.org/",
          "We used the CharityNavigator API to extract a JSON for information about the different charities concerning nature conservation.",
          "GitLab API: https://gitlab.com",
          "We used the Gitlab API to dynamically update our commits and issues."
        ]}
      />
      <AboutCard
        title="Links"
        links={[
          "GitLab Repository: https://gitlab.com/siweimao/connect-with-nature",
          "Postman: https://documenter.getpostman.com/view/6815214/S1ETPvY9"
        ]}
      />

      <div className={classes.containerToolsUsed}>
        <Typography className={classes.title}>Tools</Typography>
        <div className={classes.containerTools}>
          <ToolCard
            name="GitLab"
            image={"https://docs.gitlab.com/assets/images/gitlab-logo.svg"}
            text={
              "We used GitLab for version control."
            }
          />
          <ToolCard
            name="Postman"
            image={"https://www.getpostman.com/img/v2/media-kit/Logo/PNG/pm-logo-horiz.png"}
            text={
              "We used Postman to scrape data from APIs and create our Restful API."
            }
          />
          <ToolCard
            name="React"
            image={"https://arcweb.co/wp-content/uploads/2016/10/react-logo-1000-transparent.png"}
            text={
              "We used React for the front-end framework for creating the user interface."
            }
          />
          <ToolCard
            name="NamesCheap"
            image={"http://pluspng.com/img-png/logo-namecheap-png-namecheap-domain-and-hosting-coupons-promocodes-480.png"}
            text={
              "We used NamesCheap to obtain a pretty URL."
            }
          />
          <ToolCard
            name="AWS"
            image={"https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Amazon_Web_Services_Logo.svg/1200px-Amazon_Web_Services_Logo.svg.png"}
            text={
              "We used Amazon Web Services to host our website on the Internet."
            }
          />
          <ToolCard
            name="Docker"
            image={"https://www.docker.com/sites/default/files/social/docker_facebook_share.png"}
            text={
              "We used Docker to create containers for frontend and backend stacks."
            }
          />
          <ToolCard
            name="PostGreSQL"
            image={"https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/540px-Postgresql_elephant.svg.png"}
            text={
              "We used PostgreSQL as our Database."
            }
          />
          <ToolCard
            name="Flask"
            image={"https://cdn.freebiesupply.com/logos/thumbs/2x/flask-logo.png"}
            text={
              "We used Flask to route our endpoints for  the API."
            }
          />
          <ToolCard
            name="SQLAchemy"
            image={"https://quintagroup.com/cms/python/images/sqlalchemy-logo.png/@@images/eca35254-a2db-47a8-850b-2678f7f8bc09.png"}
            text={
              "We utilized SQLAlchemy to abstract SQL operations into the Amazon RDS database."
            }
          />
          <ToolCard
            name="Mocha"
            image={"https://cdn.freebiesupply.com/logos/large/2x/mocha-1-logo-png-transparent.png"}
            text={
              "We used Mocha to write unit tests of the JavaScript code."
            }
          />
          <ToolCard
            name="Selenium"
            image={"https://www.seleniumhq.org/images/big-logo.png"}
            text={
              "We used Selenium to write acceptance tests of the GUI."
            }
          />
          <ToolCard
            name="D3"
            image={"https://cdn.freebiesupply.com/logos/large/2x/d3-2-logo-png-transparent.png"}
            text={
              "We used D3 to create data visualizations."
            }
          />
        </div>
      </div>
    </div>


  );
}

export default withStyles(styles)(About);
