import React, { Component } from "react";
import StateInfoCard from "../Components/StateInfoCard.js";
import RelatedCard from "../Components/RelatedCard.js";
import StateTwitterBox from "../Components/StateTwitterBox.js";
import NoneCard from "../Components/NoneCard.js";

import axios from "axios";

const Header = {
  color: "white",
  fontSize: "4em",
  textAlign: "center",
  fontFamily: "Questrial"
};

const Background = {
  backgroundColor: "#ebfaeb",
  minHeight: "100vh",
  height: "auto"
};

const ImageContainer = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center"
};

const Image = {
  display: "block",
  width: "100%",
  maxHeight: "300px",
  objectFit: "cover",
  filter: "blur(3px) brightness(40%)"
};

const Content = {
  position: "absolute",
  display: "flex",
  flexDirection: "column"
};

const contentContainer = {
  margin: "0 8vw 0 8vw"
};

const paddingTopStyle = {
  paddingTop: "3vh"
};

const loaderContainer = {
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  minHeight: "100vh",
  backgroundColor: "#ebfaeb"
};

const ContainerChild = {
  display: "flex",
  flexDirection: "row",
  paddingTop: "3vh",
  width: "800px"
};

const RelatedContainer = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  flexWrap: "wrap",
  paddingBottom: "4vh"
};

const Related = {
  color: "black",
  fontSize: "4em",
  textAlign: "center",
  fontFamily: "Questrial",
  marginTop: "7vh"
};

const hr = {
  height: "1px",
  width: "20vw",
  borderTop: "1.75px solid black",
  marginTop: "0",
  marginBottom: "3vh"
};

class StatesInstance extends Component {
  state = {
    data: {}
  };

  scrapeProjectInfo() {
    axios
      .all([
        axios.get(
          `https://api.connectwithnature.me/state?state_name=${
            this.props.match.params.name
          }`
        )
      ])
      .then(
        axios.spread(stateResponse => {
          this.setState({
            data: stateResponse.data[0]
          });
        })
      );
  }

  componentDidMount() {
    this.scrapeProjectInfo();
  }

  render() {
    window.scrollTo(0, 0);
    const { data } = this.state;
    const {
      name,
      summary,
      parks,
      pic,
      events,
      orgs,
      park_pics,
      org_pics,
      event_pics,
      state_map,
      twitter
    } = data !== undefined ? data : "";

    var parkStr = parks;
    var parkName;
    if (parkStr) {
      parkName = parkStr[0];
    }
    var orgsArr = orgs;
    var orgName;
    if (orgsArr) {
      orgName = orgsArr[0];
    }
    var eventsArr = events;
    var eventsName;
    if (eventsArr) {
      eventsName = eventsArr[0];
    }

    var orgsPicUsed = org_pics;

    var eventsPics = event_pics;
    var eventsPicUsed;
    if (eventsPics) {
      eventsPicUsed = eventsPics[0];
    }

    var parksPics = park_pics;
    var parksPicUsed;
    if (parksPics) {
      parksPicUsed = parksPics[0];
    }

    if (name === undefined) {
      return (
        <div style={loaderContainer}>
          <div className="spinner-border text-success" />
        </div>
      );
    } else {
      return (
        <React.Fragment>
          <div style={Background}>
            <div style={ImageContainer}>
              <img alt="" style={Image} src={pic} />
              <div style={Content}>
                <h1 style={Header}>{name}</h1>
              </div>
            </div>
            <div style={contentContainer}>
              <div className="row">
                <div className="col-sm-6" style={paddingTopStyle}>
                  <div>
                    <img
                      alt=""
                      className="img-fluid"
                      style={ContainerChild}
                      src={pic}
                    />
                  </div>
                  <br />
                  <div>
                    <img
                      alt=""
                      className="img-fluid"
                      style={ContainerChild}
                      src={state_map}
                    />
                  </div>
                </div>
                <div className="col-sm-6" style={paddingTopStyle}>
                  <div>
                    <br />
                    <StateInfoCard style={ContainerChild} summary={summary} />
                    <br />
                    <div style={paddingTopStyle}>
                      <StateTwitterBox style={ContainerChild} name={twitter} />
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <h1 style={Related}>Related</h1>
            <hr style={hr} />
            <div style={RelatedContainer}>
              {parkName !== undefined && parkName.length > 0 && (
                <RelatedCard
                  title="Park"
                  link={"/Parks/" + parkName}
                  text={parkName}
                  pic={parksPicUsed}
                />
              )}
              {events && (
                <RelatedCard
                  title="Events"
                  link={"/Events/" + eventsName}
                  text={eventsName}
                  pic={eventsPicUsed}
                />
              )}
              {orgs && (
                <RelatedCard
                  title="Organization"
                  link={"/Orgs/" + orgName}
                  text={orgName}
                  pic={orgsPicUsed}
                />
              )}
              {parkName === undefined && !events && !orgs && <NoneCard />}
            </div>
            <br />
          </div>
        </React.Fragment>
      );
    }
  }
}

export default StatesInstance;
