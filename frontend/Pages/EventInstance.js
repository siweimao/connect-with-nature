import React, { Component } from "react";
import EventInfoCard from "../Components/EventInfoCard.js";
import RelatedCard from "../Components/RelatedCard.js";
import axios from "axios";
import Map from "../Components/Map.js";
import TwitterBox from "../Components/TwitterBox.js";


const Header = {
  color: "white",
  fontSize: "2.4em",
  textAlign: "center",
  fontFamily: "Questrial"
};

const Background = {
  backgroundColor: "#ebfaeb",
  minHeight: "100vh",
  height: "auto"
};

const ImageContainer = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

const Image = {
  display: "block",
  width: "100%",
  maxHeight: "300px",
  objectFit: "cover",
  filter: "blur(3px) brightness(40%)",
};

const DisplayImage = {
  display: "block",
  width: "100%",
  maxHeight: "350px",
  objectFit: "cover",
}

const contentContainer = {
  margin: "0 8vw 0 8vw",
};

const paddingTopStyle = {
  paddingTop: "3vh",
}

const loaderContainer = {
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  minHeight: "100vh",
  backgroundColor: "#ebfaeb"
};

const Content = {
  position: "absolute",
  display: "flex",
  flexDirection: "column"
};

const RelatedContainer = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  flexWrap: "wrap",
  paddingBottom: "4vh",
};

const Related = {
  color: "black",
  fontSize: "4em",
  textAlign: "center",
  fontFamily: "Questrial",
  marginTop: "7vh",
};

const hr = {
  height: "1px",
  width: "20vw",
  borderTop: "1.75px solid black",
  marginTop: "0",
  marginBottom: "3vh",
};

const MapWindow = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-evenly",
  paddingTop: "3vh",
  width: "100%",
};

class EventsInstance extends Component {
  state = {
    data: {
      pics: [
        "https://downloadpng.com/wp-content/uploads/thenext-thumb-cache//cartoon-tree-png-bfb6027a01338183db2bdfa145ba5932-900x0.png"
      ]
    }
  };

  scrapeProjectInfo() {
    axios
      .all([
        axios.get(
          `https://api.connectwithnature.me/event?event_name=${
            this.props.match.params.name
          }`
        )
      ])
      .then(
        axios.spread(eventResponse => {
          this.setState({
            data: eventResponse.data[0]
          });
        })
      );
  }

  componentDidMount() {
    this.scrapeProjectInfo();
  }

  render() {
    window.scrollTo(0, 0);
    var jsonStates = require("../data/states.json");
    const { data } = this.state;
    const {
      name,
      date,
      fee,
      location,
      park,
      pics,
      summary,
      time,
      states,
      orgs,
      park_pics,
      state_pics,
      org_pics
    } = data;
    var orgsArr = orgs;
    var orgName;
    if (orgsArr) {
      orgName = orgsArr[0];
    }
    var str = states;
    var res;
    if (states) {
      res = str.split(",");
      str = res[0];
    }

    var orgsPicUsed = org_pics;

    var parksPics = park_pics;
    var parksPicUsed;
    if (parksPics) {
      parksPicUsed = parksPics[0];
    }

    var statesPics = state_pics;
    var statesPicUsed;
    if (statesPics) {
      statesPicUsed = statesPics[0];
    }

    if (name === undefined) {
      return (
        <div style={loaderContainer}>
          <div className="spinner-border text-success" />
        </div>
      );
    } else { 
    return (
      <React.Fragment>
        <div style={Background}>
          <div style={ImageContainer}>
            <img alt="" style={Image} src={pics[0]} />
            <div style={Content}>
              <h1 style={Header}>
                {name}
              </h1>
            </div>
          </div>
          <div style={contentContainer}>
            <div className="row">
              <div className="col-sm-6" style={paddingTopStyle}>
                <div style={MapWindow}>
                  <img alt="" className="img-fluid" style={DisplayImage} src={pics[0]} />
                </div>         
                <div style={MapWindow}>
                  <Map
                    longitude={this.state.data.park_long}
                    latitude={this.state.data.park_lat}
                  />
                </div>
                <div style={paddingTopStyle}>
                  <TwitterBox name={park} />
                </div>
              </div>
              <div className="col-sm-6" style={paddingTopStyle}>
                <div>
                  <br />
                  <EventInfoCard
                    dates={date}
                    time={time}
                    location={location}
                    park={park}
                    fee={fee}
                    description={summary}
                  />
                  <br />
                </div>
              </div>  
            </div>
          </div>

          <h1 style={Related}>
            Related
          </h1>
          <hr style={hr} />
          <div style={RelatedContainer}>
            {park !== undefined && park.length > 0 && (
              <RelatedCard
                title="Park"
                link={"/Parks/" + park}
                text={park}
                pic={parksPicUsed}
              />
            )}

            {states !== undefined && states.length > 0 && (
              <RelatedCard
                title="States"
                link={"/States/" + jsonStates[str]}
                text={jsonStates[str]}
                pic={statesPicUsed}
              />
            )}

            {orgs && (
              <RelatedCard
                title="Organization"
                link={"/Orgs/" + orgName}
                text={orgName}
                pic={orgsPicUsed}
              />
            )}
          </div>
          <br />
        </div>
      </React.Fragment>
    );
  }
}
}

export default EventsInstance;
