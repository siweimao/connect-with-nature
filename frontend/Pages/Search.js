import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import ParkSearchList from "./ParkSearchList";
import StateSearchList from "./StateSearchList";
import EventSearchList from "./EventSearchList";
import OrgSearchList from "./OrgSearchList";

import axios from "axios";

const styles = {
  root: {
    minHeight: "100vh",
    backgroundColor: "#ebfaeb"
  },
  views: {
    height: "inherit",
    fontFamily: "Questrial"
  },
  font: {
    fontFamily: "Questrial",
    fontStyle: "bold",
    fontSize: "20px"
  },
  loaderContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    minHeight: "100vh",
    backgroundColor: "#ebfaeb"
  }
};

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      resultsPark: [],
      resultsState: [],
      resultsEvent: [],
      resultsOrg: [],
      value: 0,
      loaded: false,
      query: ""
    };
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  scrapeInfo(name) {
    axios
      .all([
        axios.get(
          "https://api.connectwithnature.me/park/filter?search=" + name
        ),
        axios.get(
          "https://api.connectwithnature.me/state/filter?search=" + name
        ),
        axios.get(
          "https://api.connectwithnature.me/event/filter?search=" + name
        ),
        axios.get("https://api.connectwithnature.me/org/filter?search=" + name)
      ])
      .then(
        axios.spread((fr, sr, tr, ffr) => {
          this.setState(
            {
              resultsPark: fr.data,
              resultsState: sr.data,
              resultsEvent: tr.data,
              resultsOrg: ffr.data,
              query: name
            },
            function() {
              this.setState({ loaded: true });
            }
          );
        })
      );
  }

  componentDidMount() {
    this.scrapeInfo(this.props.match.params.name);
  }

  componentDidUpdate() {
    if (this.props.history.location.state) {
      this.setState({ loaded: false });
      this.scrapeInfo(this.props.match.params.name);
      this.props.history.location.state = false;
    }
  }

  render() {
    const { classes, theme } = this.props;
    const {
      resultsPark,
      resultsEvent,
      resultsOrg,
      resultsState,
      query
    } = this.state;
    if (this.state.loaded === true) {
      return (
        <div className={classes.root}>
          <AppBar position="static" color="default">
            <Tabs
              value={this.state.value}
              onChange={this.handleChange}
              indicatorColor="primary"
              textColor="primary"
              variant="fullWidth"
            >
              <Tab className={classes.font} label="Parks" />
              <Tab className={classes.font} label="States" />
              <Tab className={classes.font} label="Events" />
              <Tab className={classes.font} label="Orgs" />
            </Tabs>
          </AppBar>
          <SwipeableViews
            axis={theme.direction === "rtl" ? "x-reverse" : "x"}
            index={this.state.value}
            onChangeIndex={this.handleChangeIndex}
            className={classes.views}
          >
            {(
              <ParkSearchList allParks={resultsPark} query={query} />
            )}

            {(
              <StateSearchList allStates={resultsState} query={query} />
            )}

            {(
              <EventSearchList allEvents={resultsEvent} query={query} />
            )}

            {(
              <OrgSearchList allOrgs={resultsOrg} query={query} />
            )}
          </SwipeableViews>
        </div>
      );
    } else {
      return (
        <div className={classes.loaderContainer}>
          <div className="spinner-border text-success" />
        </div>
      );
    }
  }
}

Search.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(Search);
