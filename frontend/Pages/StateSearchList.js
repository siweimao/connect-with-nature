import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import SearchCard from "../Components/SearchCard.js";

const styles = {
  root: {
    padding: "3vh",
    flexGrow: 1,
    height: "inherit"
  },
  container: {
    padding: "3vh",
    height: "inherit",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  bannerSearch: {
    display: "flex",
    height: "6vh",
    fontFamily: "questrial",
    alignItems: "center",
    justifyContent: "center",
    color: "black",
    width: "100%",
    fontSize: "1.5em"
  }
};

class StateSearchList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allStates: props.allStates,
      textToSearch: [],
      query: props.query
    };
  }

  componentDidMount() {
    const { allStates } = this.state;
    allStates.forEach(state => {
      this.loadHighlightQuery(state);
    });
  }

  loadHighlightQuery(state) {
    var arr = [];
    var searchQueries = ["abbr", "capital", "name", "nickname", "summary"];
    for (var s in searchQueries) {
      if (state[searchQueries[s]]) arr.push(state[searchQueries[s]]);
    }
    var arrString = [arr.toString()];
    this.setState(state => {
      const textToSearch = state.textToSearch.concat(arrString);

      return {
        textToSearch
      };
    });
  }

  render() {
    const { classes } = this.props;
    const { allStates, textToSearch, query } = this.state;
    var keygen = -1;
    return (
      <div>
        {allStates.length !== 0 && (
          <div className={classes.bannerSearch}>
            Search results for: {query}
          </div>
        )}
        {allStates.length === 0 && (
          <div className={classes.bannerSearch}>
            No results found for: {query}
          </div>
        )}
      <div className={classes.container}>
        {allStates.map(item => {
          keygen += 1;
          return (
            <SearchCard
              item={item}
              key={keygen}
              query={query}
              searchText={textToSearch[keygen]}
              link={"/States/"}
            />
          );
        })}
      </div>
      </div>
    );
  }
}

StateSearchList.propTypes = {
  data: PropTypes.object
};

export default withStyles(styles)(StateSearchList);
