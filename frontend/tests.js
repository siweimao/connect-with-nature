import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import { expect, assert } from 'chai';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });
import { BrowserRouter } from 'react-router-dom';

/* Components */
import AboutCard from './Components/AboutCard';
import ButtonAppBar from './Components/ButtonAppBar';
import ListInfoCard from './Components/ListInfoCard';
import ParkInfoCard from './Components/ParkInfoCard';
import RelatedCard from './Components/RelatedCard';
import Splash from './Components/Splash';
import StatTable from './Components/StatTable';
import Pagination from './Components/Pagination';
import ParkFilter from './Components/ParkFilter';

/* Pages */
import About from './Pages/About';
import StateList from './Pages/StateList';
import EventList from './Pages/EventList';
import OrgList from './Pages/OrgList';
import ParkList from './Pages/ParkList';
import EventInstance from './Pages/EventInstance';
import ParkInstance from './Pages/ParkInstance';
import StateInstance from './Pages/StateInstance';
import OrgInstance from './Pages/OrgInstance';

/* Component Tests */
describe('tests for <AboutCard> component', () => {
  it('can set text', () => {
    const wrapper = shallow(<AboutCard title="Project Goal" text="Lorem Ipsum"/>);
    expect(wrapper.prop('text')).equal("Lorem Ipsum");
  });
  
  it('can set links', () => {
    const placeholderLinks = ["linkOne", "linkTwo", "linkThree"];
    const wrapper = shallow(<AboutCard links={placeholderLinks} />);
    expect(wrapper.prop('links')).lengthOf(placeholderLinks.length);
  });
  it('can create with props', () => {
    const wrapper = shallow(<AboutCard />);
    expect(Object.keys(wrapper.prop('classes'))).lengthOf(5);
  });
});

describe('tests for <ButtonAppBar> component', () => {
  it('can create with props', () => {
    const wrapper = shallow(<ButtonAppBar />);
    expect(Object.keys(wrapper.prop('classes'))).lengthOf(2);
  });
});

describe('tests for <ListInfoCard> component', () => {
  it('can create with props', () => {
    const wrapper = shallow(<ListInfoCard />);
    expect(Object.keys(wrapper.prop('classes'))).lengthOf(3);
  });
  it('can set title', () => {
    const wrapper = shallow(<ListInfoCard title='test title'/>);
    expect(wrapper.prop('title')).equal('test title');
  });
});

describe('tests for <ParkInfoCard> component', () => {
  it('can create with props', () => {
    const wrapper = shallow(<ParkInfoCard />);
    expect(Object.keys(wrapper.prop('classes'))).lengthOf(6);
  });
  it('can set summary', () => {
    const wrapper = shallow(<ParkInfoCard summary='test summary'/>);
    expect(wrapper.prop('summary')).equal('test summary');
  });
});

describe('tests for <ParkFilter> component', () => {
  it('can create with props', () => {
    const wrapper = shallow(<ParkFilter />);
    expect(Object.keys(wrapper.prop('classes'))).lengthOf(6);
  });
});

describe('tests for <RelatedCard> component', () => {
  it('can create with props', () => {
    const wrapper = shallow(<RelatedCard />);
    expect(Object.keys(wrapper.prop('classes'))).lengthOf(3);
  });
  it('can set text', () => {
    const wrapper = shallow(<RelatedCard title='test text'/>);
    expect(wrapper.prop('title')).equal('test text');
  });
});

describe('tests for <Splash> component', () => {
  it('can create with props', () => {
    const wrapper = shallow(<Splash />);
    expect(Object.keys(wrapper.prop('classes'))).lengthOf(5);
  });
});

describe('tests for <StatTable> component', () => {
  it('can create with props', () => {
    const wrapper = shallow(<StatTable />);
    expect(Object.keys(wrapper.prop('classes'))).lengthOf(2);
  });
  
  it('creates with correct columns', () => {
    const wrapper = shallow(<StatTable />);
    expect(wrapper.html()).include("Name");
    expect(wrapper.html()).include("Issues");
    expect(wrapper.html()).include("Commits");
    expect(wrapper.html()).include("Unit Tests");
  });
});

describe('tests for <Pagination> component', () => {
  it('can create with props', () => {
    const wrapper = shallow(<Pagination totalRecords={60}/>);
    expect(Object.keys(wrapper.props().children.props)).includes('aria-label');
    expect(Object.keys(wrapper.props().children.props)).includes('children');
  });
  it('can create with required prop', () => {
    const wrapper = shallow(<Pagination totalRecords={60}/>);
    const instance = wrapper.instance();
    expect(Object.keys(instance.props)).includes('totalRecords');
  });
  it('can create with state', () => {
    const wrapper = shallow(<Pagination totalRecords={60}/>);
    const instance = wrapper.instance();
    expect(Object.keys(instance.state)).includes('currentPage');
  });
    it('can update state', () => {
    const wrapper = shallow(<Pagination totalRecords={60}/>);
    const instance = wrapper.instance();
    instance.gotoPage(2);
    expect(Object.values(instance.state)).to.include(2);
  });
});

/* Page Tests */
describe('tests for <About> page', () => {
  it('renders About page', () => {
    const wrapper = mount(<About />);
    expect(wrapper.find('About')).to.have.lengthOf(1);
    assert.ok(wrapper);
  });
  it('has 5 developer cards', () => {
    const wrapper = mount(<About />);
    expect(wrapper.find('DeveloperCard')).to.have.lengthOf(17);
  });  
  it('has stat table', () => {
    const wrapper = mount(<About />);
    expect(wrapper.find('StatTable')).to.have.lengthOf(1);
  });
});

describe('tests for <StateList> page', () => {
  it('renders StateList page', () => {
    const wrapper = mount(<BrowserRouter><StateList /></BrowserRouter>);
    expect(wrapper.find('StateList')).to.have.lengthOf(1);
    assert.ok(wrapper);
  });
  it('has search on page', () => {
    const wrapper = mount(<BrowserRouter><StateList /></BrowserRouter>);
    expect(wrapper.find('InputGroup')).to.not.have.lengthOf(0);
  }); 
  it('has filter on page', () => {
    const wrapper = mount(<BrowserRouter><StateList /></BrowserRouter>);
    expect(wrapper.find('StateFilter')).to.have.lengthOf(1);
  });
});

describe('tests for <EventList> page', () => {
  it('renders EventList page', () => {
    const wrapper = mount(<BrowserRouter><EventList /></BrowserRouter>);
    expect(wrapper.find('EventList')).to.have.lengthOf(1);
    assert.ok(wrapper);
  });
  it('has search on page', () => {
    const wrapper = mount(<BrowserRouter><EventList /></BrowserRouter>);
    expect(wrapper.find('InputGroup')).to.not.have.lengthOf(0);
  }); 
  it('has filter on page', () => {
    const wrapper = mount(<BrowserRouter><EventList /></BrowserRouter>);
    expect(wrapper.find('EventFilter')).to.have.lengthOf(1);
  });
});

describe('tests for <OrgList> page', () => {
  it('renders OrgList page', () => {
    const wrapper = mount(<BrowserRouter><OrgList /></BrowserRouter>);
    expect(wrapper.find('OrgList')).to.have.lengthOf(1);
    assert.ok(wrapper);
  });
  it('has search on page', () => {
    const wrapper = mount(<BrowserRouter><OrgList /></BrowserRouter>);
    expect(wrapper.find('InputGroup')).to.not.have.lengthOf(0);
  }); 
  it('has filter on page', () => {
    const wrapper = mount(<BrowserRouter><OrgList /></BrowserRouter>);
    expect(wrapper.find('OrgFilter')).to.have.lengthOf(1);
  });
});

describe('tests for <ParkList> page', () => {
  it('renders ParkList page', () => {
    const wrapper = mount(<BrowserRouter><ParkList /></BrowserRouter>);
    expect(wrapper.find('ParkList')).to.have.lengthOf(1);
    assert.ok(wrapper);
  });
  it('has search on page', () => {
    const wrapper = mount(<BrowserRouter><ParkList /></BrowserRouter>);
    expect(wrapper.find('InputGroup')).to.not.have.lengthOf(0);
  }); 
  it('has filter on page', () => {
    const wrapper = mount(<BrowserRouter><ParkList /></BrowserRouter>);
    expect(wrapper.find('ParkFilter')).to.have.lengthOf(1);
  });
});

describe('tests for <EventsInstance> page', () => {
  const parameterObject = {"params": {"name": "Ask a Geologist Drop-In Table"}};
  it('renders EventInstance page', () => {
    const wrapper = mount(<BrowserRouter><EventInstance match = {parameterObject}/></BrowserRouter>);
    expect(wrapper.find('EventsInstance')).to.have.lengthOf(1);
    assert.ok(wrapper);
  });
  it('has loader on page before complete render', () => {
    const wrapper = mount(<BrowserRouter><EventInstance match = {parameterObject}/></BrowserRouter>);
    expect(wrapper.find('EventInfoCard')).to.have.lengthOf(0);
  });
});

describe('tests for <ParkInstance> page', () => {
  const parameterObject = {"params": {"name": "Yosemite National Park"}};
  it('renders ParkInstance page', () => {
    const wrapper = mount(<BrowserRouter><ParkInstance match = {parameterObject}/></BrowserRouter>);
    expect(wrapper.find('ParksInstance')).to.have.lengthOf(1);
    assert.ok(wrapper);
  });
    it('has loader on page before complete render', () => {
    const wrapper = mount(<BrowserRouter><ParkInstance match = {parameterObject}/></BrowserRouter>);
    expect(wrapper.find('ParkInfoCard')).to.have.lengthOf(0);
  });
});

describe('tests for <StateInstance> page', () => {
  const parameterObject = {"params": {"name": "Texas"}};
  it('renders StateInstance page', () => {
    const wrapper = mount(<BrowserRouter><StateInstance match = {parameterObject}/></BrowserRouter>);
    expect(wrapper.find('StatesInstance')).to.have.lengthOf(1);
    assert.ok(wrapper);
  });
  it('has loader on page before complete render', () => {
    const wrapper = mount(<BrowserRouter><StateInstance match = {parameterObject}/></BrowserRouter>);
    expect(wrapper.find('StateInfoCard')).to.have.lengthOf(0);
  });
});

describe('tests for <OrgInstance> page', () => {
  const parameterObject = {"params": {"name": "Conservation Strategy Fund"}};
  it('renders OrgInstance page', () => {
    const wrapper = mount(<BrowserRouter><OrgInstance match = {parameterObject}/></BrowserRouter>);
    expect(wrapper.find('OrgsInstance')).to.have.lengthOf(1);
    assert.ok(wrapper);
  });
  it('has loader on page before complete render', () => {
    const wrapper = mount(<BrowserRouter><OrgInstance match = {parameterObject}/></BrowserRouter>);
    expect(wrapper.find('OrgInfoCard')).to.have.lengthOf(0);
  });
});
