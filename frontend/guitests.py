from unittest import main, TestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

class UnitTests (TestCase) :
    @classmethod
    def setUpClass(self):
        chrome_driver = r'/Users/Siwei/Documents/webdriver/chromedriver'
        self.driver = webdriver.Chrome(chrome_driver)
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()
        self.driver.get("https://www.connectwithnature.me")
        self.driver.title

    def test_navbar_park_link(self):
        park_link = self.driver.find_element_by_xpath('//a[@href="/Parks"]')
        park_link.click()
        anchorTags = self.driver.find_elements_by_tag_name("a")
        pageContainsNationalPark = False
        for anchorTag in anchorTags:
            elementToTest = self.driver.find_element_by_link_text(anchorTag.text)
            if "Park" in elementToTest.text:
                pageContainsNationalPark = True
                break

        assert pageContainsNationalPark
        assert "Park" in self.driver.current_url

    def test_navbar_state_link(self):
        state_link = self.driver.find_element_by_xpath('//a[@href="/States"]')
        state_link.click()
        assert "State" in self.driver.current_url

    def test_navbar_event_link(self):
        org_link = self.driver.find_element_by_xpath('//a[@href="/Events"]')
        org_link.click()
        assert "Events" in self.driver.current_url

    def test_navbar_org_link(self):
        org_link = self.driver.find_element_by_xpath('//a[@href="/Orgs"]')
        org_link.click()
        assert "Orgs" in self.driver.current_url

    def test_navbar_about_link(self):
        org_link = self.driver.find_element_by_xpath('//a[@href="/About"]')
        org_link.click()
        assert "About" in self.driver.current_url

    def test_navbar_home_link(self):
        org_link = self.driver.find_element_by_xpath('//a[@href="/Home"]')
        org_link.click()
        assert "Home" in self.driver.current_url

    def test_navbar_inavlid_link(self):
        bad_url = "https://www.connectwithnature.me/invalid"
        self.driver.get(bad_url)
        invalidImage = self.driver.find_element_by_tag_name("img")
        assert invalidImage.get_attribute("alt") == "thinking..."

    def test_state_pagination(self):
         self.pagination_test('https://www.connectwithnature.me/States')

    def test_org_pagination(self):
        self.pagination_test('https://www.connectwithnature.me/Orgs')

    def test_event_pagination(self):
        self.pagination_test('https://www.connectwithnature.me/Events')

    def test_park_pagination(self):
        self.pagination_test('https://www.connectwithnature.me/Parks')

    def pagination_test(self, url):
        self.driver.get(url)
        imgTags = self.driver.find_elements_by_tag_name("img")

        # parks on the first page
        orig_models = []
        for img in imgTags:
            model_instance = img.get_attribute("title")
            orig_models.append(model_instance)

        page = self.driver.find_element_by_class_name("pagination")
        items = page.find_elements_by_tag_name("li")
        for item in items:
            # check start on the first page
            if item.get_attribute('class') == 'page-item active':
                assert item.find_element_by_tag_name('a').get_attribute('innerHTML') == '1'
            # navigate to the second page
            if  item.find_element_by_tag_name('a').get_attribute('aria-label') == 'Next':
                item.click()
                break

        # events on the second page
        newImgTags = self.driver.find_elements_by_tag_name("img")
        for img in newImgTags:
            model_instance = img.get_attribute("title")
            assert model_instance not in orig_models

        page = self.driver.find_element_by_class_name("pagination")
        items = page.find_elements_by_tag_name("li")
        for item in items:
            # check on second page
            if item.get_attribute('class') == 'page-item active':
                assert item.find_element_by_tag_name('a').get_attribute('innerHTML') == '2'
                break

    # def test_state_filter(self):
    #     url = 'https://www.connectwithnature.me/States'
    #     self.driver.get(url)
    #     search = self.driver.find_element_by_tag_name("input")
    #     search.send_keys("texas")
    #     search.send_keys(Keys.RETURN);

        # imgTags = self.driver.find_elements_by_tag_name("img")
        # pageContainsNationalPark = False
        # for imgTag in imgTags:
            # print(imgTag.get_attribute("title"))



    def test_park_filter(self):
        url = 'https://www.connectwithnature.me/Parks'
        self.driver.get(url)
        dropdown = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, 'dropdown-basic')))
        # click the dropdown button
        dropdown.click()
        # find all list elements in the dropdown.
        # target the parent of the button for the list
        li = dropdown.parent.find_elements_by_tag_name('li')
        before = self.driver.find_elements_by_tag_name("a")
        #print(before)
        # click the second element in list
        li[1].click()

        buttons = self.driver.find_elements_by_xpath("//*[contains(text(), 'Go')]")
        for btn in buttons:
           btn.click()
        after = self.driver.find_elements_by_tag_name("a")
        #print(after)
        assert len(before) != len(after)

    def test_event_filter(self):
        url = 'https://www.connectwithnature.me/Events'
        self.driver.get(url)
        dropdown = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, 'dropdown-basic')))
        # click the dropdown button
        dropdown.click()
        # find all list elements in the dropdown.
        # target the parent of the button for the list
        li = dropdown.parent.find_elements_by_tag_name('li')
        before = self.driver.find_elements_by_tag_name("a")
        #print(before)
        # click the second element in list
        li[1].click()

        buttons = self.driver.find_elements_by_xpath("//*[contains(text(), 'Go')]")
        for btn in buttons:
           btn.click()
        after = self.driver.find_elements_by_tag_name("a")
        #print(after)
        assert len(before) != len(after)

    def test_org_filter(self):
        url = 'https://www.connectwithnature.me/Orgs'
        self.driver.get(url)
        dropdown = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, 'dropdown-basic')))
        # click the dropdown button
        dropdown.click()
        # find all list elements in the dropdown.
        # target the parent of the button for the list
        li = dropdown.parent.find_elements_by_tag_name('li')
        before = self.driver.find_elements_by_tag_name("a")
        #print(before)
        # click the second element in list
        li[1].click()

        buttons = self.driver.find_elements_by_xpath("//*[contains(text(), 'Go')]")
        for btn in buttons:
           btn.click()
        after = self.driver.find_elements_by_tag_name("a")
        #print(after)
        assert len(before) != len(after)



    @classmethod
    def tearDownClass(self):
        self.driver.quit()

if __name__ == "__main__" :
    main()
