Click==7.0
contextlib2==0.5.5
Flask==1.0.2
Flask-Cors==3.0.7
itsdangerous==1.1.0
Jinja2==2.10
MarkupSafe==1.1.1
psycopg2==2.7.7
schema==0.7.0
six==1.12.0
SQLAlchemy==1.3.1
Werkzeug==0.15.1
